Build and docker configurations for various mymedic projects

# RUN LOCALLY

1. Install prerequisites

- Docker and Docker Compose (https://docs.docker.com/install/linux/docker-ce/ubuntu/#install-docker-ce)
2. Clone repository to your development directory (`/home/user/mymedic-monorepo`)
3. Create your data directory ```mkdir data```. Go to data and copy FULL PATH (e.g. /home/user/mymedic-monorepo/data)

4. Set proper environmental variables:

```
export SOLUTION_DATA_PATH="/home/user/mymedic-monorepo/data
export KEYCLOAK_ADMIN_USERNAME=admin
export KEYCLOAK_ADMIN_PASSWORD=admin
```
add this to your .bashrc - /home/user/.bashrc
5.
Open new console in mymedic directory and run
```docker-compose up```


You can put Environment Variable SOLUTION_DATA_PATH into your `.bashrc`, to make it default for your Linux account. 
SOLUTION_DATA_PATH is mandatory for proper installation and running.
If you want to run system in background use -d argument (`docker-compose up -d`)

All services are proxied and available on port 80

After installation, you've got following routes:

http://localhost - main app

http://localhost/auth - integrated Keycloak instance

http://localhost/fhir - FHIR instance


# RUN ON PRODUCTION

Following instructions works for Debian-based servers, along with Gitlab auto-deployment:

1. Mount/create following directories - anywhere, where you could back-up your data, and even easilly restore whole service in critical situations:
    ```
        mkdir /root/mymedic-data
    ```
2. Install prerequisites
- Docker and Docker Compose (https://docs.docker.com/install/linux/docker-ce/ubuntu/#install-docker-ce)
- Gitlab Runner (https://docs.gitlab.com/runner/install/linux-manually.html)
- Apache2 utils
3. Change ownership of data dirs for gitlab-runner
   ```chown -R gitlab-runner /root/mymedic-data```
   I admit you to add some redundance for this data (rsync it with some other server)
4. Run `sudo gitlab-runner start` . Set up your VPS as Runner for this repository in Gitlab > CI / CD >  Runners > Set up a specific Runner manually, using your token. 
5. Use the Gitlab pipeline to deploy the project. After first deployment connect domain, by pointing it to your VPS server..
6. Put gitlab-runner account to sudoers
7. Go to `/home/gitlab-runner` and create `certbot` directory. Assign permissions 755 `chmod -R 755 certbot`.
8. After purchasing and connecting your domain, set up Environment variables for your repository. Go to Gitlab > Settings > CI/CD and put following variables:
    ``` 
        SOLUTION_DATA_PATH=/root/mymedic-data
        DOMAIN=your_domain
        EMAIL=your_contact_email
    ```
9.  Run `docker-compose -f docker-compose.letsencrypt.yml up -d letsencrypt-nginx`. 
10. 
```docker run -it --rm --name certbot -v SOLUTION_DATA_PATH/mymedicpro-devops/data/letsencrypt:/etc/letsencrypt certbot/certbot certonly --webroot -w /etc/letsencrypt/nginx-acme/ --domain YOUR_DOMAIN --agree-tos --email your@email.com```
