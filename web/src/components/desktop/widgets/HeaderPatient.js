import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import logo from '../pages/images/logo.png';

const styles = () => ({
    root: {
        textAlign: 'left',
        fontFamily: 'Poppins',
        fontSize: '16px'
    },
    bdg: {
        paddingBottom: '85px',
        width: '100%'
    },
    bgt: {
        backgroundColor: '#f1f1f1'
    },
    sc: {
        backgroundColor: '#fff',
        width: '100%',
        height: '100%',
        margin: '0 auto',
        marginBottom: '5px'
    },
    inputs: {
        border: '1px solid',
        borderColor: '#cccccc',
        color: '#000',
        borderRadius: '3px',
        marginTop: '6px',
        marginBottom: '6px',
        padding: '12px',
        fontSize: '15px',
        width: '92%'
    },
    logo: {
        height: '40px',
        width: '222px',
        marginTop: '28px',
        marginLeft: '0',
        marginBottom: '0'
    },
    elementMenu: {
        display: 'inline-block',
        height: '6vh',
        marginTop: '5vh',
        marginBottom: '3vh',
        marginRight: '15px',
        paddingTop: '15px',
        paddingBottom: '15px',
        color: '#8b909b',
        fontWeight: '600',
        textDecoration: 'none'
    },
    login: {
        backgroundImage: `url(${login})`,
        height: '72px',
        width: '230px',
        backgroundSize: 'contain',
        backgroundRepeat: 'no-repeat',
        backgroundPosition: 'center'
    },
    graf: {
        backgroundImage: `url(${graf})`,
        height: '340px',
        width: '180px',
        backgroundSize: 'contain',
        backgroundRepeat: 'no-repeat',
        backgroundPosition: 'center',
        position: 'absolute',
        top: '45%',
        right: '2%'
    },
    geo: {
        display: 'inline-block',
        backgroundImage: `url(${geo})`,
        height: '15px',
        width: '13px',
        backgroundSize: 'contain',
        backgroundRepeat: 'no-repeat',
        backgroundPosition: 'center',
        marginRight: '4px',
        verticalAlign: 'middle'
    },
    nextArrow: {
        display: 'inline-block',
        backgroundImage: `url(${nextArrow})`,
        height: '18px',
        width: '18px',
        backgroundSize: 'contain',
        backgroundRepeat: 'no-repeat',
        backgroundPosition: 'center',
        marginRight: '4px',
        verticalAlign: 'middle'
    },
    bg: {
        backgroundColor: '#f1f1f1',
        width: '100%',
        height: '100%'
    },
    borderBox: {
        maxWidth: '952px',
        border: '1px solid',
        color: '#dadada'
    },
    formBox: {
        textAlign: 'center',
        paddingTop: '20px',
        paddingLeft: '85px',
        paddingRight: '85px',
        paddingBottom: '20px'
    },
    formBox1: {
        textAlign: 'left',
        paddingLeft: '85px',
        paddingRight: '85px',
        paddingBottom: '20px'
    },
    formBox2: {
        textAlign: 'center',
        paddingTop: '0',
        paddingLeft: '85px',
        paddingRight: '85px',
        paddingBottom: '20px'
    },
    centerBox: {
        width: '230px',
        height: '230px',
        margin: '176px auto'
    },
    span1: {
        color: '#383838',
        display: 'inline',
        fontWeight: '700',
        fontSize: '20px'
    },
    span2: {
        color: '#2B99F3',
        fontWeight: '700',
        fontSize: '20px'
    },
    span3: {
        color: '#383838'
    },
    p1: {
        color: '#383838',
        fontWeight: '700',
        fontSize: '20px'
    },
    btnForm: {
        border: 'none',
        borderRadius: '3px',
        width: '100%',
        paddingTop: '12px',
        paddingBottom: '12px',
        fontSize: '15px',
        fontWeight: '600',
        backgroundColor: '#0eb9ba',
        color: '#fff',
        marginBottom: '15px'
    },
    btnFacebook: {
        border: 'none',
        borderRadius: '3px',
        width: '100%',
        paddingTop: '12px',
        paddingBottom: '12px',
        fontSize: '15px',
        fontWeight: '600',
        backgroundColor: '#3c5a99',
        color: '#fff',
        marginBottom: '15px'
    },
    btnLogin: {
        border: 'none',
        borderRadius: '3px',
        paddingTop: '12px',
        paddingBottom: '12px',
        paddingLeft: '33px',
        paddingRight: '33px',
        fontSize: '15px',
        fontWeight: '600',
        backgroundColor: '#2196f3',
        color: '#fff',
        textDecoration: 'none'
    },
    linkText: {
        lineHeight: '0.8',
        color: '#8b909b',
        fontWeight: '600',
        textDecoration: 'none'
    },
    linkText1: {
        ineHeight: '0.8',
        color: '#000',
        fontWeight: '600',
        textDecoration: 'none'
    },
    linkText2: {
        fontSize: '14px',
        ineHeight: '0.8',
        color: '#259ee3',
        fontWeight: '600',
        textDecoration: 'none'
    },
    linkText3: {
        fontSize: '14px',
        ineHeight: '0.8',
        color: '#8b909b',
        fontWeight: '600',
        textDecoration: 'none'
    },
    btnTextMenu: {
        color: '#b1b1b1',
        display: 'inline-block',
        fontSize: '16px',
        lineHeight: '15px',
        padding: '12px 15px',
        fontWeight: '400',
        textDecoration: 'none'
    },
    btnTextMenu1: {
        backgroundColor: '#fff',
        color: '#000',
        display: 'inline-block',
        fontSize: '16px',
        lineHeight: '15px',
        padding: '12px 15px',
        fontWeight: '400',
        textDecoration: 'none'
    },
    btnRegisterMenu: {
        color: '#259ee3',
        display: 'inline-block',
        border: '2px solid',
        borderColor: '#259ee3',
        borderRadius: '3px',
        fontSize: '16px',
        lineHeight: '15px',
        padding: '10px 15px',
        fontWeight: '500',
        marginLeft: '15px',
        marginTop: '20px',
        textDecoration: 'none'
    },
    btnLoginMenu: {
        color: '#000',
        display: 'inline-block',
        border: '2px solid',
        borderColor: '#b1b1b1',
        borderRadius: '3px',
        fontSize: '16px',
        lineHeight: '15px',
        padding: '10px 15px',
        fontWeight: '500',
        marginLeft: '15px',
        marginTop: '20px',
        textDecoration: 'none'
    },
    boxRight: {
        marginTop: '10px',
        marginBottom: '32px',
        textAlign: 'right'
    },
    boxTop: {
        marginTop: '10px',
        marginBottom: '0',
        textAlign: 'right'
    },
    crumbsBox: {
        marginBottom: '20px'
    },
    linkBox: {
        display: 'inline-block',
        verticalAlign: 'top',
        marginTop: '-10px'
    },
    linkBox1: {
        display: 'inline-block'
    },
    rowLine: {
        borderBottom: '1px solid',
        color: '#dadada',
        marginBottom: '25px'
    },
    cf: {
        paddingBottom: '45px'
    },
    maxCol: {
        maxWidth: '1366px',
        margin: '0 auto'
    }
});

class HeaderPatient extends React.Component {
    render () {
        const { classes } = this.props;
        return (
            <section className={ classes.bdg }>
                <div className={ classes.sc }>
                    <div className={ classes.graf } />
                    <div className={ `${classes.bgt} container-fluid` }>
                        <div className={ `${classes.maxCol} row ` }>
                            <div className="col-xs-12 col-sm-12 col-md-12">
                                <div className={ classes.boxTop }>
                                    <a className={ classes.btnTextMenu1 } href="/loginpatient">
                    Strefa Pacjenta
                                    </a>
                                    <a className={ classes.btnTextMenu } href="/logindoctor">
                    Strefa Lekarza
                                    </a>
                                    <a className={ classes.btnTextMenu } href="/">
                    Strefa Biznesu
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className={ `${classes.rowLine} container-fluid ` }>
                        <div className={ `${classes.maxCol} row ` }>
                            <div className="col-xs-12 col-sm-12 col-md-3">
                                <a href="https://zdrowie.mymedic.com.pl/">
                                    <img src={ logo } className={ classes.logo } />
                                </a>
                            </div>
                            <div className="col-xs-12 col-sm-12 col-md-9">
                                <div className={ classes.boxRight }>
                                    <a className={ classes.btnTextMenu } href="/">
                    Aktualności
                                    </a>
                                    <a className={ classes.btnTextMenu } href="/">
                    Jak to działa
                                    </a>
                                    <a className={ classes.btnTextMenu } href="/">
                    Aplikacje
                                    </a>
                                    <a className={ classes.btnRegisterMenu } href="/registerpatient">
                    Załóż konto
                                    </a>
                                    <a className={ classes.btnLoginMenu } href="/loginpatient">
                    Zaloguj się
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        );
    }
}

export default withStyles(styles)(HeaderPatient);
