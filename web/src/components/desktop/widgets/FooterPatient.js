import React, { Component } from 'react';
import { withStyles } from '@material-ui/core/styles';

const styles = () => ({
    root: {
        textAlign: 'left',
        fontFamily: 'Poppins sans-serif',
        fontSize: '16px'
    },
    btnBlue: {
        border: 'none',
        borderRadius: '3px',
        paddingTop: '9px',
        paddingBottom: '9px',
        paddingLeft: '16px',
        paddingRight: '16px',
        fontSize: '14px',
        fontWeight: '600',
        backgroundColor: '#2196f3',
        color: '#fff',
        marginLeft: '5px',
        marginRight: '5px'
    },
    mgr: {
        margin: '0',
        padding: '0'
    }
});
class FooterPatient extends Component {
    render () {
        const { classes } = this.props;
        return (
<>
        <div className={ `${classes.mgr} row` }>
            <div class="col-md-3">
                <p>Serwis</p>
                <p>Zgłoś problem</p>
                <p>Napisz opinie</p>
                <p>Złóż rekomendacje</p>
                <p>Aplikacje</p>
            </div>
            <div class="col-md-3">
                <p>Pomoc</p>
                <p>Jak to działa?</p>
                <p>Regulamin</p>
                <p>Pomoc techniczna</p>
                <p>Pytania i odpowiedzi</p>
            </div>
            <div class="col-md-3">
                <p>Dla pacjentów</p>
                <p>Umów e-Wizytę</p>
                <p>Zamów e-Poradę</p>
                <p>Choroby</p>
                <p>Usługi i zabiegi</p>
                <p>Pomoc</p>
                <p>Usługi i zabiegi</p>
                <p>Aplikacje mobilne</p>
            </div>
            <div class="col-md-3">
                <p>Dla lekarzy</p>
                <p>Informacje</p>
                <p>Cennik</p>
                <p>Warunki współpracy</p>
                <p>Vademecum leków</p>
            </div>
        </div>
        <div className={ `${classes.mr} row` }>
            <div class="col-md-6">
            Wszelkie prawa zastrzeżone
            </div>
            <div class="col-md-6">
                <button className={ classes.btnBlue } href="/">
              Do góry
                </button>
            </div>
        </div>
</>
        );
    }
}

export default withStyles(styles)(FooterPatient);
