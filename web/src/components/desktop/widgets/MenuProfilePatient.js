import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import Collapse from '@material-ui/core/Collapse';
import ExpandLess from '@material-ui/icons/ExpandLess';
import ExpandMore from '@material-ui/icons/ExpandMore';
import logo from '../pages/images/logo-profile.png';
import ewizyty from '../pages/images/e-wizyty.png';
import eporady from '../pages/images/message_white.png';
import doc from '../pages/images/doc.png';
import pomiary from '../pages/images/pomiary.png';
import leki from '../pages/images/leki.png';
import lekarze from '../pages/images/serce_white.png';
import szukaj from '../pages/images/szukaj.png';
import problem from '../pages/images/problem.png';
import review from '../pages/images/review.png';
import complaint from '../pages/images/complaint.png';
import support from '../pages/images/support.png';
import historia from '../pages/images/historia_white.png';
import message from '../pages/images/mail_white.png';
import logout from '../pages/images/logout.png';
import recommend from '../pages/images/recommend.png';
import manage from '../pages/images/manage.png';
import payment from '../pages/images/payment.png';
import functions from '../pages/images/functions.png';
import regulations from '../pages/images/regulations.png';
import key from '../pages/images/key.png';

const styles = theme => ({
    root: {
        width: '295px',
        height: '100%',
        backgroundColor: '#242E41',
        fontFamily: 'Poppins',
        fontSize: '14px',
        color: '#fff'
    },
    nested: {
        paddingLeft: theme.spacing.unit * 4
    },
    logo: {
        backgroundImage: `url(${logo})`,
        height: '40px',
        width: '223px',
        backgroundSize: 'contain',
        backgroundRepeat: 'no-repeat',
        backgroundPosition: 'left',
        marginTop: '22px',
        marginBottom: '22px',
        marginLeft: '35px'
    },
    avatar: {
        borderRadius: '50%',
        marginLeft: '15px'
    },
    textActive: {
        display: 'innline-block',
        color: 'rgba(33, 150, 243, 255)',
        fontSize: '14px',
        paddingLeft: '20px',
        paddingRight: '5x',
        width: '166px'
    },
    textMenu1: {
        display: 'innline-block',
        color: '#fff',
        fontSize: '14px',
        paddingLeft: '20px',
        paddingRight: '5x',
        width: '166px'
    },
    textMenu2: {
        display: 'innline-block',
        color: '#fff',
        fontSize: '14px',
        paddingLeft: '20px',
        paddingRight: '10px',
        width: '200px'
    },
    textMenu3: {
        display: 'innline-block',
        color: '#40c4ff',
        fontSize: '14px',
        paddingLeft: '20px',
        paddingRight: '10px',
        width: '200px'
    },
    textMenu4: {
        display: 'innline-block',
        color: 'rgba(33, 150, 243, 255)',
        fontSize: '14px',
        paddingLeft: '20px',
        paddingRight: '10px',
        width: '200px'
    },
    itemColor: {
        backgroundColor: '#3b4354'
    },
    barLine1: {
        display: 'block',
        backgroundColor: '#2196f3',
        borderBottom: '1px solid #242E41',
        color: '#fff',
        fontSize: '12px',
        paddingTop: '5px',
        paddingBottom: '5px',
        paddingLeft: '35px',
        fontWeight: '600',
        Width: '100%'
    },
    barLine2: {
        display: 'block',
        backgroundColor: '#40c4ff',
        borderBottom: '1px solid #242E41',
        color: '#fff',
        fontSize: '12px',
        paddingTop: '5px',
        paddingBottom: '5px',
        paddingLeft: '35px',
        fontWeight: '600',
        Width: '100%'
    },
    barLine3: {
        display: 'block',
        backgroundColor: '#01b5b6',
        borderBottom: '1px solid #242E41',
        color: '#fff',
        fontSize: '12px',
        paddingTop: '5px',
        paddingBottom: '5px',
        paddingLeft: '35px',
        fontWeight: '600',
        Width: '100%'
    },
    ewizyty: {
        backgroundImage: `url(${ewizyty})`,
        width: '18px',
        height: '16px',
        backgroundSize: 'cover',
        backgroundPosition: 'center center',
        backgroundRepeat: 'none'
    },
    eporady: {
        backgroundImage: `url(${eporady})`,
        width: '19px',
        height: '18px',
        backgroundSize: 'cover',
        backgroundPosition: 'center center',
        backgroundRepeat: 'none'
    },
    doc: {
        backgroundImage: `url(${doc})`,
        width: '20px',
        height: '16px',
        backgroundSize: 'cover',
        backgroundPosition: 'center center',
        backgroundRepeat: 'none'
    },
    pomiary: {
        backgroundImage: `url(${pomiary})`,
        width: '17px',
        height: '17px',
        backgroundSize: 'cover',
        backgroundPosition: 'center center',
        backgroundRepeat: 'none'
    },
    leki: {
        backgroundImage: `url(${leki})`,
        width: '15px',
        height: '16px',
        backgroundSize: 'cover',
        backgroundPosition: 'center center',
        backgroundRepeat: 'none'
    },
    lekarze: {
        backgroundImage: `url(${lekarze})`,
        width: '18px',
        height: '16px',
        backgroundSize: 'cover',
        backgroundPosition: 'center center',
        backgroundRepeat: 'none'
    },
    szukaj: {
        backgroundImage: `url(${szukaj})`,
        width: '16px',
        height: '17px',
        backgroundSize: 'cover',
        backgroundPosition: 'center center',
        backgroundRepeat: 'none'
    },
    problem: {
        backgroundImage: `url(${problem})`,
        width: '16px',
        height: '17px',
        backgroundSize: 'cover',
        backgroundPosition: 'center center',
        backgroundRepeat: 'none'
    },
    review: {
        backgroundImage: `url(${review})`,
        width: '16px',
        height: '17px',
        backgroundSize: 'cover',
        backgroundPosition: 'center center',
        backgroundRepeat: 'none'
    },
    complaint: {
        backgroundImage: `url(${complaint})`,
        width: '16px',
        height: '16px',
        backgroundSize: 'cover',
        backgroundPosition: 'center center',
        backgroundRepeat: 'none'
    },
    support: {
        backgroundImage: `url(${support})`,
        width: '16px',
        height: '16px',
        backgroundSize: 'cover',
        backgroundPosition: 'center center',
        backgroundRepeat: 'none'
    },
    historia: {
        backgroundImage: `url(${historia})`,
        width: '19px',
        height: '16px',
        backgroundSize: 'cover',
        backgroundPosition: 'center center',
        backgroundRepeat: 'none'
    },
    message: {
        backgroundImage: `url(${message})`,
        width: '20px',
        height: '14px',
        backgroundSize: 'cover',
        backgroundPosition: 'center center',
        backgroundRepeat: 'none'
    },
    logout: {
        backgroundImage: `url(${logout})`,
        width: '18px',
        height: '18px',
        backgroundSize: 'cover',
        backgroundPosition: 'center center',
        backgroundRepeat: 'none'
    },
    recommend: {
        backgroundImage: `url(${recommend})`,
        width: '18px',
        height: '17px',
        backgroundSize: 'cover',
        backgroundPosition: 'center center',
        backgroundRepeat: 'none'
    },
    manage: {
        backgroundImage: `url(${manage})`,
        width: '19px',
        height: '18px',
        backgroundSize: 'cover',
        backgroundPosition: 'center center',
        backgroundRepeat: 'none'
    },
    payment: {
        backgroundImage: `url(${payment})`,
        width: '20px',
        height: '15px',
        backgroundSize: 'cover',
        backgroundPosition: 'center center',
        backgroundRepeat: 'none'
    },
    functions: {
        backgroundImage: `url(${functions})`,
        width: '16px',
        height: '24px',
        backgroundSize: 'cover',
        backgroundPosition: 'center center',
        backgroundRepeat: 'none'
    },
    regulations: {
        backgroundImage: `url(${regulations})`,
        width: '16px',
        height: '21px',
        backgroundSize: 'cover',
        backgroundPosition: 'center center',
        backgroundRepeat: 'none'
    },
    key: {
        backgroundImage: `url(${key})`,
        width: '18px',
        height: '18px',
        backgroundSize: 'cover',
        backgroundPosition: 'center center',
        backgroundRepeat: 'none'
    }
});

class MenuProfilePatient extends React.Component {
    constructor () {
        super();
        this.state = {
            // open1: true,
            open2: false,
            open3: false
        };

        // this.profile = () => {
        //     this.setState(state => ({ open1: !state.open1 }));
        // };

        this.help = () => {
            this.setState(state => ({ open2: !state.open2 }));
        };

        this.settings = () => {
            this.setState(state => ({ open3: !state.open3 }));
        };
    }
    render () {
        const { classes } = this.props;
        // const urlAvatar = this.props.patientStore.getAvatar("http://via.placeholder.com/35x35");
        const urlAvatar = 'http://zdrowie.mymedic.com.pl/wp-content/uploads/2019/03/avatar.png';
        const namePatient = 'Anna Kowalska';

        return (
    <>
      <List component="nav" className={ classes.root }>

          <div className={ classes.logo } />

          <ListItem style={ { backgroundColor: '#3b4354', borderBottom: '1px solid #242E41' } } button onClick={ this.profile }>
              <div className={ classes.avatar } style={ {
                  width: '30px',
                  height: '30px',
                  backgroundSize: 'cover',
                  backgroundPosition: 'center center',
                  backgroundRepeat: 'none',
                  backgroundImage: 'url(' + urlAvatar + ')' } }
              />
              <span className={ classes.textActive }>{namePatient}</span>
              {this.state.open1 ? <ExpandLess /> : <ExpandMore />}
          </ListItem>
          {/* <Collapse in={ this.state.open1 } timeout="auto" unmountOnExit> */}
          <span className={ classes.barLine1 }>E-KONSULTACJE</span>
          <List component="div" disablePadding>
              <ListItem button className={ classes.nested }>
                  <div className={ classes.ewizyty } />
                  <span className={ classes.textMenu2 }>Moje e-Wizyty</span>
              </ListItem>
              <ListItem button className={ classes.nested } style={ { backgroundColor: '#2c3647' } }>
                  <div className={ classes.eporady } />
                  <span className={ classes.textMenu2 }>Moje e-Porady</span>
              </ListItem>
              <ListItem button className={ classes.nested }>
                  <div className={ classes.message } />
                  <span className={ classes.textMenu2 }>Wiadomości</span>
              </ListItem>
          </List>
          <span className={ classes.barLine2 }>ZDROWIE</span>
          <List component="div" disablePadding>
              <ListItem button className={ classes.nested }>
                  <div className={ classes.historia } />
                  <span className={ classes.textMenu2 }>Moja historia zdrowia</span>
              </ListItem>
              <ListItem button className={ classes.nested } style={ { backgroundColor: '#2c3647' } }>
                  <div className={ classes.doc } />
                  <span className={ classes.textMenu2 }>Moje dokumenty</span>
              </ListItem>
              <ListItem button className={ classes.nested }>
                  <div className={ classes.pomiary } />
                  <span className={ classes.textMenu2 }>Moje pomiary</span>
              </ListItem>
              <ListItem button className={ classes.nested } style={ { backgroundColor: '#2c3647' } }>
                  <div className={ classes.leki } />
                  <span className={ classes.textMenu2 }>Moje leki</span>
              </ListItem>
          </List>
          <span className={ classes.barLine3 }>LEKARZ</span>
          <List component="div" disablePadding>
              <ListItem button className={ classes.nested }>
                  <div className={ classes.lekarze } />
                  <span className={ classes.textMenu2 }>Ulubieni lekarze</span>
              </ListItem>
              <ListItem button className={ classes.nested } style={ { backgroundColor: '#2c3647' } }>
                  <div className={ classes.szukaj } />
                  <span className={ classes.textMenu2 }>Szukaj lekarza</span>
              </ListItem>
          </List>
          {/* </Collapse> */}

          <ListItem style={ { backgroundColor: '#3b4354', borderBottom: '1px solid #242E41' } } button onClick={ this.help }>
              <span className={ classes.textMenu2 }>Pomoc</span>
              {this.state.open2 ? <ExpandLess /> : <ExpandMore />}
          </ListItem>
          <Collapse in={ this.state.open2 } timeout="auto" unmountOnExit>
              <List component="div" disablePadding>
                  <ListItem button className={ classes.nested }>
                      <div className={ classes.problem } />
                      <span className={ classes.textMenu2 }>Zgłoś problem</span>
                  </ListItem>
                  <ListItem button className={ classes.nested } style={ { backgroundColor: '#2c3647' } }>
                      <div className={ classes.review } />
                      <span className={ classes.textMenu2 }>Napisz opinię</span>
                  </ListItem>
                  <ListItem button className={ classes.nested }>
                      <div className={ classes.complaint } />
                      <span className={ classes.textMenu2 }>Zgłoś reklamację</span>
                  </ListItem>
                  <ListItem button className={ classes.nested } style={ { backgroundColor: '#2c3647' } }>
                      <div className={ classes.support } />
                      <span className={ classes.textMenu2 }>Pomoc techniczna</span>
                  </ListItem>
              </List>
          </Collapse>

          <ListItem style={ { backgroundColor: '#3b4354', borderBottom: '1px solid #242E41' } } button onClick={ this.settings }>
              <span className={ classes.textMenu2 }>Ustawienia</span>
              {this.state.open3 ? <ExpandLess /> : <ExpandMore />}
          </ListItem>
          <Collapse in={ this.state.open3 } timeout="auto" unmountOnExit>
              <List component="div" disablePadding>
                  <ListItem button className={ classes.nested }>
                      <div className={ classes.recommend } />
                      <span className={ classes.textMenu2 }>Poleć aplikację Zdrowie Plus</span>
                  </ListItem>
                  <ListItem button className={ classes.nested } style={ { backgroundColor: '#2c3647' } }>
                      <div className={ classes.manage } />
                      <span className={ classes.textMenu2 }>Zarządzaj kontami</span>
                  </ListItem>
                  <ListItem button className={ classes.nested }>
                      <div className={ classes.payment } />
                      <span className={ classes.textMenu2 }>Moje płatności</span>
                  </ListItem>
                  <ListItem button className={ classes.nested } style={ { backgroundColor: '#2c3647' } }>
                      <div className={ classes.functions } />
                      <span className={ classes.textMenu2 }>Funkcje aplikacji</span>
                  </ListItem>
                  <ListItem button className={ classes.nested }>
                      <div className={ classes.regulations } />
                      <span className={ classes.textMenu2 }>Regulamin</span>
                  </ListItem>
                  <ListItem button className={ classes.nested } style={ { backgroundColor: '#2c3647' } }>
                      <div className={ classes.key } />
                      <span className={ classes.textMenu2 }>Zmień hasło</span>
                  </ListItem>
              </List>
          </Collapse>
          <ListItem button className={ classes.nested }>
              <div className={ classes.logout } />
              <span className={ classes.textMenu4 }>Wyloguj się</span>
          </ListItem>
      </List>
    </>
        );
    }
}

MenuProfilePatient.propTypes = {
    classes: PropTypes.object.isRequired
};

export default withStyles(styles)(MenuProfilePatient);
