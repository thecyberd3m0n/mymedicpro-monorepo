import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import foto from '../pages/images/foto.png';
import alert from '../pages/images/alert2.png';
import geogreen from '../pages/images/geogreen.png';
import ranking from '../pages/images/ranking.png';

const styles = () => ({
    opavatar: {
        borderRadius: '50%',
        marginLeft: '15px'
    },
    avatar: {
        width: '154px',
        height: '154px',
        backgroundSize: 'cover',
        backgroundPosition: 'center center',
        backgroundRepeat: 'none',
        borderTop: '5px solid #01b5b6',
        borderLeft: '5px solid #01b5b6',
        borderRight: '5px solid #fff',
        borderBottom: '5px solid #01b5b6',
        borderRadius: '50%'
    },
    avatarBox: {
        display: 'inline-block',
        padding: '20px',
        marginTop: '-40px',
        marginRight: '40px'
    },
    foto: {
        backgroundImage: `url(${foto})`,
        height: '42px',
        width: '42px',
        backgroundSize: 'contain',
        backgroundRepeat: 'no-repeat',
        backgroundPosition: 'center',
        position: 'relative',
        top: '155px',
        left: '118px'
    },
    procent: {
        color: '#01b5b6',
        margin: '2px',
        fontSize: '20px',
        fontWeight: '600',
        position: 'relative',
        top: '40px',
        left: '60px'
    },
    namePatient: {
        fontSize: '20px',
        color: 'rgba(32, 32, 32, 255)'
    },
    headerProfile: {
        backgroundColor: '#0eb9ba',
        textAlign: 'right',
        width: '100%',
        borderBottom: '1px solid #dadada',
        paddingTop: '20px',
        paddingBottom: '20px',
        margin: '0'
    },
    personalEdit: {
        display: 'block'
    },
    contentBox: {
        border: '1px solid #dadada',
        margin: '20px'
    },
    paramL: {
        display: 'inline-block',
        borderLeft: '1px solid #dadada',
        borderTop: '1px solid #dadada',
        paddingTop: '10px',
        paddingLeft: '20px',
        paddingBottom: '10px',
        paddingRight: '20px',
        borderBottom: '1px solid #dadada',
        width: '65px'
    },
    paramR: {
        display: 'inline-block',
        borderLeft: '1px solid #dadada',
        borderRight: '1px solid #dadada',
        borderTop: '1px solid #dadada',
        paddingTop: '10px',
        paddingLeft: '20px',
        paddingBottom: '10px',
        paddingRight: '20px',
        borderBottom: '1px solid #dadada',
        width: '65px'
    },
    btnBlue: {
        border: 'none',
        borderRadius: '3px',
        paddingTop: '9px',
        paddingBottom: '9px',
        paddingLeft: '16px',
        paddingRight: '16px',
        fontSize: '14px',
        fontWeight: '600',
        backgroundColor: '#2196f3',
        color: '#fff',
        marginLeft: '5px',
        marginRight: '5px'
    },
    tb: {
        fontFamily: 'Poppins',
        borderCollapse: 'collapse',
        border: '1px solid #dadada',
        fontSize: '14px',
        margin: '40px',
        width: '95%',
        maxWidth: '1525px'
    },
    br: {
        backgroundColor: '#f5f5f5',
        border: '1px solid #dadada'
    },
    btr: {
        paddingTop: '10px',
        paddingLeft: '20px',
        paddingBottom: '10px',
        paddingRight: '20px',
        verticalAlign: 'middle',
        fontWeight: '600'
    },
    btnr: {
        paddingTop: '10px',
        paddingLeft: '20px',
        paddingBottom: '10px',
        paddingRight: '20px',
        verticalAlign: 'middle',
        textAlign: 'right',
        fontWeight: '600'
    },
    bt: {
        border: '1px solid #dadada',
        paddingTop: '10px',
        paddingLeft: '20px',
        paddingBottom: '10px',
        paddingRight: '20px',
        verticalAlign: 'middle',
        fontWeight: '600'
    },
    p1: {
        color: 'rgba(178, 178, 178, 255)',
        marginTop: '2px',
        marginBottom: '2px'
    },
    p2: {
        marginTop: '2px',
        marginBottom: '2px'
    },
    p3: {
        marginTop: '2px',
        marginBottom: '2px',
        fontSize: '20px',
        fontWeight: '600'
    },
    textName: {
        fontSize: '20px',
        marginBottom: '10px',
        fontWeight: '600'
    },
    textSpec: {
        display: 'inline-block',
        fontSize: '20px',
        marginBottom: '20px',
        color: '#2196f3',
        fontWeight: '600'
    },
    textLoc: {
        display: 'inline-block',
        fontSize: '20px',
        marginBottom: '20px',
        color: '#01b5b6',
        fontWeight: '600'
    },
    textgreen: {
        color: '#01b5b6',
        marginTop: '2px',
        marginBottom: '2px',
        fontSize: '16px',
        fontWeight: '600'
    },
    textgrey: {
        color: 'rgba(178, 178, 178, 255)',
        marginTop: '2px',
        marginBottom: '2px',
        fontSize: '16px',
        fontWeight: '600'
    },
    textblue: {
        color: '#2196f3',
        marginTop: '2px',
        marginBottom: '2px',
        fontSize: '16px',
        fontWeight: '600'
    },
    textred: {
        color: 'red',
        marginTop: '2px',
        marginBottom: '2px',
        fontSize: '16px',
        fontWeight: '600'
    },
    linkBlue: {
        color: '#2196f3',
        marginTop: '2px',
        marginBottom: '2px',
        fontSize: '15px',
        textDecoration: 'none',
        marginLeft: '30px',
        marginRight: '30px',
        fontWeight: '600'
    },
    linkWhite: {
        color: '#fff',
        border: '1px solid #fff',
        borderRadius: '3px',
        paddingTop: '10px',
        paddingLeft: '12px',
        paddingRight: '12px',
        paddingBottom: '10px',
        marginTop: '2px',
        marginBottom: '2px',
        fontSize: '15px',
        textDecoration: 'none',
        marginLeft: '30px',
        marginRight: '30px',
        fontWeight: '600'
    },
    linkBlack: {
        color: '#000',
        marginTop: '2px',
        marginBottom: '2px',
        fontSize: '15px',
        textDecoration: 'none',
        marginLeft: '30px',
        marginRight: '15px',
        fontWeight: '600'
    },
    alertBox: {
        display: 'inline-block',
        border: '1px solid #dadada',
        width: '35px',
        height: '35px',
        borderRadius: '50%',
        textAlign: 'center',
        verticalAlign: 'middle',
        marginRight: '40px'
    },
    alertRed: {
        backgroundColor: 'red',
        fontSize: '13px',
        color: '#fff',
        width: '22px',
        height: '22px',
        borderRadius: '50%',
        position: 'relative',
        paddingTop: '1px',
        right: '-25px',
        top: '-40px'
    },
    alert: {
        backgroundImage: `url(${alert})`,
        height: '20px',
        width: '16px',
        backgroundSize: 'contain',
        backgroundRepeat: 'no-repeat',
        backgroundPosition: 'center',
        margin: '7px auto'
    },
    geogreen: {
        display: 'inline-block',
        backgroundImage: `url(${geogreen})`,
        height: '18px',
        width: '14px',
        backgroundSize: 'contain',
        backgroundRepeat: 'no-repeat',
        backgroundPosition: 'center',
        marginLeft: '40px',
        marginRight: '10px'
    },
    ranking: {
        display: 'inline-block',
        backgroundImage: `url(${ranking})`,
        height: '15px',
        width: '15px',
        backgroundSize: 'contain',
        backgroundRepeat: 'no-repeat',
        backgroundPosition: 'center',
        marginRight: '3px'
    },
    span1: {
        color: '#cccccc',
        fontSize: '16px'
    },
    ulbox: {
        listStyle: 'none'
    },
    libox: {
        border: '1px solid #01b5b6',
        display: 'inline-block',
        fontSize: '14px',
        fontWeight: '600',
        margin: '5px',
        padding: '10px'
    }
});

class EditProfileDoctor extends React.Component {
    render () {
        const { classes } = this.props;

        // const urlAvatar = this.props.patientStore.getAvatar("http://via.placeholder.com/35x35");
        const urlAvatar = 'http://zdrowie.mymedic.com.pl/wp-content/uploads/2019/03/avatar3.png';
        const nameDoctor = 'dr Jan Kowalski';
        const specialization = 'laryngolog';
        const location = 'Warszawa';

        return (
        <>
            <div className={ classes.headerProfile }>
                <a className={ classes.linkBlack } href="/">Poleć aplikację </a>
                <span className={ classes.linkWhite }>Powiadomienia </span>
                <div className={ classes.alertBox }>
                    <div className={ classes.alert } />
                    <div className={ classes.alertRed }>
                        <span>12</span>
                    </div>
                </div>
            </div>

            <table className={ classes.tb }>
                <tbody>
                    <tr className={ classes.bt }>
                        <td className={ classes.bt } colspan="3">
                            <table>
                                <tbody>
                                    <tr>
                                        <td>
                                            <div className={ classes.avatarBox }>
                                                <div className={ classes.procent }>75%</div>
                                                <div className={ classes.foto } />
                                                <div className={ classes.avatar } style={ {
                                                    backgroundImage: 'url(' + urlAvatar + ')' } }
                                                />
                                            </div>
                                        </td>
                                        <td>
                                            <div className={ classes.personalEdit }>
                                                <div className={ classes.textName }>{nameDoctor}</div>
                                                <div className={ classes.textSpec }>{specialization}</div>
                                                <div className={ classes.geogreen } />
                                                <div className={ classes.textLoc }>{location}</div>
                                            </div>
                                            <div className={ classes.ranking } />
                                            <div className={ classes.ranking } />
                                            <div className={ classes.ranking } />
                                            <div className={ classes.ranking } />
                                            <div className={ classes.ranking } />
                                            <span className={ classes.span1 }> (400)</span>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </td>
                    </tr>
                    <tr className={ classes.br }>
                        <td className={ classes.btr } colspan="2">
                  Dane osobowe
                        </td>
                        <td className={ classes.btnr }>
                            <button className={ classes.btnBlue } href="/">Edytuj</button>
                        </td>
                    </tr>
                    <tr className={ classes.bt }>
                        <td className={ classes.bt }>
                            <p className={ classes.p1 }>Imię *</p>
                            <p className={ classes.p2 }>Jan</p>
                        </td>
                        <td className={ classes.bt }>
                            <p className={ classes.p1 }>Nazwisko *</p>
                            <p className={ classes.p2 }>Kowalski</p>
                        </td>
                        <td className={ classes.bt }>
                            <p className={ classes.p1 }>PESEL</p>
                            <p className={ classes.p2 }>94071200343</p>
                        </td>
                    </tr>
                    <tr className={ classes.bt }>
                        <td className={ classes.bt } colspan="3">
                            <p className={ classes.p1 }>Opis lekarza *</p>
                            <p className={ classes.p2 }>
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor
                    incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud
                    exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute
                    irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.
                            </p>
                        </td>
                    </tr>
                    <tr className={ classes.br }>
                        <td className={ classes.btr } colspan="2">
                  Dane adresowe
                        </td>
                        <td className={ classes.btnr }>
                            <button className={ classes.btnBlue } href="/">Edytuj</button>
                        </td>
                    </tr>
                    <tr className={ classes.bt }>
                        <td className={ classes.bt }>
                            <p className={ classes.p1 }>Ulica</p>
                            <p className={ classes.p2 }>Aleje Niepodległości</p>
                        </td>
                        <td className={ classes.bt }>
                            <p className={ classes.p1 }>Numer domu</p>
                            <p className={ classes.p2 }>18</p>
                        </td>
                        <td className={ classes.bt }>
                            <p className={ classes.p1 }>Numer lokalu</p>
                            <p className={ classes.p2 }>2</p>
                        </td>
                    </tr>
                    <tr className={ classes.bt }>
                        <td className={ classes.bt }>
                            <p className={ classes.p1 }>Miasto</p>
                            <p className={ classes.p2 }>Warszawa</p>
                        </td>
                        <td className={ classes.bt } colspan="2">
                            <p className={ classes.p1 }>Kod pocztowy</p>
                            <p className={ classes.p2 }>00-950</p>
                        </td>
                    </tr>
                    <tr className={ classes.br }>
                        <td className={ classes.btr } colspan="2">
                  Kwalifikacja zawodowa
                        </td>
                        <td className={ classes.btnr }>
                            <button className={ classes.btnBlue } href="/">Edytuj</button>
                        </td>
                    </tr>
                    <tr className={ classes.bt }>
                        <td className={ classes.bt }>
                            <p className={ classes.p1 }>Tytuł naukowy *</p>
                            <p className={ classes.p2 }>Lekarz medyczny</p>
                        </td>
                        <td className={ classes.bt } colspan="2">
                            <p className={ classes.p1 }>Numer PWZ *</p>
                            <p className={ classes.p2 }>00000000</p>
                        </td>
                    </tr>
                    <tr className={ classes.br }>
                        <td className={ classes.btr } colspan="2">
                  Specjalizacje
                        </td>
                        <td className={ classes.btnr }>
                            <button className={ classes.btnBlue } href="/">Edytuj</button>
                        </td>
                    </tr>
                    <tr className={ classes.bt }>
                        <td className={ classes.bt } colspan="3">
                            <ul className={ classes.ulbox }>
                                <li className={ classes.libox }>Okulista</li>
                                <li className={ classes.libox }>Chirurg plastyczny</li>
                                <li className={ classes.libox }>Kardiolog</li>
                            </ul>
                        </td>
                    </tr>
                    <tr className={ classes.br }>
                        <td className={ classes.btr } colspan="2">
                  Leczone choroby
                        </td>
                        <td className={ classes.btnr }>
                            <button className={ classes.btnBlue } href="/">Edytuj</button>
                        </td>
                    </tr>
                    <tr className={ classes.bt }>
                        <td className={ classes.bt } colspan="3">
                            <ul className={ classes.ulbox }>
                                <li className={ classes.libox }>Alergia</li>
                                <li className={ classes.libox }>Zapalenie oskrzeli</li>
                            </ul>
                        </td>
                    </tr>
                    <tr className={ classes.br }>
                        <td className={ classes.btr } colspan="2">
                  Praktyka lekarska
                        </td>
                        <td className={ classes.btnr }>
                            <button className={ classes.btnBlue } href="/">Edytuj</button>
                        </td>
                    </tr>
                    <tr className={ classes.bt }>
                        <td className={ classes.bt }>
                            <p className={ classes.p1 }>Nazwa praktyki lekarskiej *</p>
                            <p className={ classes.p2 }>Nowa praktyka lekarska</p>
                        </td>
                        <td className={ classes.bt }>
                            <p className={ classes.p1 }>Ulica i numer domu / lokalu *</p>
                            <p className={ classes.p2 }>Miodowa 5 /13</p>
                        </td>
                        <td className={ classes.bt }>
                            <p className={ classes.p1 }>Miasto i kod pocztowy</p>
                            <p className={ classes.p2 }>03-555 Warszawa</p>
                        </td>
                    </tr>
                    <tr className={ classes.bt }>
                        <td className={ classes.bt }>
                            <p className={ classes.p1 }>NIP *</p>
                            <p className={ classes.p2 }>00000000</p>
                        </td>
                        <td className={ classes.bt } colspan="2">
                            <p className={ classes.p1 }>REGON *</p>
                            <p className={ classes.p2 }>00000000000</p>
                        </td>
                    </tr>
                    <tr className={ classes.br }>
                        <td className={ classes.btr } colspan="2">
                  Preferencje odnośnie e-Wizyt
                        </td>
                        <td className={ classes.btnr }>
                            <button className={ classes.btnBlue } href="/">Edytuj</button>
                        </td>
                    </tr>
                    <tr className={ classes.bt }>
                        <td className={ classes.bt } colspan="3">
                            <ul className={ classes.ulbox }>
                                <li className={ classes.libox }>Dorośli</li>
                                <li className={ classes.libox }>Dzieci poniżej 16 lat</li>
                            </ul>
                        </td>
                    </tr>
                    <tr className={ classes.br }>
                        <td className={ classes.btr } colspan="2">
                  Preferencje odnośnie języków
                        </td>
                        <td className={ classes.btnr }>
                            <button className={ classes.btnBlue } href="/">Edytuj</button>
                        </td>
                    </tr>
                    <tr className={ classes.bt }>
                        <td className={ classes.bt } colspan="3">
                            <ul className={ classes.ulbox }>
                                <li className={ classes.libox }>Polski</li>
                                <li className={ classes.libox }>Hiszpański</li>
                            </ul>
                        </td>
                    </tr>
                </tbody>
            </table>
        </>
        );
    }
}

export default withStyles(styles)(EditProfileDoctor);
