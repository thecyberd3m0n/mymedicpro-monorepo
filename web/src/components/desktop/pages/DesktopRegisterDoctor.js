import React, { Component } from 'react';
import { withStyles } from '@material-ui/core/styles';
import logo from './images/logo-profile.png';
import login from './images/img-login2.png';
import graf from './images/graf.png';
import geo from './images/geo.png';
import nextArrow from './images/next.png';
import Checkbox from '@material-ui/core/Checkbox';

const styles = () => ({
    root: {
        textAlign: 'left',
        fontFamily: 'Poppins',
        fontSize: '16px'
    },
    bdg: {
        paddingBottom: '85px',
        width: '100%'
    },
    bgt: {
        backgroundColor: '#f1f1f1'
    },
    sc: {
        backgroundColor: '#fff',
        width: '100%',
        height: '100%',
        margin: '0 auto',
        marginBottom: '5px'
    },
    inputs: {
        border: '1px solid',
        borderColor: '#cccccc',
        color: '#000',
        borderRadius: '3px',
        marginTop: '6px',
        marginBottom: '6px',
        padding: '12px',
        fontSize: '15px',
        width: '92%'
    },
    logo: {
        height: '40px',
        width: '222px',
        marginTop: '28px',
        marginLeft: '0',
        marginBottom: '0'
    },
    elementMenu: {
        display: 'inline-block',
        height: '6vh',
        marginTop: '5vh',
        marginBottom: '3vh',
        marginRight: '15px',
        paddingTop: '15px',
        paddingBottom: '15px',
        color: '#8b909b',
        fontWeight: '600',
        textDecoration: 'none'
    },
    login: {
        backgroundImage: `url(${login})`,
        height: '72px',
        width: '230px',
        backgroundSize: 'contain',
        backgroundRepeat: 'no-repeat',
        backgroundPosition: 'center'
    },
    graf: {
        backgroundImage: `url(${graf})`,
        height: '340px',
        width: '180px',
        backgroundSize: 'contain',
        backgroundRepeat: 'no-repeat',
        backgroundPosition: 'center',
        position: 'absolute',
        top: '45%',
        right: '2%'
    },
    geo: {
        display: 'inline-block',
        backgroundImage: `url(${geo})`,
        height: '15px',
        width: '13px',
        backgroundSize: 'contain',
        backgroundRepeat: 'no-repeat',
        backgroundPosition: 'center',
        marginRight: '4px',
        verticalAlign: 'middle'
    },
    nextArrow: {
        display: 'inline-block',
        backgroundImage: `url(${nextArrow})`,
        height: '18px',
        width: '18px',
        backgroundSize: 'contain',
        backgroundRepeat: 'no-repeat',
        backgroundPosition: 'center',
        marginRight: '4px',
        verticalAlign: 'middle'
    },
    bg: {
        width: '100%',
        height: '100%',
        backgroundColor: '#f1f1f1'
    },
    borderBox: {
        maxWidth: '952px',
        border: '1px solid #dadada',
        color: '#dadada'
    },
    formBox: {
        textAlign: 'center',
        paddingTop: '20px',
        paddingLeft: '85px',
        paddingRight: '85px',
        paddingBottom: '20px'
    },
    formBox1: {
        textAlign: 'left',
        paddingLeft: '85px',
        paddingRight: '85px',
        paddingBottom: '20px'
    },
    formBox2: {
        textAlign: 'center',
        paddingTop: '0',
        paddingLeft: '85px',
        paddingRight: '85px',
        paddingBottom: '20px'
    },
    centerBox: {
        width: '230px',
        height: '230px',
        margin: '176px auto'
    },
    span1: {
        color: '#383838',
        display: 'inline',
        fontWeight: '700',
        fontSize: '20px'
    },
    span2: {
        color: '#43ceba',
        fontWeight: '700',
        fontSize: '20px'
    },
    span3: {
        color: '#383838'
    },
    p1: {
        color: '#383838',
        fontWeight: '700',
        fontSize: '20px'
    },
    btnForm: {
        border: 'none',
        borderRadius: '3px',
        width: '100%',
        paddingTop: '12px',
        paddingBottom: '12px',
        fontSize: '15px',
        fontWeight: '600',
        backgroundColor: '#0eb9ba',
        color: '#fff',
        marginBottom: '15px'
    },
    btnLogin: {
        border: 'none',
        borderRadius: '3px',
        paddingTop: '12px',
        paddingBottom: '12px',
        paddingLeft: '33px',
        paddingRight: '33px',
        fontSize: '15px',
        fontWeight: '600',
        backgroundColor: '#2196f3',
        color: '#fff',
        textDecoration: 'none'
    },
    linkText: {
        fontSize: '15px',
        lineHeight: '0.8',
        color: '#8b909b',
        fontWeight: '600',
        textDecoration: 'none'
    },
    linkText1: {
        fontSize: '15px',
        lineHeight: '0.8',
        color: '#000',
        fontWeight: '600',
        textDecoration: 'none'
    },
    linkText2: {
        fontSize: '14px',
        ineHeight: '0.8',
        color: '#259ee3',
        fontWeight: '600',
        textDecoration: 'none'
    },
    linkText3: {
        fontSize: '14px',
        ineHeight: '0.8',
        color: '#8b909b',
        fontWeight: '600',
        textDecoration: 'none'
    },
    btnTextMenu: {
        color: '#fff',
        display: 'inline-block',
        fontSize: '16px',
        lineHeight: '15px',
        padding: '12px 15px',
        fontWeight: '400',
        textDecoration: 'none'
    },
    btnTextMenu1: {
        backgroundColor: '#0eb9ba',
        color: '#fff',
        display: 'inline-block',
        fontSize: '16px',
        lineHeight: '15px',
        padding: '12px 15px',
        fontWeight: '400',
        textDecoration: 'none'
    },
    btnTextMenu2: {
        color: '#000',
        display: 'inline-block',
        fontSize: '16px',
        lineHeight: '15px',
        padding: '12px 15px',
        fontWeight: '400',
        textDecoration: 'none'
    },
    btnRegisterMenu: {
        color: '#fff',
        display: 'inline-block',
        border: '2px solid',
        borderColor: '#fff',
        borderRadius: '3px',
        fontSize: '16px',
        lineHeight: '15px',
        padding: '10px 15px',
        fontWeight: '500',
        marginLeft: '15px',
        marginTop: '20px',
        textDecoration: 'none'
    },
    btnLoginMenu: {
        color: '#fff',
        display: 'inline-block',
        border: '2px solid',
        borderColor: '#fff',
        borderRadius: '3px',
        fontSize: '16px',
        lineHeight: '15px',
        padding: '10px 15px',
        fontWeight: '500',
        marginLeft: '15px',
        marginTop: '20px',
        textDecoration: 'none'
    },
    boxRight: {
        marginTop: '10px',
        marginBottom: '32px',
        textAlign: 'right'
    },
    boxTop: {
        marginTop: '10px',
        marginBottom: '0',
        textAlign: 'right'
    },
    crumbsBox: {
        marginBottom: '20px'
    },
    linkBox: {
        display: 'inline-block',
        verticalAlign: 'top',
        marginTop: '-10px'
    },
    linkBox1: {
        display: 'inline-block',
        width: '240px'
    },
    rowLine: {
        backgroundColor: '#0eb9ba',
        borderBottom: '1px solid',
        color: '#dadada',
        marginBottom: '25px'
    },
    cf: {
        paddingBottom: '45px'
    },
    maxCol: {
        maxWidth: '1366px',
        margin: '0 auto'
    }
});

class DesktopRegisterDoctor extends Component {
    constructor (props) {
        super(props);
        this.state = {
            name: '',
            surname: '',
            PWZNumber: '',
            email: '',
            password: '',
            repeatPassword: ''
        };
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleChange = this.handleChange.bind(this);
    }
    handleSubmit (e) {
        e.preventDefault();
        e.stopPropagation();
        if (this.state.password !== this.state.repeatPassword) {
            alert('Password mismatch');
            return null;
        }
        if (Array.from(this.state).filter((x) => x !== '') > 0) {
            alert('All data required');
            return null;
        }
        this.props.register(this.state);
    }

    handleChange (e) {
        const { name, value } = e.target;
        this.setState({ [name]: value });
    }
    render () {
        const { classes } = this.props;
        return (
            <section className={ classes.bdg }>
                <div className={ classes.sc }>
                    <div className={ classes.graf } />
                    <div className={ `${classes.bgt} container-fluid` }>
                        <div className={ `${classes.maxCol} row ` }>
                            <div className="col-xs-12 col-sm-12 col-md-12">
                                <div className={ classes.boxTop }>
                                    <a className={ classes.btnTextMenu2 } href="https://zdrowie.plus/auth/realms/patient/protocol/openid-connect/auth?client_id=account&redirect_uri=https%3A%2F%2Fzdrowie.plus%2Fapp&state=0%2F190b3dd5-4968-438e-907c-a73e21e8cbc6&response_type=code&scope=openid">
                    Strefa Pacjenta
                                    </a>
                                    <a className={ classes.btnTextMenu1 } href="https://zdrowie.plus/auth/realms/practitioner/protocol/openid-connect/auth?client_id=account&redirect_uri=https%3A%2F%2Fzdrowie.plus%2Fapp&state=0%2F190b3dd5-4968-438e-907c-a73e21e8cbc6&response_type=code&scope=openid
">
                    Strefa Lekarza
                                    </a>
                                    <a className={ classes.btnTextMenu2 } href="/">
                    Strefa Biznesu
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className={ `${classes.rowLine} container-fluid ` }>
                        <div className={ `${classes.maxCol} row ` }>
                            <div className="col-xs-12 col-sm-12 col-md-3">
                                <a href="https://zdrowie.mymedic.com.pl/">
                                    <img src={ logo } className={ classes.logo } />
                                </a>
                            </div>
                            <div className="col-xs-12 col-sm-12 col-md-9">
                                <div className={ classes.boxRight }>
                                    <a className={ classes.btnTextMenu } href="/">
                    Aktualności
                                    </a>
                                    <a className={ classes.btnTextMenu } href="/">
                    Jak to działa
                                    </a>
                                    <a className={ classes.btnTextMenu } href="/">
                    Aplikacje
                                    </a>
                                    <a className={ classes.btnRegisterMenu } href="/register/practitioner">
                    Załóż konto
                                    </a>
                                    <a className={ classes.btnLoginMenu } href="https://zdrowie.plus/auth/realms/practitioner/protocol/openid-connect/auth?client_id=account&redirect_uri=https%3A%2F%2Fzdrowie.plus%2Fapp&state=0%2F190b3dd5-4968-438e-907c-a73e21e8cbc6&response_type=code&scope=openid">
                    Zaloguj się
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className={ `${classes.cf} container` }>
                        <div className={ classes.crumbsBox }>
                            <div className={ classes.geo } /> <a className={ classes.linkText } href="/">Strona główna</a><div className={ classes.nextArrow } /><span className={ classes.linkText1 }>Rejestracja</span>
                        </div>
                        <div className={ `${classes.borderBox} row` }>
                            <div className="col-xs-12 col-sm-12 col-md-6">
                                <form onSubmit={ this.handleSubmit }>
                                    <div className={ classes.formBox }>
                                        <h1>
                                            <span className={ classes.span1 }>Dołącz, jako </span>
                                            <span className={ classes.span2 }>Lekarz</span>
                                        </h1>
                                        <label>
                                            <input type="text" className={ classes.inputs } ref="" placeholder="Imię *"
                                                name="name"
                                                onChange={ this.handleChange }
                                            />
                                        </label>
                                        <label>
                                            <input type="text" className={ classes.inputs } ref="" placeholder="Nazwisko *"
                                                name="surname"
                                                onChange={ this.handleChange }
                                            />
                                        </label>
                                        <label>
                                            <input type="e-mail" className={ classes.inputs } ref="" placeholder="Adres e-mail *"
                                                name="email"
                                                onChange={ this.handleChange }
                                            />
                                        </label>
                                        <label>
                                            <input type="text" className={ classes.inputs } ref="" placeholder="Numer PWZ *"
                                                name="PWZNumber"
                                                onChange={ this.handleChange }
                                            />
                                        </label>
                                        <label>
                                            <input type="password" className={ classes.inputs } ref="" placeholder="Hasło (min. 7 znaków) * "
                                                name="password"
                                                onChange={ this.handleChange }
                                            />
                                        </label>
                                        <label>
                                            <input type="password" className={ classes.inputs } ref="" placeholder="Powtórz hasło *"
                                                name="repeatPassword"
                                                onChange={ this.handleChange }
                                            />
                                        </label>
                                    </div>
                                    <div className={ classes.formBox1 }>
                                        <div className={ classes.linkBox }><Checkbox color="primary" /></div>
                                        <div className={ classes.linkBox1 }>
                                            <span className={ classes.linkText3 }>Akceptuję</span>{' '}
                                            <a className={ classes.linkText2 } href="/">
                        Regulamin serwisu{' '}
                                            </a>{' '}
                                            <br />
                                            <span className={ classes.linkText3 }>i </span>
                                            <a className={ classes.linkText2 } href="/">
                        Politykę prywatności
                                            </a>
                                            <br />
                                        </div>
                                    </div>
                                    <div className={ classes.formBox2 }>
                                        <button type="submit" className={ classes.btnForm }>
                        Załóż konto
                                        </button>
                                    </div>
                                </form>
                            </div>
                            <div
                                className={ `${
                                    classes.bg
                                } col-xs-12 col-sm-12 col-md-6 center-xs center-sm` }
                            >
                                <div className={ classes.centerBox }>
                                    <div className={ classes.login } />
                                    <p className={ classes.p1 }>Masz już konto?</p>
                                    <a href="https://zdrowie.plus/auth/realms/practitioner/protocol/openid-connect/auth?client_id=account&redirect_uri=https%3A%2F%2Fzdrowie.plus%2Fapp&state=0%2F190b3dd5-4968-438e-907c-a73e21e8cbc6&response_type=code&scope=openid
" className={ classes.btnLogin }>
                    Zaloguj się
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        );
    }
}

export default withStyles(styles)(DesktopRegisterDoctor);
