import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import MenuProfilePatient from '../widgets/MenuProfilePatient';
import EditProfilePatient from '../widgets/EditProfilePatient';
// import FooterPatient from '../widgets/FooterPatient'

const styles = () => ({
    tb: {
        fontFamily: 'Poppins',
        fontSize: '14px',
        borderCollapse: 'collapse',
        margin: '0',
        padding: '0',
        width: '100%'
    },
    cl: {
        padding: '0',
        verticalAlign: 'top',
        height: '100%',
        width: '10%',
        backgroundColor: '#242E41'
    },
    cr: {
        padding: '0',
        verticalAlign: 'top',
        textAlign: 'left',
        height: '100%',
        width: '90%'
    }
});

class DesktopProfilePatient extends React.Component {
    render () {
        const { classes } = this.props;
        return (
            <table className={ classes.tb }>
                <tr>
                    <td className={ classes.cl }>
                        <MenuProfilePatient />
                    </td>
                    <td className={ classes.cr }>
                        {this.props.children}
                        <EditProfilePatient />
                        {/* <FooterPatient /> */}
                    </td>
                </tr>
            </table>
        );
    }
}
export default withStyles(styles)(DesktopProfilePatient);
