import React, { Component } from 'react';
import { withStyles } from '@material-ui/core/styles';
import logo from './images/logo.png';
import login from './images/img-login.png';
import graf from './images/graf.png';
import geo from './images/geo.png';
import nextArrow from './images/next.png';
import wbg3 from './images/wbg3.png';
import foto from './images/foto2.png';
import weightImg from './images/weight_img.png';

const styles = () => ({
    root: {
        textAlign: 'left',
        fontFamily: 'Poppins sans-serif',
        fontSize: '16px'
    },
    bdg: {
        paddingBottom: '85px',
        width: '100%'
    },
    bgt: {
        backgroundColor: '#f1f1f1'
    },
    sc: {
        backgroundColor: '#fff',
        width: '100%',
        margin: '0 auto',
        marginBottom: '5px'
    },
    inputs: {
        border: '1px solid',
        borderColor: '#cccccc',
        color: '#cccccc',
        borderRadius: '3px',
        marginTop: '6px',
        padding: '12px',
        fontSize: '15px',
        width: '80%'
    },
    logo: {
        backgroundImage: `url(${logo})`,
        height: '60px',
        width: '200px',
        backgroundSize: 'contain',
        backgroundRepeat: 'no-repeat',
        backgroundPosition: 'left',
        marginLeft: '150px',
        marginTop: '20px',
        marginBottom: '0'
    },
    avatar: {
        width: '102px',
        height: '102px',
        backgroundSize: 'cover',
        backgroundPosition: 'center center',
        backgroundRepeat: 'none',
        borderRadius: '50%'
    },
    avatarBox: {
        textAlign: 'center',
        margin: '0 auto',
        width: '102px'
    },
    foto: {
        backgroundImage: `url(${foto})`,
        height: '42px',
        width: '42px',
        backgroundSize: 'contain',
        backgroundRepeat: 'no-repeat',
        backgroundPosition: 'center',
        position: 'relative',
        top: '110px',
        left: '70px'
    },
    wbg3: {
        backgroundImage: `url(${wbg3})`,
        height: '710px',
        width: '1366px',
        backgroundSize: 'contain',
        backgroundRepeat: 'no-repeat',
        backgroundPosition: 'top center'
    },
    elementMenu: {
        display: 'inline-block',
        height: '6vh',
        marginTop: '5vh',
        marginBottom: '3vh',
        marginRight: '15px',
        paddingTop: '15px',
        paddingBottom: '15px',
        color: '#8b909b',
        fontWeight: '600',
        textDecoration: 'none'
    },
    login: {
        backgroundImage: `url(${login})`,
        height: '72px',
        width: '230px',
        backgroundSize: 'contain',
        backgroundRepeat: 'no-repeat',
        backgroundPosition: 'center'
    },
    graf: {
        backgroundImage: `url(${graf})`,
        height: '340px',
        width: '180px',
        backgroundSize: 'contain',
        backgroundRepeat: 'no-repeat',
        backgroundPosition: 'center',
        position: 'absolute',
        top: '45%',
        right: '15%'
    },
    geo: {
        display: 'inline-block',
        backgroundImage: `url(${geo})`,
        height: '15px',
        width: '13px',
        backgroundSize: 'contain',
        backgroundRepeat: 'no-repeat',
        backgroundPosition: 'center',
        marginRight: '4px',
        verticalAlign: 'middle'
    },
    nextArrow: {
        display: 'inline-block',
        backgroundImage: `url(${nextArrow})`,
        height: '18px',
        width: '18px',
        backgroundSize: 'contain',
        backgroundRepeat: 'no-repeat',
        backgroundPosition: 'center',
        marginRight: '4px',
        verticalAlign: 'middle'
    },
    bg: {
        width: '100%',
        backgroundColor: '#f1f1f1'
    },
    borderBox: {
        maxWidth: '952px',
        border: '1px solid',
        color: '#dadada'
    },
    formBox: {
        backgroundColor: '#fff',
        textAlign: 'center',
        paddingTop: '20px',
        paddingLeft: '85px',
        paddingRight: '85px',
        paddingBottom: '20px',
        border: '1px solid #dadada',
        marginTop: '30px',
        marginLeft: '83px',
        marginRight: '83px',
        width: '360px'
    },
    formBox1: {
        textAlign: 'left',
        paddingLeft: '85px',
        paddingRight: '85px',
        paddingBottom: '20px'
    },
    formBox2: {
        textAlign: 'center',
        paddingTop: '0',
        paddingLeft: '85px',
        paddingRight: '85px',
        paddingBottom: '20px'
    },
    span1: {
        color: '#383838',
        display: 'inline-block',
        fontWeight: '700',
        fontSize: '25px',
        marginTop: '35px'
    },
    span2: {
        color: '#2B99F3',
        fontWeight: '700',
        fontSize: '20px'
    },
    span3: {
        color: '#383838'
    },
    p1: {
        color: '#8b909b',
        fontSize: '14px',
        marginTop: '3px'
    },
    btnLogin: {
        border: 'none',
        borderRadius: '3px',
        paddingTop: '12px',
        paddingBottom: '12px',
        paddingLeft: '33px',
        paddingRight: '33px',
        fontSize: '15px',
        fontWeight: '600',
        backgroundColor: '#2196f3',
        color: '#fff',
        textDecoration: 'none'
    },
    linkText: {
        lineHeight: '0.8',
        color: '#8b909b',
        fontWeight: '600',
        textDecoration: 'none'
    },
    linkText1: {
        ineHeight: '0.8',
        color: '#000',
        fontWeight: '600',
        textDecoration: 'none'
    },
    linkText2: {
        ineHeight: '0.8',
        color: '#259ee3',
        fontWeight: '600',
        textDecoration: 'none'
    },
    btnTextMenu: {
        color: '#b1b1b1',
        display: 'inline-block',
        fontSize: '16px',
        lineHeight: '15px',
        padding: '12px 15px',
        fontWeight: '400',
        textDecoration: 'none'
    },
    btnTextMenu1: {
        backgroundColor: '#fff',
        color: '#000',
        display: 'inline-block',
        fontSize: '16px',
        lineHeight: '15px',
        padding: '12px 15px',
        fontWeight: '400',
        textDecoration: 'none'
    },
    btnRegisterMenu: {
        color: '#259ee3',
        display: 'inline-block',
        border: '2px solid',
        borderColor: '#259ee3',
        borderRadius: '3px',
        fontSize: '16px',
        lineHeight: '15px',
        padding: '10px 15px',
        fontWeight: '500',
        marginLeft: '15px',
        marginTop: '20px',
        textDecoration: 'none'
    },
    btnLoginMenu: {
        color: '#000',
        display: 'inline-block',
        border: '2px solid',
        borderColor: '#b1b1b1',
        borderRadius: '3px',
        fontSize: '16px',
        lineHeight: '15px',
        padding: '10px 15px',
        fontWeight: '500',
        marginLeft: '15px',
        marginTop: '20px',
        textDecoration: 'none'
    },
    boxRight: {
        marginTop: '10px',
        marginBottom: '32px',
        marginRight: '150px',
        textAlign: 'right'
    },
    boxTop: {
        marginTop: '10px',
        marginBottom: '0',
        marginRight: '150px',
        textAlign: 'right'
    },
    crumbsBox: {
        marginBottom: '20px'
    },
    linkBox: {
        display: 'inline-block',
        verticalAlign: 'top',
        marginTop: '-12px'
    },
    linkBox1: {
        display: 'inline-block'
    },
    rowLine: {
        borderBottom: '1px solid',
        color: '#dadada',
        marginBottom: '25px'
    },
    cf: {
        paddingBottom: '45px'
    },
    btnFoto: {
        border: 'none',
        borderRadius: '3px',
        width: '87%',
        paddingTop: '12px',
        paddingBottom: '12px',
        fontSize: '14px',
        fontWeight: '600',
        backgroundColor: '#2d98ee',
        color: '#fff',
        marginTop: '20px',
        marginLeft: '5px',
        marginRight: '5px',
        marginBottom: '60px'
    },
    btnLater: {
        border: 'none',
        borderRadius: '3px',
        width: '42%',
        paddingTop: '12px',
        paddingBottom: '12px',
        fontSize: '14px',
        fontWeight: '600',
        backgroundColor: '#bdc3c9',
        color: '#fff',
        marginLeft: '5px',
        marginRight: '5px',
        marginBottom: '15px'
    },
    btnContinue: {
        border: 'none',
        borderRadius: '3px',
        width: '42%',
        paddingTop: '12px',
        paddingBottom: '12px',
        fontSize: '14px',
        fontWeight: '600',
        backgroundColor: '#0eb9ba',
        color: '#fff',
        marginLeft: '5px',
        marginRight: '5px',
        marginBottom: '15px'
    },
    stepBox: {
        backgroundColor: '#fff',
        paddingTop: '20px',
        paddingLeft: '85px',
        paddingRight: '85px',
        paddingBottom: '20px',
        textAlign: 'center',
        marginLeft: '83px',
        marginRight: '83px',
        marginBottom: '30px',
        width: '360px',
        borderLeft: '1px solid #dadada',
        borderRight: '1px solid #dadada',
        borderBottom: '1px solid #dadada'
    },
    step1: {
        backgroundColor: '#2d98ee',
        display: 'inline-block',
        marginRight: '5px',
        width: '58px',
        height: '3px'
    },
    step2: {
        backgroundColor: '#cccccc',
        display: 'inline-block',
        marginRight: '5px',
        width: '58px',
        height: '3px'
    },
    stepEge: {
        display: 'inline-block',
        width: '42%',
        textAlign: 'left',
        paddingTop: '3px',
        fontWeight: '600',
        color: '#2d98ee'
    },
    step15: {
        display: 'inline-block',
        width: '42%',
        textAlign: 'right',
        paddingTop: '3px',
        fontWeight: '600',
        color: '#000'
    },
    inputBox: {
        width: '100%',
        marginBottom: '40px',
        textAlign: 'center'
    },
    weightImg: {
        backgroundImage: `url(${weightImg})`,
        height: '63px',
        width: '51px',
        backgroundSize: 'contain',
        backgroundRepeat: 'no-repeat',
        backgroundPosition: 'center',
        margin: '0 auto 40px'
    }
});

class MedicalInterviewFoto extends Component {
    constructor (props) {
        super(props);
        this.state = {
            startDate: new Date()
        };
        this.handleChange = this.handleChange.bind(this);
    }

    handleChange (date) {
        this.setState({
            startDate: date
        });
    }
    render () {
        const { classes } = this.props;
        const urlAvatar = 'http://mymedic.com.pl/wp-content/uploads/2019/03/avatar2.png';
        return (
            <section className={ classes.bdg }>
                <div className={ classes.sc }>
                    <div className={ `${classes.bgt} container-fluid` }>
                        <div className="row">
                            <div className="col-xs-12 col-sm-12 col-md-12">
                                <div className={ classes.boxTop }>
                                    <a className={ classes.btnTextMenu1 } href="/loginpatient">
                    Strefa Pacjenta
                                    </a>
                                    <a className={ classes.btnTextMenu } href="/logindoctor">
                    Strefa Lekarza
                                    </a>
                                    <a className={ classes.btnTextMenu } href="/">
                    Strefa Biznesu
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className={ `${classes.rowLine} container-fluid ` }>
                        <div className="row">
                            <div className="col-xs-12 col-sm-12 col-md-2">
                                <div className={ classes.logo } />
                            </div>
                            <div className="col-xs-12 col-sm-12 col-md-10">
                                <div className={ classes.boxRight }>
                                    <a className={ classes.btnTextMenu } href="/">
                    Aktualności
                                    </a>
                                    <a className={ classes.btnTextMenu } href="/">
                    Jak to działa
                                    </a>
                                    <a className={ classes.btnTextMenu } href="/">
                    Aplikacje
                                    </a>
                                    <a className={ classes.btnRegisterMenu } href="/registerpatient">
                    Załóż konto
                                    </a>
                                    <a className={ classes.btnLoginMenu } href="/loginpatient">
                    Zaloguj się
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="container">
                        <div className="row">
                            <div className="col-xs-12 col-sm-12 col-md-12">
                                <div className={ classes.crumbsBox }>
                                    <div className={ classes.geo } /> <a className={ classes.linkText } href="/">Strona główna</a><div className={ classes.nextArrow } />
                                    <span className={ classes.linkText }>Rejestracja</span><div className={ classes.nextArrow } />
                                    <span className={ classes.linkText }>Wywiad</span><div className={ classes.nextArrow } />
                                    <span className={ classes.linkText1 }>Krok 5</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className={ `${classes.wbg3} container` }>
                        <div className="row">
                            <div className="col-xs-12 col-sm-12 col-md-12">

                                <form onSubmit={ this.submitForm }>
                                    <div className={ classes.formBox }>
                                        <div className={ classes.avatarBox }>
                                            <div className={ classes.foto } />
                                            <div className={ classes.avatar } style={ {
                                                backgroundImage: 'url(' + urlAvatar + ')' } }
                                            />
                                        </div>
                                        <span className={ classes.span1 }>Zdjęcie profilowe</span>
                                        <p className={ classes.p1 }>Dodaj swoje zdjęcie w celu łatwiejszej identyfikacji</p>
                                        <div className={ classes.inputBox }>
                                            <button className={ classes.btnFoto } href="/">
                          Wybierz zdjęcie z dysku
                                            </button>
                                        </div>
                                        <button onClick={ this.submitForm } className={ classes.btnLater }>
                          Później
                                        </button>
                                        <button className={ classes.btnContinue } href="/">
                          Kontynuuj
                                        </button>
                                    </div>
                                    <div className={ classes.stepBox }>
                                        <div className={ classes.stepElement }>
                                            <span className={ classes.step1 }></span>
                                            <span className={ classes.step1 }></span>
                                            <span className={ classes.step1 }></span>
                                            <span className={ classes.step1 }></span>
                                            <span className={ classes.step1 }></span>
                                        </div>
                                        <div className={ classes.stepText }>
                                            <span className={ classes.stepEge }>Zdjęcie profilowe</span>
                                            <span className={ classes.step15 }>Krok 5 z 5</span>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        );
    }
}

export default withStyles(styles)(MedicalInterviewFoto);
