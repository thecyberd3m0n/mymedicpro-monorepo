import React, { Component } from 'react';
import { withStyles } from '@material-ui/core/styles';
import IconButton from '@material-ui/core/IconButton';
import ChevronLeft from '@material-ui/icons/ChevronLeft';

const styles = theme => ({
    root: theme.mixins.gutters({
        backgroundColor: theme.palette.primary.light,
        textAlign: 'center',
        fontFamily: 'Poppins'
    }),
    arrow: {
        color: 'white',
        fontSize: '40px',
        paddingTop: '0px',
        marginLeft: '-5px'
    },
    register: {
        fontWeight: '600',
        textTransform: 'uppercase',
        color: '#FFFFFF',
        width: '100%'
    },
    container_1: {
        height: '43px'
    }
});
class MMMobileTopBar extends Component {
    render () {
        const { classes, title } = this.props;
        return (
            <div className="row center-xs center-sm middle-xs middle-sm">
                <div className={ `${classes.container_1} col-xs-11 col-sm-11` }>
                
                    <div className="col-xs-12 row">
                        <div className="col-xs-2">
                            <IconButton onClick={(e) => {
                                window.history.back();
                            }} className={ classes.arrow }>
                                <ChevronLeft className={ classes.arrow }></ChevronLeft>
                            </IconButton>
                        </div>
                   
                        {/* <IconButton className={classes.arrow}>
            <i className="material-icons">menu</i>
          </IconButton>
          <IconButton className={classes.arrow}>
            <i className="material-icons">dashboard</i>
          </IconButton> */}
                    <div className={ `${classes.register} col-xs-4 col-xs-offset-2 center-xs` }>
                        {title}
                    </div>
                    </div>
                </div>
               
     
            </div>
        );
    }
}

export default withStyles(styles, { withTheme: true })(MMMobileTopBar);
