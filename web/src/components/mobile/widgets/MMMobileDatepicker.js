import React, { Component } from 'react';
import TextField from '@material-ui/core/TextField';
import { withStyles } from '@material-ui/core/styles';

const styles = () => ({
    root: {
        fontFamily: 'Poppins',
        padding: '0px'
    },
    inputComponent: {
        fontFamily: 'Poppins',
        height: '40px',
        fontSize: '0.75em',
        marginBottom: '1vh'
    },
    inputNative: {
        paddingTop: '0px',
        paddingBottom: '0px',
        lineHeight: '1'
    }
});

class MMMobileDatepicker extends Component {
    render () {
        const { classes, min, max } = this.props;
        return (
            <div className={ `${classes.root} col-xs-12 col-sm-12 middle-xs middle-sm` }>
                <TextField
                    type="date"
                    placeholder='Wpisz swoją datę urodzenia'
                    variant="outlined"
                    className={ classes.root }
                    fullWidth
                    { ...this.props }
                    InputProps={ { className: classes.inputComponent } }
                    inputProps={ { className: classes.inputNative, min: min, max: max } }
                />
            </div>
        );
    }
}

export default withStyles(styles)(MMMobileDatepicker);
