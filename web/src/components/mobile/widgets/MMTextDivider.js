import React, { Component } from 'react';
import Divider from '@material-ui/core/Divider';
import { withStyles } from '@material-ui/core/styles';

const styles = () => ({
    root: {
        height: '30px',
        paddingTop: '10px',
        fontFamily: 'Poppins'
    },
    span: {
        zIndex: 2,
        paddingLeft: '4px',
        paddingRight: '4px',
        backgroundColor: 'white',
        fontWeight: '500',
        color: '#AFAFAF',
        fontSize: '14px'
    },
    divider: {
        marginTop: '-10px',
        zIndex: 1
    }
});
class MMTextDivider extends Component {
    render () {
        const { text, classes } = this.props;
        return (
            <div className={ classes.root }>
                <span className={ classes.span }>{text}</span>
                <div className={ classes.divider }>
                    <Divider variant="middle">
                    </Divider>
                </div>
            </div>

        );
    }
}

export default withStyles(styles)(MMTextDivider);
