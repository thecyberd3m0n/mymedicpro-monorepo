import React, { Component } from 'react';
import { withStyles } from '@material-ui/core/styles';
import MobileStepper from '@material-ui/core/MobileStepper';

const styles = () => ({
    root: {
        width: '100%',
        flexGrow: '1',
        fontFamily: 'Poppins sans-serif'
    },
    dot: {
        width: '10vh',
        height: '0.2vh',
        borderRadius: '0'
    },
    progressbar: {
        width: '100%',
        justifyContent: 'center'
    },
    dots: {
        justifyContent: 'center'
    }
});
class MMMobileStepper extends Component {
    constructor (props) {
        super(props);
        this.state = {
            activeStep: 0
        };
    }
    handleNext () {
        this.setState(state => ({
            activeStep: state.activeStep + 1
        }));
    }
    handleBack () {
        this.setState(state => ({
            activeStep: state.activeStep - 1
        }));
    }

    render () {
        const { classes } = this.props;
        return (
            <div className="row center-xs center-sm">
                <MobileStepper
                    steps={ 4 }
                    dot={ classes.dot }
                    dots={ classes.dots }
                    variant="dots"
                    position="static"
                    activeStep={ this.state.activeStep }
                    { ...this.props }
                    className={ classes.progressbar }
                >
                </MobileStepper>
            </div>
        );
    }
}

export default withStyles(styles)(MMMobileStepper);
