import React, { Component } from 'react';
import { withStyles } from '@material-ui/core/styles';
import MMMobileTopBar from './MMMobileTopBar';
import { disableBodyScroll } from 'body-scroll-lock';

const styles = theme => ({
    root: theme.mixins.gutters({
        backgroundColor: theme.palette.primary.light,
        textAlign: 'center',
        fontFamily: 'Poppins'
    }),
    bg: {
        position: 'absolute',
        top: '0',
        bottom: '0',
        width: '100%',
        overflowX: 'hidden',
        backgroundColor: '#FFFFFF'
    },
    second_bg: {
        height: '30vh',
        backgroundColor: '#2195F2'
    },
    second_bg_doctor: {
        height: '30vh',
        backgroundColor: '#01B5B6'
    },
    container: {
        position: 'absolute',
        width: '100%',
        height: '100%', // due to Chrome duet bar
        overflow: 'hidden'
    },
    container_white: {
        backgroundColor: '#FFFFFF',
        borderRadius: '5px',
        webkitBoxShadow: '0px 0px 12px -1px rgba(0,0,0,0.31)',
        mozBoxShadow: '0px 0px 12px -1px rgba(0,0,0,0.31)',
        boxShadow: '0px 0px 12px -1px rgba(0,0,0,0.31)',
        overflowY: 'scroll',
        overflowX: 'hidden',
        height:'80vh'
    },
    arrow: {
        color: '#FFFFFF',
        left: '0'
    },
    t_bar: {
        width: '100%'
    }
});
class MMMobileBackgroundContainer extends Component {
    componentDidMount() {
        // 2. Get a target element that you want to persist scrolling for (such as a modal/lightbox/flyout/nav). 
        disableBodyScroll(document.querySelector('#scroll'));
    }
    render () {
        const { classes, children, title, role } = this.props;
        return (
            <div className={ classes.bg }>
                <div className={ `${classes.container}` }>
                    <div className={ `${classes.t_bar} ` }>
                        <MMMobileTopBar
                            title={ title }
                        ></MMMobileTopBar>
                    </div>
                    <div className={ 'row center-xs' }>
                        <div id="scroll" className={ `${classes.container_white} col-xs-10` }>
                            {children}
                        </div>
                    </div>
                </div>
                <div className={ `${role === 'patient' ? classes.second_bg : classes.second_bg_doctor} row` }>
                </div>
            </div>
        );
    }
}
export default withStyles(styles)(MMMobileBackgroundContainer);
