import React, { Component } from 'react';
import { withStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import IconButton from '@material-ui/core/IconButton';
import SettingsIcon from '@material-ui/icons/Settings';
import NotificationsIcon from '@material-ui/icons/Notifications';

const styles = () => ({
    root: {
        backgroundColor: '#2196F3',
        textAlign: 'center',
        fontFamily: 'Poppins',
        boxShadow: '0px 0px',
        overflowX: 'hidden'
    },
    label: {
        fontFamily: 'Poppins',
        fontSize: '14px',
        fontWeight: '600'
    },
    menuButton: {
       
    }
});
class MMMobileProfileTopBar extends Component {
    render () {
        const { classes } = this.props;
        return (
            <div className={ classes.root }>
                <AppBar position="static" className={ classes.root }>
                    <Toolbar>
                        <div className={ `${classes.label} col-xs-4 col-xs-offset-4` }>
TABLICA
                        </div>
                        <div className="col-xs-1 col-xs-offset-1">
                            <IconButton className={ classes.menuButton } color="inherit" aria-label="Notifications">
                                <NotificationsIcon />
                            </IconButton>
                        </div>
                        <div className="col-xs-1">
                            <IconButton className={ classes.menuButton } color="inherit" aria-label="Settings">
                                <SettingsIcon />
                            </IconButton>
                        </div>
                       
                    </Toolbar>
                </AppBar>
            </div>
        );
    }
}

export default withStyles(styles, { withTheme: true })(MMMobileProfileTopBar);
