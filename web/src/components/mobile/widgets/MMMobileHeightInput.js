import React, { Component } from 'react';
import { withStyles } from '@material-ui/core/styles';
import Select from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import InputLabel from '@material-ui/core/InputLabel';
import OutlinedInput from '@material-ui/core/OutlinedInput';

const styles = () => ({
    root: {
        fontFamily: 'Poppins',
        padding: '0px',
        width: '100%',
        marginTop: '3vh',
        borderRadius: '7px',
        height: '40px'
    },
    input: {
        fontFamily: 'Poppins',
        paddingTop: '0px',
        marginTop: '0px',
        height: '40px'
    },
    label: {
        textAligne: 'left',
        lineHeight: '0',
        fontSize: '0.75em'
    },
    form_container: {
        height: '40px'
    },
    select_input: {
        marginTop: '0px',
        padding: '0px',
        textAlign: 'left'
    },
    input_under: {
        marginTop: '0px',
        height: '40px',
        textAlign: 'left'
    },
    selectMenuclass: {
        paddingTop: '1vh',
        textAlign: 'left'
    }
});


class MMMobileHeightInput extends Component {
    constructor (props) {
        super(props);
        this.state = {
            level: 'empty',
            labelWidth: 0,
            selectionMade: false,
            dontShowEmptyOption: props.dontShowEmptyOption || true,
            nativeSelection: props.nativeSelection || true,
            levelRange: props.levelRange || [ 150, 200 ],
            placeholder: props.placeholder || 'Wpisz wartość ',
            unit:  props.unit || 'cm'
        };
        // this.updateConfiguration();
        this.state.levels = this.createLevels();


        this.config = {
            enableNativeSelectBehavior: this.props.nativeSelection || true,
            disablePossibilityToSelectEmptyValue: this.props.nativeSelection && this.props.dontShowEmptyOption || true
        };
    }

    // componentDidMount() {
    //     console.log(ReactDOM.findDOMNode(this.InputLabelRef));
    //     this.setState({
    //         labelWidth: ReactDOM.findDOMNode(this.InputLabelRef).offsetWidth,
    //     }); 
    // }

    updateConfiguration () {
        this.setState({
            dontShowEmptyOption: this.props.dontShowEmptyOption || true,
            nativeSelection: this.props.nativeSelection || true,
            levelRange: this.props.levelRange || [ 150, 200 ],
            placeholder: this.props.placeholder || 'Wpisz wartość ',
            unit:  this.props.unit || 'cm'
        })
    }

    createLevels () {
        let levels = [];

        for (let i = this.state.levelRange[0]; i < this.state.levelRange[1]; i++) {
            levels.push(i);
        }
        return levels;
    };

    setSelectionMadeFlag(e) {

        // we want to make some initialization only
        // stop any native unwanted behavior
        e.stopPropagation();
        e.preventDefault();

        // this is react onClick listener and we wnt to fire it once
        // should be refactored and listener should be unregistered after belo logic execution
        this.setSelectionMadeFlag = function () {};
        this.setState({ selectionMade: true });
    }
   
    handleChange(event) {
        this.setState({ level: event.target.value });
        this.props.onChange(event)
    }

    render () {

        // this.updateConfiguration();
        const { classes, onItemChange } = this.props;
        const { enableNativeSelectBehavior } = this.config;
        console.log(onItemChange)

        return (
            <div className={ `${classes.root} row center-xs center-sm middle-xs middle-sm` }>
                <FormControl variant="outlined" className={ `${classes.form_container} col-xs-12 col-sm-12` }>
                    <InputLabel 
                        className={ classes.label } 
                        htmlFor="level-simple"
                    >{ `${this.state.placeholder}${this.state.unit}` }</InputLabel>
                    <Select
                        ref="Select"
                        native={ enableNativeSelectBehavior }
                        margin="dense"
                        onChange={ this.handleChange.bind(this) }
                        fullWidth
                        input={ <OutlinedInput
                            labelWidth= { 140 }
                            className={ classes.input_under }
                            name="level" 
                            id="level-select"
                        /> }
                        value={ this.state.level }
                        inputProps={ {
                            level: 'level',
                            id: 'level-simple',
                            className: classes.input_inside,
                            value: this.state.level,

                            // Below will fire when "native" selection behavior enabled
                            onClick: this.setSelectionMadeFlag.bind(this)
                        } }
                    >
                        { this.renderSelectOptions() }
                    </Select>
                </FormControl>
                
            </div>
        );
    }

    // sub renderers
    renderSelectOptions() {
        const {
            enableNativeSelectBehavior,
            disablePossibilityToSelectEmptyValue
        } = this.config;

        if (enableNativeSelectBehavior) {
            return [
                disablePossibilityToSelectEmptyValue
                    ? this.state.selectionMade
                        ? null
                        : <option key="empty" value=""></option>
                    : <option key="empty" value=""></option>,
                this.state.levels.map(level => ( <option key={level} value={level}>{level}</option> ))
            ]
        }

        // @materia-ui behavior
        return this.state.levels.map(level => (
            <MenuItem key={ level } value={ level }>
                {level}
            </MenuItem>
        ))
    }
}

export default withStyles(styles)(MMMobileHeightInput);
