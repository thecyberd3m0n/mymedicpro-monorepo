import React, { Component } from 'react';
import { withStyles } from '@material-ui/core/styles';
import tablicaB from '../pages/images/tablica_b.svg';
import ekonsultacjeB from '../pages/images/ekonsultacje_b.svg';
import profilB from '../pages/images/profil_b.svg';
import hamburgerB from '../pages/images/hamburger_b.svg';
import { AppBar } from '@material-ui/core';
import Toolbar from '@material-ui/core/Toolbar';
import IconButton from '@material-ui/core/IconButton';
import Icon from '@material-ui/core/Icon';
import AddCircleIcon from '@material-ui/icons/AddCircle';

const styles = theme => ({
    root: theme.mixins.gutters({
        fontFamily: 'Poppins sans-serif',
        width: '100%',
        paddingRight: '0px',
        paddingLeft: '0px',
        backgroundColor: '#F3F5F6',
        boxShadow: '0px 0px'
    }),
    iconTable: {
        backgroundImage: `url(${tablicaB})`,
        backgroundSize: 'contain',
        backgroundPosition: 'center',
        backgroundRepeat: 'no-repeat',
        width: '100%',
        height: '25px'
    },
    iconMessage: {
        backgroundImage: `url(${ekonsultacjeB})`,
        backgroundSize: 'contain',
        backgroundPosition: 'center',
        backgroundRepeat: 'no-repeat',
        width: '100%',
        height: '30px'
    },
    iconProfile: {
        backgroundImage: `url(${profilB})`,
        backgroundSize: 'contain',
        backgroundPosition: 'center',
        backgroundRepeat: 'no-repeat',
        width: '100%',
        height: '25px'

    },
    iconHamburger: {
        backgroundImage: `url(${hamburgerB})`,
        backgroundSize: 'contain',
        backgroundPosition: 'center',
        backgroundRepeat: 'no-repeat',
        width: '100%',
        height: '25px'
    },
    iconAdd: {
        width: '100%',
        marginTop: '1vh',
        height: '6vh',
        padding: '0'
    },
    icon_btn: {
        width: '6vh',
        padding: '0'
    },
    navigate_container: {
        paddingRight: '0px',
        paddingLeft: '0px',
    },
    wrapper: {
        height: '6vh',
        width: '6vh'
    },
    appBar: {
        top: 'auto',
        bottom: 0,
    },
    toolbar: {
        alignItems: 'center',
        justifyContent: 'space-between',
        width: 'auto'
    },
    textIcon: {
        color: '#AFAFAF',
        textAligne: 'center',
        fontWeight: '600',
        fontSize: '9px',
        width: '17vh'
    }

});

class MMMobileBottomBar extends Component {

    handleChange = ( value) => {
        this.setState({ value });
    };

    render () {
        const { classes } = this.props;
        return (
            <div className={ classes.root }>
                <AppBar position="fixed" color="inherit" className={ classes.appBar }>
                    <Toolbar className={ classes.toolbar }>
                        <IconButton color="primary" className={ classes.icon_btn }>
                            <div>
                                <Icon className={ classes.iconTable }/>
                                <div className= { classes.textIcon }>TABLICA</div>
                            </div>
                        </IconButton>
                       
                        <IconButton color="primary" className={ classes.icon_btn } >
                            <div className= { classes.textIcon }>
                                <Icon className={ classes.iconMessage }/>
                                <div className= { classes.textIcon }>E-KONSULTACJE</div>
                            </div>
                        </IconButton>

                        <IconButton color="primary" className={ classes.icon_btn }>
                            <AddCircleIcon className={ classes.iconAdd } />
                        </IconButton>

                        <IconButton color="primary" className={ classes.icon_btn }>
                            <div>
                                <Icon className={ classes.iconProfile }/>
                                <div className= { classes.textIcon }>MÓJ PROFIL</div>
                            </div>
                        </IconButton>

                        <IconButton color="primary" className={ classes.icon_btn }>
                            <div>
                                <Icon className={ classes.iconHamburger }/>
                                <div className= { classes.textIcon }>WIĘCEJ</div>
                            </div>
                            
                        </IconButton>
                    </Toolbar>
                </AppBar>
            </div>

        );
    }
}

export default withStyles(styles, { withTheme: true })(MMMobileBottomBar);