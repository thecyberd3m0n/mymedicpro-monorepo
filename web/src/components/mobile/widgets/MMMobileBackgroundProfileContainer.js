import React, { Component } from 'react';
import { withStyles } from '@material-ui/core/styles';
import MMMobileProfileTopBar from '../widgets/MMMobileProfileTopBar';
import MMMobileBottomBar from '../widgets/MMMobileBottomBar';

const styles = () => ({
    root: {
        textAlign: 'center',
        fontFamily: 'Poppins',
        boxShadow: '0px 0px',
        overflowX: 'hidden'
    },
   
    t_bar: {
        width: '100%'
    },

    b_bar: {
        width: '100%'
    }
});

class MMMobileBackgroundProfileContainer extends Component {
    render () {
        const { classes, children } = this.props;
        return (
            <div className={ classes.root }>
                <div className={ `${classes.t_bar} ` }>
                    <MMMobileProfileTopBar ></MMMobileProfileTopBar>
                </div>
                {children}
                <div className={ classes.b_bar }>
                    <MMMobileBottomBar></MMMobileBottomBar>
                </div>
            </div>
        );
    }
}
export default withStyles(styles)(MMMobileBackgroundProfileContainer);
