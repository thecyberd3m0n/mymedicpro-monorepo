import React, { Component } from 'react';
import { withStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import IconButton from '@material-ui/core/IconButton';
import Icon from '@material-ui/core/Icon';
import MenuIcon from '@material-ui/icons/Menu';
import ChatBubbleOutlineIcon from '@material-ui/icons/ChatBubbleOutline';
import AddIcon from '@material-ui/icons/Add';
import Fab from '@material-ui/core/Fab';
import tablicaPacjentIcon from '../pages/images/tablicaPacjentIcon.png';
import profilPacjentIcon from '../pages/images/profilPacjentIcon.png';

const styles = theme => ({
    root: theme.mixins.gutters({
        backgroundColor: theme.palette.primary.light,
        textAlign: 'center',
        fontFamily: 'Poppins sans-serif'
    }),
    appBar: {
        top: 'auto',
        bottom: 0,
    },
    toolbar: {
        alignItems: 'center',
        justifyContent: 'space-between',
    },
    icon: {
        height: '7vh',
        color: '#AFAFAF',
        width: '5vh'
    },
    iconadd: {
        height: '7vh',
        backgroundColor: '#2196F3',
        color: 'white',
        width: '7vh',
        paddingTop: '1px',
        paddingBottom: '0px',
        boxShadow: '0px 0px'
    },
    icon_btn: {
        paddingTop: '1px',
        paddingBottom: '0px'
    },
    addicon_btn: {
        paddingTop: '0px',
        paddingBottom: '0px'
    },
    iconTablica: {
        backgroundImage: `url(${tablicaPacjentIcon})`,
        backgroundSize: 'contain',
        backgroundPosition: 'center',
        backgroundRepeat: 'no-repeat',
        width: '5vh',
        height: '5vh',
        color: '#2196F3'
    },
    iconProfil: {
        backgroundImage: `url(${profilPacjentIcon})`,
        backgroundSize: 'contain',
        backgroundPosition: 'center',
        backgroundRepeat: 'no-repeat',
        width: '5vh',
        height: '5vh',
        color: '#2196F3'
    }
});

class MMMobileDoctorBottomBar extends Component {
    render () {
        const { classes } = this.props;
        return (
            <div>
                <AppBar position="fixed" color="inherit" className={ classes.appBar }>
                    <Toolbar className={ classes.toolbar }>
                        <IconButton color="primary" className={ classes.icon_btn }>
                            <Icon className={ classes.iconTablica }/>
                        </IconButton>
                        <IconButton color="primary"className={ classes.icon_btn } >
                            <ChatBubbleOutlineIcon className={ classes.icon }/>
                        </IconButton >
                        <Fab className={ classes.iconadd }>
                            <AddIcon />
                        </Fab>
                        <IconButton color="primary" className={ classes.icon_btn }>
                            <Icon className={ classes.iconProfil }/>
                        </IconButton>
                        <IconButton color="primary" className={ classes.icon_btn }>
                            <MenuIcon className={ classes.icon }/>
                        </IconButton>
                    </Toolbar>
                </AppBar>
            </div>
        );
    }
}

export default withStyles(styles, { withTheme: true })(MMMobileDoctorBottomBar);