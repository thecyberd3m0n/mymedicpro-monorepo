import React, { Component } from 'react';
import Fab from '@material-ui/core/Fab';
import { withStyles } from '@material-ui/core/styles';

const styles = () => ({
    root: {
        width: '100%',
        maxHeight: '6vh',
        marginLeft: 'auto',
        marginRight: 'auto',
        fontWeight: '700',
        fontFamily: 'Poppins',
        fontSize: '0.75em',
        boxShadow: '0px 0px'
    }
});
class MMMobileButton extends Component {
    render () {
        const { classes, label } = this.props;
        return (
            <Fab
                variant="extended"
                size="large"
                className={ classes.root }
                { ...this.props }
            >{label}</Fab>
        );
    }
}

export default withStyles(styles)(MMMobileButton);
