import React, { Component } from 'react';
import { withStyles } from '@material-ui/core/styles';
import MMMobileAvatarUploader from '../widgets/MMMobileAvatarUploader';
import MMMobileButton from '../widgets/MMMobileButton';

import { Link } from 'react-router-dom';
const styles = theme => ({
    root: theme.mixins.gutters({
        fontFamily: 'Poppins'
    }),
    container: {
        marginTop: '3vh'
    },
    span1: {
        color: '#000000',
        textAlign: 'center',
        fontSize: '20px',
        fontWeight: '400'
    },
    span2: {
        color: '#AFAFAF',
        textAlign: 'center',
        fontSize: '14px',
        fontWeight: '400'
    },
    span_container_1: {
        marginTop: '1vh'
    },
    span_container_2: {
        marginTop: '2vh',
        lineHeight: '95%'
    },
    container_3: {
        marginTop: '10vh'
    },
    begin_btn: {
        marginTop: '2vh'
    }
});

class MobilePatientInterviewFinish extends Component {
    render () {
        const { classes } = this.props;
        return (
            <div className={ classes.root }>
                <div className={ `${classes.container}` }>
                    <MMMobileAvatarUploader></MMMobileAvatarUploader>
                </div>
                <div className={ classes.span_container_1 }>
                    <span className={ classes.span1 }>Gotowe!</span>
                </div>

                <div className="row center-xs center-sm">
                    <div className={ `${classes.span_container_2} col-xs-11 col-sm-11` }>
                        <span className={ classes.span2 }>
                        Zanim rozpoczniesz korzystanie z aplikacji pamiętaj o uzupełnieniu pozostałych informacji profilowych oraz zdrowotnych. Możesz również dodawać dokumenty, zdjęcia jak również zapisywać swoje pomiary związane ze zdrowiem. Podczas e-wizyty w prosty sposób udostępnisz wybrane elementy lekarzowi co pomoże w trafniejszej diagnozie.
Życzymy przyjemnego korzystania z aplikacji!

                        </span>
                    </div>
                </div>
                <div className={ `${classes.container_3}` }>
                    <div className="row center-xs center-sm">
                        <div className="col-xs-12 col-sm-12">
                            <Link to="/app">
                                <MMMobileButton
                                    color="primary" label="ZACZYNAJMY" 
                                    className={ `${classes.begin_btn} ` }
                                >
                                </MMMobileButton>
                            </Link>
                        </div>
                    </div>
                </div>
          
            </div>
        );
    }
}

export default withStyles(styles)(MobilePatientInterviewFinish);
