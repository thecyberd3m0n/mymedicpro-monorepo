import React, { Component } from 'react';
import { withStyles } from '@material-ui/core/styles';
import MMMobileAvatarUploader from '../widgets/MMMobileAvatarUploader';
import MMMobileBottomBar from '../widgets/MMMobileBottomBar';
import Switch from '@material-ui/core/Switch';
import cameraIcon from '../pages/images/cameraIcon.png';
import chatIcon from '../pages/images/chatIcon.png';

const styles = theme => ({
    root: theme.mixins.gutters({
        fontFamily: 'Poppins'
    }),
    white_container: {
        backgroundColor: '#FFFFFF',
        webkitBoxShadow: '0px 0px 12px -1px rgba(0,0,0,0.31)',
        mozBoxShadow: '0px 0px 12px -1px rgba(0,0,0,0.31)',
        boxShadow: '0px 0px 12px -1px rgba(0,0,0,0.31)',
        overflowY: 'scroll',
        height: '85vh',
        overflowX: 'hidden'
    },
    container_1: {
        marginTop: '3vh'
    },
    span_container_1: {
        marginTop: '4vh'
    },
    container_2: {
        marginTop: '-1vh'
    },
    span1: {
        display: 'inline',
        fontWeight: '500',
        fontSize: '20px'
    },
    span2: {
        fontWeight: '500',
        color: '#AFAFAF',
        fontSize: '14px'
    },
    span3: {
        fontWeight: '600',
        color: 'white',
        fontSize: '12px'
    },
    checked: {
        color: '#2B99F3'
    },
    box1: {
        backgroundColor: '#2196F3',
        minHeight: '19vh',
        width: '100%',
        border: '1vh solid white'
    },
    box2: {
        backgroundColor: '#0082F1',
        minHeight: '19vh',
        width: '100%',
        border: '1vh solid white'
    },
    box3: {
        backgroundColor: '#1BB9FF',
        minHeight: '19vh',
        width: '100%',
        border: '1vh solid white'
    },
    box4: {
        backgroundColor: '#40C4FF',
        minHeight: '19vh',
        width: '100%',
        border: '1vh solid white'
    },
    box5: {
        backgroundColor: '#01B5B6',
        minHeight: '19vh',
        width: '100%',
        border: '1vh solid white'
    },
    box6: {
        backgroundColor: '#00A7A8',
        minHeight: '19vh',
        width: '100%',
        border: '1vh solid white'
    },
    container_3: {
        marginTop: '2vh'
    },
    row1: {
        width: '100%'
    },
    camera_icon: {
        backgroundImage: `url(${cameraIcon})`,
        backgroundSize: 'contain',
        backgroundPosition: 'center',
        backgroundRepeat: 'no-repeat',
        width: '100%',
        height: '7vh'
    },
    chat_icon: {
        backgroundImage: `url(${chatIcon})`,
        backgroundSize: 'contain',
        backgroundPosition: 'center',
        backgroundRepeat: 'no-repeat',
        width: '100%',
        height: '7vh'
    }
});

class MobileDoctorProfileDash extends Component {
    render () {
        const { classes } = this.props;
        return (
            <div className={ `${ classes.root } row center-xs center-sm` }>
                <div className={ `${classes.white_container} col-xs-11 col-sm-11` }>
                    <div className={ `${classes.container_1} row center-xs center-sm` }>
                        <MMMobileAvatarUploader></MMMobileAvatarUploader>

                        <div className="row center-xs center-sm middle-xs middle-sm">
                            <div className={ `${classes.span_container_1} col-xs-12 col-sm-12` }>
                                <span className={ classes.span1 }>dr Jan Kowalski</span>
                            </div>
                            <div className={ `${classes.container_2} col-xs-12 col-sm-12` }>
                                <span className={ classes.span2 }>Aktywny teraz</span>
                                <Switch
                                    iconChecked={ classes.checked }
                                    color="default"
                                />
                            </div>
                        </div>
                    </div>


                    <div className={ `${classes.container_3} row center-xs center-sm` }>

                        <div className={ `${classes.row1} row center-xs center-sm` }>

                            <div className={ `${classes.box1} col-xs-6 col-sm-6 row middle-xs middle-sm center-xs center-sm` }>
                                <div>
                                    <div className={ `${classes.camera_icon} col-xs-12` }>
                                    </div>
                                    <div className={ `${classes.span3} col-xs-12` }>
                                        Moje e-Wizyty
                                    </div>
                                </div>    
                            </div>

                            <div className={ `${classes.box2}  col-xs-6 col-sm-6 row middle-xs middle-sm center-xs center-sm` }>
                                <div>
                                    <div className={ `${classes.chat_icon} col-xs-12` }>
                                    </div>
                                    <div className={ `${classes.span3} col-xs-12` }>
                                        Moje e-Porady
                                    </div>
                                </div>
                            </div>
                            
                        </div>

                        <div className={ `${classes.row1} row center-xs center-sm` }>

                            <div className={ `${classes.box3} col-xs-6 col-sm-6 row middle-xs middle-sm center-xs center-sm` }>
                                <div>
                                    <div className={ `${classes.camera_icon} col-xs-12` }>
                                    </div>
                                    <div className={ `${classes.span3} col-xs-12` }>
                                        Mój kalendarz
                                    </div>
                                </div>    
                            </div>


                            <div className={ `${classes.box4}  col-xs-6 col-sm-6 row middle-xs middle-sm center-xs center-sm` }>
                                <div>
                                    <div className={ `${classes.chat_icon} col-xs-12` }>
                                    </div>
                                    <div className={ `${classes.span3} col-xs-12` }>
                                        Mój cennik
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className={ `${classes.row1} row center-xs center-sm` }>

                            <div className={ `${classes.box5} col-xs-6 col-sm-6 row middle-xs middle-sm center-xs center-sm` }>
                                <div>
                                    <div className={ `${classes.camera_icon} col-xs-12` }>
                                    </div>
                                    <div className={ `${classes.span3} col-xs-12` }>
            Mój kalendarz
                                    </div>
                                </div>    
                            </div>


                            <div className={ `${classes.box6}  col-xs-6 col-sm-6 row middle-xs middle-sm center-xs center-sm` }>
                                <div>
                                    <div className={ `${classes.chat_icon} col-xs-12` }>
                                    </div>
                                    <div className={ `${classes.span3} col-xs-12` }>
            Mój cennik
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <MMMobileBottomBar></MMMobileBottomBar>
            </div>
        );
    }
}

export default withStyles(styles)(MobileDoctorProfileDash);
