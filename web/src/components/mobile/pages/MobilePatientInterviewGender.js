import React, { Component } from 'react';
import { withStyles } from '@material-ui/core/styles';
import MMMobileAvatarUploader from '../widgets/MMMobileAvatarUploader';
import MMMobileStepper from '../widgets/MMMobileStepper';
import MMMobileButton from '../widgets/MMMobileButton';
import manSzary from '../pages/images/manSzary.svg';
import womanSzary from '../pages/images/womanSzary.svg';
import { Link } from 'react-router-dom';

const styles = theme => ({
    root: theme.mixins.gutters({
        fontFamily: 'Poppins',
        minHeight: '80vh',
        overflowX: 'hidden'
    }),
    container: {
        marginTop: '3vh'
    },
    span1: {
        color: '#000000',
        textAlign: 'center',
        fontSize: '20px',
        fontWeight: '400'
    },
    span_container_1: {
        marginTop: '2vh',
        marginBottom: '1vh'
    },
    span2: {
        color: '#AFAFAF',
        textAlign: 'center',
        fontSize: '14px',
        fontWeight: '500'
    },
    span3: {
        fontWeight: '500',
        color: '#AFAFAF',
        fontSize: '12px',
        marginBottom: '1vh'
    },
    span_container_2: {
        lineHeight: '95%',
        marginBottom: '3vh'
    },
    begin_btn: {
        marginTop: '1vh'
    },
    later_btn: {
        marginTop: '1vh'
    },
    container_3: {
        marginTop: '3vh',
        marginBottom: '3vh'
    },
    empty_div: {
        width: '100%',
        height: '1vh'
    },
    woman_icon: {
        backgroundImage: `url(${womanSzary})`,
        backgroundSize: 'contain',
        backgroundPosition: 'center',
        backgroundRepeat: 'no-repeat',
        width: '8vh',
        height: '8vh'
    },
    men_icon: {
        backgroundImage: `url(${manSzary})`,
        backgroundSize: 'contain',
        backgroundPosition: 'center',
        backgroundRepeat: 'no-repeat',
        width: '8vh',
        height: '8vh'
    },
    image_container: {
        marginTop: '2vh',
        width: '100%',
        marginBottom: '2vh'
    },
    icon_container: {
        width: '100%',
        height: '17vh',
        borderColor: '#D9D9D9',
        border: '0.2vh solid',
        borderRadius: '3%'
    },
    icon_text: {
        color: '#AFAFAF',
        marginTop: '-3vh',
        fontSize: '12px',
        fontWeight: '500',
        textAlign: 'center'
    }
});

class MobilePatientInterviewGender extends Component {

    constructor (props) {
        super(props);

        this.state = {
            gender: '',
            opacity: 0.7
        };
    }

    renderGenderSwitchButtons () {

        const { classes, handlePatientInterviewFormChange } = this.props;

        return [
            <div onClick={ () => 
            {
                handlePatientInterviewFormChange('sex','male')
                this.setState({ gender: 'man' })
             } }
                 style={ { opacity: this.state.gender === 'man'? '' : this.state.opacity } }
                 className={ `${classes.image_container} row start-xs center-sm col-xs-6 col-sm-5 middle-xs middle-sm` }>
                <div className={ `${classes.icon_container} row center-xs center-sm middle-xs` }>
                    <div className={ `${classes.men_icon} col-xs-6 col-sm-6` }>
                    </div>
                    <div className={ `${classes.icon_text} col-xs-12 col-sm-12` }>Mężczyzna</div>
                </div>
            </div>,
            <div onClick={ () => {
                handlePatientInterviewFormChange('sex','female')
                this.setState({ gender: 'woman' })
                } }
                 style={ { opacity: this.state.gender === 'woman'? '' : this.state.opacity } }
                 className={ `${classes.image_container} row end-xs end-sm col-xs-6 col-sm-5 middle-xs middle-sm` }>
                <div className={ `${classes.icon_container} row center-xs center-sm middle-xs` }>
                    <div className={ `${classes.woman_icon} col-xs-6 col-sm-6` }>
                    </div>
                    <div className={ `${classes.icon_text} col-xs-12 col-sm-12` }>Kobieta</div>
                </div>
            </div>
        ];
    }

    render () {
        const { classes } = this.props;

        return (
            <div className={ classes.root }>
                <div className={ `${classes.container} row center-xs center-sm` }>
                    <div className="col-xs-10 col-sm-10">
                        <MMMobileAvatarUploader></MMMobileAvatarUploader>
                    </div>

                
                    <div className={ `${classes.span_container_1} col-xs-10 col-sm-10` }>
                        <span className={ classes.span1 }>Wybierz płeć</span>
                    </div>
                    <div className={ `${classes.span_container_2} col-xs-10 col-sm-10` }>
                        <span className={ classes.span2 }>Zaznacz wybraną opcję i przejdź do następnego kroku</span>
                    </div>
                </div>
                <div className="row center-xs center-sm middle-xs middle-sm">
                    { this.renderGenderSwitchButtons() }
                </div>
                <div className={ classes.container_3 }>
                    <div className="row center-xs center-sm">
                        <div className="col-xs-12 col-sm-12">
                            <MMMobileStepper
                                activeStep={ 1 }
                            ></MMMobileStepper>
                        </div>
                    </div>
                    <div className="row center-xs center-sm">
                        <div className="row col-xs-11 col-sm-10">
                            <div className={ `${classes.span3} col-xs-6 col-sm-6 start-xs` }>Postęp ukończenia:</div>
                            <div className={ `${classes.span3} col-xs-6 col-sm-6 end-xs` }>krok 2 z 4</div>
                        </div>
                        <div className="col-xs-12 col-sm-12">
                            <Link to="/register/patientinterview/3">
                                <MMMobileButton
                                    color="primary" 
                                    label="DALEJ" 
                                    className={ `${classes.begin_btn} ` }
                                >
                                </MMMobileButton>
                            
                            </Link>
                        </div>
                        <div className={ classes.empty_div }></div>
                        <div className="col-xs-12 col-sm-12">
                            <Link to="/register/patientinterview/3">
                                <MMMobileButton
                                    color="secondary"
                                    label="UZUPEŁNIJ PÓŹNIEJ"
                                    className={ `${classes.later_btn} ` }
                                ></MMMobileButton>
                            </Link>
                        </div> 
                    </div>
                </div>

            </div>
        );
    }
}
export default withStyles(styles)(MobilePatientInterviewGender);
