import React, { Component } from 'react';
import MMMobileAvatarUploader from '../widgets/MMMobileAvatarUploader';
import { withStyles } from '@material-ui/core/styles';
import MMMobileStepper from '../widgets/MMMobileStepper';
import MMMobileButton from '../widgets/MMMobileButton';
import { Link } from 'react-router-dom';
import MMMobileDatepicker from '../widgets/MMMobileDatepicker';

const styles = theme => ({
    root: theme.mixins.gutters({
        fontFamily: 'Poppins',
        minHeight: '60vh',
        overflowX: 'hidden'
    }),
    container: {
        marginTop: '3vh'
    },
    span1: {
        color: '#000000',
        textAlign: 'center',
        fontSize: '20px',
        fontWeight: '400'
    },
    span_container_1: {
        marginTop: '2vh',
        marginBottom: '2vh'
    },
    span2: {
        color: '#AFAFAF',
        textAlign: 'center',
        fontSize: '14px',
        fontWeight: '500'
    },
    span_container_2: {
        lineHeight: '95%',
        marginBottom: '2vh'
    },
    container_3: {
        marginTop: '15vh'
    },
    date_icon: {
        width: '100%'
    },
    span3: {
        fontWeight: '500',
        color: '#AFAFAF',
        fontSize: '10px',
        marginBottom: '1vh'
    },
    begin_btn: {
        marginTop: '1vh'
    },
    later_btn: {
        marginTop: '1vh',
        marginBottom: '3vh'
    },
    empty_div: {
        width: '100%',
        height: '1vh'
    }
});
class MobilePatientInterview extends Component {
    constructor(props) {
        super(props)
        this.state = {
            date: "1990-01-01"
        }
        this.handleChange = this.handleChange.bind(this)
    }

    handleChange(e) {
        this.setState({ date: e.target.value })
        this.props.handlePatientInterviewFormChange("birthDate", e.target.value)
    }

    render () {
        const { classes } = this.props;
        const { date } = this.state;

        return (
            <div className={ classes.root }>
                <div className={ `${classes.container} row center-xs center-sm` }>
                    <div className="col-xs-10 col-sm-10">
                        <MMMobileAvatarUploader></MMMobileAvatarUploader>
                    </div>
                    <div className={ `${classes.span_container_1} col-xs-10 col-sm-10` }>
                        <span className={ classes.span1 }>Ile masz lat?</span>
                    </div>
                    <div className={ `${classes.span_container_2} col-xs-11 col-sm-11` }>
                        <span className={ `${classes.span2}` }>Podaj swoją datę urodzenia.</span>
                    </div>
                </div>
                <MMMobileDatepicker value={ date } min="1960-10-12" max="2015-01-01" onChange={this.handleChange}></MMMobileDatepicker>
                <div className={ classes.container_3 }>
                    <div className="row center-xs center-sm">
                        <div className="col-xs-12 col-sm-12">
                            <MMMobileStepper
                                activeStep={ 0 }
                            ></MMMobileStepper>
                        </div>
                    </div>
                    <div className="row center-xs center-sm">
                        <div className="row col-xs-12 col-sm-12">
                            <div className={ `${classes.span3} col-xs-6 col-sm-6 start-xs` }>Postęp ukończenia:</div>
                            <div className={ `${classes.span3} col-xs-6 col-sm-6 end-xs` }>krok 1 z 4</div>
                        </div>
                        <div className="col-xs-12 col-sm-12">
                            <Link to="/register/patientinterview/2">
                                <MMMobileButton
                                    color="primary" 
                                    label="DALEJ" 
                                    className={ `${classes.begin_btn} ` }
                                >
                                </MMMobileButton>
                            </Link>
                           
                        </div>
                        <div className={ classes.empty_div }></div>
                        <div className="col-xs-12 col-sm-12">
                            <Link to="/register/patientinterview/2">
                                <MMMobileButton
                                    color="secondary"
                                    label="UZUPEŁNIJ PÓŹNIEJ"
                                    className={ `${classes.later_btn} ` }
                                ></MMMobileButton>
                            </Link>
                        </div> 
                    </div>
                </div>
                

            </div>
        );
    }
}

export default withStyles(styles)(MobilePatientInterview);
