import React, { Component } from 'react';
import { withStyles } from '@material-ui/core/styles';
import 'typeface-roboto';
import MMMobileButton from '../widgets/MMMobileButton';
import MMMobileInput from '../widgets/MMMobileInput';
import Checkbox from '@material-ui/core/Checkbox';
import axios from 'axios';

const styles = theme => ({
    root: theme.mixins.gutters({
        textAlign: 'center',
        fontFamily: 'Poppins',
        paddingTop: '3vh'
    }),
    span1: {
        display: 'inline',
        fontWeight: '500',
        fontSize: '20px'
    },
    span2: {
        fontWeight: '900',
        fontSize: '20px'
    },
    span3: {
        fontWeight: '500',
        color: '#2B99F3',
        fontSize: '12px'
    },
    span4: {
        fontWeight: '500',
        color: '#AFAFAF',
        fontSize: '12px'
    },
    span_container_1: {
        marginBottom: '2vh'
    },
    span_container_2: {
        marginBottom: '2vh',
        lineHeight: '95%'
    },
    container_3: {
        marginBottom: '1vh',
        marginTop: '1vh'
    },
    terms: {
        textAlign: 'left',
        marginLeft: '-2vh',
        lineHeight: '95%'
    }
});

class MobileRegisterDoctor extends Component {
    constructor (props) {
        super(props);
        this.state = {
            firstName: '',
            lastName: '',
            pwz: '',
            email: '',
            password: '',
            repeatPassword: '',
            acceptedAgreement: false
        };
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleChange = this.handleChange.bind(this);
    }

    handleSubmit (e) {
        e.preventDefault();
        e.stopPropagation();
        if (this.state.password !== this.state.repeatPassword) {
            alert('Password mismatch');
            return null;
        }
        if (Array.from(this.state).filter((x) => x !== '') > 0) {
            alert('All data required');
            return null;
        }
        /**tmp request without mobx */
            axios.post('/api/registerpractitioner', {
                firstName: this.state.firstName,
                lastName: this.state.lastName,
                email: this.state.email,
                password: this.state.password,
                acceptedAgreement: true
            }).then(() => {
                this.props.history.push('/register/patientinterview/1')
            }, (err) => {
                console.error(err)
                alert('Nieprawidłowe dane, lub inny błąd')
            })
    }

    handleChange (e) {
        const { name, value } = e.target;
        this.setState({ [name]: value });
    }

    render () {
        const { classes } = this.props;
        return (
            <form onSubmit={ this.handleSubmit } className={ classes.root }>
                <div className="row center-xs center-sm">
                    <div className="col-xs-11 col-sm-11">
                        <div className={ classes.span_container_1 }>
                            <span className={ classes.span1 }>Dołącz, jako </span><span className={ classes.span2 }>Lekarz</span>
                        </div>
                        <div className={ classes.span_container_2 }>
                            <span className={ classes.span3 }>Załóż bezpłatne konto </span><span className={ classes.span4 }>i zyskaj dostęp do pełnej wersji aplikacji.</span>
                        </div>
                    </div>
                </div>
                <MMMobileInput
                    placeholder="Imię *"
                    name="firstName"
                    onChange={ this.handleChange }
                />
                <MMMobileInput
                    placeholder="Nazwisko *"
                    name="lastName"
                    onChange={ this.handleChange }
                />
                <MMMobileInput
                    placeholder="Numer PWZ *"
                    name="PWZNumber"
                    onChange={ this.handleChange }
                />
                <MMMobileInput
                    placeholder="Adres e-mail *"
                    name="email"
                    onChange={ this.handleChange }
                />
                <MMMobileInput
                    placeholder="Hasło *"
                    type="password"
                    name="password"
                    onChange={ this.handleChange }
                />
                <MMMobileInput
                    type="password"
                    name="repeatPassword"

                    placeholder="Powtórz hasło *"
                    onChange={ this.handleChange }

                />
                <div className={ `${classes.container_3} row middle-xs middle-sm` }>
                    <Checkbox />
                    <div className={ `${classes.terms} col-xs-10 col-sm-11` }>
                        <span className={ classes.span4 }>
                  Akceptuję&nbsp;</span>
                        <span className={ classes.span3 }>
                  Regulamin serwisu&nbsp;</span>
                        <span className={ classes.span4 }>
                  i&nbsp;</span>
                        <span className={ classes.span3 }>
                  Politykę prywatności.</span>
                    </div>
                </div>
                <div className="row center-xs center-sm">
                    <div className="col-xs-12 col-sm-12">
                        <MMMobileButton color="primary" type="submit"  label="ZAŁÓŻ KONTO">
                        </MMMobileButton>
                    </div>

                </div>
            </form>
        );
    }
}

export default withStyles(styles)(MobileRegisterDoctor);
