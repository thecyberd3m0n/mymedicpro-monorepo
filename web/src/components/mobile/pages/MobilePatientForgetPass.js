import React, { Component } from 'react';
import { withStyles } from '@material-ui/core/styles';
import MMMobileInput from '../widgets/MMMobileInput';
import MMMobileButton from '../widgets/MMMobileButton';

const styles = theme => ({
    root: theme.mixins.gutters({
        fontFamily: 'Poppins',
        minHeight: '80vh',
        overflowX: 'hidden'
    }),
    span1: {
        display: 'inline',
        fontWeight: '500',
        fontSize: '20px'
    },
    span_container_1: {
        marginTop: '3vh'
    },
    span2: {
        fontWeight: '500',
        color: '#AFAFAF',
        fontSize: '14px'
    },
    span_container_2: {
        marginTop: '2.5vh',
        lineHeight: '95%',
        marginBottom: '2.5vh'
    },
    container_3: {
        marginTop: '2.5vh'
    }
});

class MobilePatientForgetPass extends Component {
    render () {
        const { classes } = this.props;
        return (
            <div className={ `${classes.root} col-xs-12` }>
                <div className={ `${classes.span_container_1} row center-xs center-sm` }>
                    <span className={ classes.span1 }>Zapomniałeś hasła?</span>
                </div>
                <div className={ `${classes.span_container_2} row center-xs center-sm` }>
                    <span className={ classes.span2 }>Wpisz i powtórz nowe hasło, aby uaktualnić informacje dotyczące logowania.</span>
                </div>
                <MMMobileInput
                    type="password"
                    placeholder="Nowe hasło *"
                ></MMMobileInput>
                <MMMobileInput
                    type="password"
                    placeholder="Powtórz hasło *"
                ></MMMobileInput>
                <div className={ `${classes.container_3} row center-xs center-sm` }>
                    <div className="col-xs-12 col-sm-12">
                        <MMMobileButton color="primary" label="ZMIEŃ HASŁO"></MMMobileButton>
                    </div>

                </div>
            </div>
        );
    }
}

export default withStyles(styles)(MobilePatientForgetPass);
