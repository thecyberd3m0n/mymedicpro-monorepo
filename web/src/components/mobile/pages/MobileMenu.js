import React, { Component } from 'react';
import { withStyles } from '@material-ui/core/styles';
import MMMobileBottomBar from '../widgets/MMMobileBottomBar';
import cameraIcon from './images/cameraIcon.png';
import chatIcon from './images/chatIcon.png';
import wywiadAvatar from '../pages/images/wywiad_avatar.png';

import icon_cennik from './images/cennik.svg';
import icon_ewizyta from './images/ewizyta.svg';
import icon_kalendarz from './images/kalendarz.svg';
import icon_wiadomosci from './images/wiadomosci.svg';
import icon_preferencje from './images/preferencje.svg';
import icon_platnosci from './images/platnosci.svg';
import icon_ekonsultacje_a from './images/ekonsultacje_a.svg';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemAvatar from '@material-ui/core/ListItemAvatar';
import ListItemSecondaryAction from '@material-ui/core/ListItemSecondaryAction';
import ListItemText from '@material-ui/core/ListItemText';
import Avatar from '@material-ui/core/Avatar';
import IconButton from '@material-ui/core/IconButton';
import LinearProgress from '@material-ui/core/LinearProgress';


function genIcons(...iconsMeta) {

    return iconsMeta.reduce((styles, iconMeta) => {
        styles[ iconMeta.name ] = {
            backgroundImage: `url(${iconMeta.url})`,
            backgroundSize: 'contain',
            backgroundPosition: 'center',
            backgroundRepeat: 'no-repeat',
            width: '3vh',
            height: '3vh'
        };
        return styles;
    });
}

const styles = theme => (Object.assign({
    chatIcon: {
        width: '6vh',
        height: '6vh'
    },
    root: theme.mixins.gutters({
        fontFamily: 'Poppins'
    }),
    white_container: {
        backgroundColor: '#FFFFFF',
        overflowY: 'scroll',
        // height: '85vh',
        overflowX: 'hidden'
    },
    container_1: {
        backgroundColor: 'green'
    },
    container_2: {
        marginTop: '-1vh'
    },
    span3: {
        fontWeight: '600',
        color: 'white',
        fontSize: '12px'
    },
    checked: {
        color: '#2B99F3'
    },
    box1: {
        backgroundColor: '#2196F3',
        minHeight: '19vh',
        width: '100%',
        border: '1vh solid white'
    },
    box2: {
        backgroundColor: '#0082F1',
        minHeight: '19vh',
        width: '100%',
        border: '1vh solid white'
    },
    box3: {
        backgroundColor: '#1BB9FF',
        minHeight: '19vh',
        width: '100%',
        border: '1vh solid white'
    },
    box4: {
        backgroundColor: '#40C4FF',
        minHeight: '19vh',
        width: '100%',
        border: '1vh solid white'
    },
    box5: {
        backgroundColor: '#01B5B6',
        minHeight: '19vh',
        width: '100%',
        border: '1vh solid white'
    },
    box6: {
        backgroundColor: '#00A7A8',
        minHeight: '19vh',
        width: '100%',
        border: '1vh solid white'
    },
    container_3: {
        marginTop: '2vh'
    },
    row1: {
        width: '100%'
    },
    camera_icon: {
        backgroundImage: `url(${cameraIcon})`,
        backgroundSize: 'contain',
        backgroundPosition: 'center',
        backgroundRepeat: 'no-repeat',
        width: '6vh',
        height: '6vh'
    },
    chat_icon: {
        backgroundImage: `url(${chatIcon})`,
        backgroundSize: 'contain',
        backgroundPosition: 'center',
        backgroundRepeat: 'no-repeat',
        width: '100%',
        height: '7vh'
    },

    // TODO: remove above styles
    header: {
        backgroundColor: '#01b2b4',
        height: '13vh',
        width: '100%',
        paddingTop: '2.5vh',
        paddingBottom: '1.5vh',
    },
    menu: {
        // take rest of view
        //      full - unknown - header - bottom menu
        // should be more dynamic
        height: `${100 - 10 - 13 - 6}vh`,
        marginTop: '0px',
        width: '100%'
    },
    menuGroupHeader: {
        fontSize: '.7em',
        backgroundColor: '#edeef0',
        fontWeight: 'bold',
        padding: '5px 25px',
        textAlign: 'left'
    },
    menuGroupButton: {

    },
    menuBtn: {
        fontSize: '1em'
    },
    avatar: {
        color: '#82c3f8',
        backgroundColor: '#ffffff'
    },
    avatarIcon: {
        width: '5vh',
        height: '5vh'
    },
    avatarName: {
        color: '#ffffff',
        display: 'inline',
        fontWeight: '500',
        fontSize: '12px'
    },
    linearBar: {
        color: '#ffffff',
        marginTop: '-1vh',
        display: 'inline',
        fontSize: '12px',
        fontWeight: '500'
    },
    linearColorPrimary: {
        backgroundColor: '#fcfdfd',
    },
    linearBarColorPrimary: {
        backgroundColor: '#2b99f0',
    },
    resetTopMargin: {
        paddingTop: '0px'
    }
},

genIcons(
    { name: 'icon_cennik', url: icon_cennik },
    { name: 'icon_ekonsultacje_a', url: icon_ekonsultacje_a },
    { name: 'icon_ewizyta', url: icon_ewizyta },
    { name: 'icon_kalendarz', url: icon_kalendarz },
    { name: 'icon_platnosci', url: icon_platnosci },
    { name: 'icon_preferencje', url: icon_preferencje },
    { name: 'icon_wiadomosci', url: icon_wiadomosci }
)
));

class MobileMenu extends Component {

    constructor (props) {
        super(props);

        const { classes } = this.props;

        this.menu = [
            {
                groupName: 'E-KONSULTACJE',
                groupItems: [
                    { icon: classes.icon_ewizyta, text: 'Moje e-Wizyty', ariaLabel: 'Moje e-Wizyty'  },
                    { icon: classes.icon_ekonsultacje_a, text: 'Mojde e-Porady', ariaLabel: 'Mojde e-Porady'  },
                    { icon: classes.icon_wiadomosci, text: 'Wiadomości', ariaLabel: 'Wiadomości'  }
                ]
            },
            {
                groupName: 'GABINET',
                groupItems: [
                    { icon: classes.icon_kalendarz, text: 'Kalendarz', ariaLabel: 'Kalendarz'  },
                    { icon: classes.icon_platnosci, text: 'Cennik', ariaLabel: 'Cennik'  },
                    { icon: classes.icon_preferencje, text: 'Preferencje pracy', ariaLabel: 'Preferencje pracy'  },
                    { icon: classes.icon_platnosci, text: 'Płatności', ariaLabel: 'Płatności'  }
                ]
            }
        ];

        this.state = {
            accountFillUpProgress: 80
        };
    }

    renderButton (config) {

        // TODO: 'config.onClick'

        const { classes } = this.props;

        return (
            <ListItem key={ `menu-item-${ config.key }` }>
                <ListItemAvatar>
                    <Avatar children={ <div className={ config.icon }></div> }
                            classes={ { colorDefault: classes.avatar } } />
                </ListItemAvatar>
                <ListItemText classes={ { root: classes.menuBtn } } primary={ config.text } />
                <ListItemSecondaryAction>
                    <IconButton aria-label={ config.ariaLabel }>
                        <i className="material-icons">keyboard_arrow_right</i>
                    </IconButton>
                </ListItemSecondaryAction>
            </ListItem>
        );
    }

    render () {

        const { classes } = this.props;

        return (
            <div className={ `row center-xs center-sm` }>

                <div className={ `${classes.header} row center-xs center-sm middle-xs middle-sm` }>
                    <div className={ `col-xs-8 col-sm-8` }>
                        <div className="row center-xs center-sm middle-xs middle-sm">
                            <Avatar className={ `${classes.avatarIcon}` } src={ wywiadAvatar } />
                        </div>
                    </div>
                    <div className={ `col-xs-8 col-sm-8` }>
                        <div className={ `row center-xs center-sm` }>
                            <span className={ classes.avatarName }>Jan Kowalski</span>
                        </div>
                    </div>
                    <div className={ `${classes.linearBar} col-xs-9 col-sm-9` }>
                        <div className="row middle-xs middle-xs">
                            <div className="col-xs-9">
                                <LinearProgress
                                    variant="determinate"
                                    classes={{
                                        colorPrimary: classes.linearColorPrimary,
                                        barColorPrimary: classes.linearBarColorPrimary,
                                    }} value={ this.state.accountFillUpProgress }/>
                            </div>
                            <div className="col-xs-3">
                                <span>{ this.state.accountFillUpProgress } %</span>
                            </div>
                        </div>

                    </div>

                </div>
                <div className={ classes.menu }>

                    <List classes={ { root: classes.resetTopMargin } }>
                        { this.menu.map( (menuGroup, index) => (
                            <div key={ `menu-group-${ index + 1 }` }>

                                <div className={ classes.menuGroupHeader }>
                                    { `${menuGroup.groupName}` }
                                </div>
                                { menuGroup.groupItems.map( (menuGroup, index) =>  this.renderButton(
                                    Object.assign({}, menuGroup, { key: `menu-item-${ index + 1 }` })
                                )) }
                            </div>
                        )) }
                    </List>

                </div>
                <MMMobileBottomBar></MMMobileBottomBar>
            </div>
        );
    }
}

export default withStyles(styles)(MobileMenu);
