import React, { Component } from 'react';
import { Button } from '@material-ui/core';
import avatar from './images/avatar_patient_register.png';
import { withStyles } from '@material-ui/core/styles';
import MMMobileInput from '../widgets/MMMobileInput';
import Checkbox from '@material-ui/core/Checkbox';
import MMMobileButton from '../widgets/MMMobileButton';
import MMTextDivider from '../widgets/MMTextDivider';
import { Link } from 'react-router-dom';

const styles = theme => ({
    root: theme.mixins.gutters({
        textAlign: 'center',
        fontFamily: 'Poppins',
        paddingTop: '3vh'
    }),
    avatar: {
        backgroundImage: `url(${avatar})`,
        height: '9vh',
        width: '100%',
        backgroundSize: 'contain',
        backgroundRepeat: 'no-repeat',
        backgroundPosition: 'center',
        marginTop: '2vh',
        marginBottom: '2vh'
    },
    span_container_1: {
        marginTop: '4vh',
        marginBottom: '3vh',
        
    },
    span_container_2: {
        lineHeight: '95%',
        marginBottom: '1vh',
        marginTop: '2vh'
    },
    span1: {
        display: 'inline',
        fontWeight: '500',
        fontSize: '20px'
    },
    span2: {
        fontWeight: '900',
        fontSize: '20px'
    },
    span3: {
        fontWeight: '500',
        color: '#2B99F3',
        fontSize: '12px'
    },
    span4: {
        fontWeight: '500',
        color: '#AFAFAF',
        fontSize: '12px'
    },
    span5: {
        fontWeight: '500',
        color: '#2B99F3',
        fontSize: '12px'
    },
    span6: {
        fontWeight: '500',
        color: '#AFAFAF',
        fontSize: '12px'
    },
    terms: {
        textAlign: 'left',
        marginLeft: '-2vh',
        lineHeight: '95%'
    },
    have_acc: {
        textAlign: 'center',
        textTransform: 'uppercase',
        color: '#878889',
        fontWeight: '900'
    },
    btn_list: {
        width: '100%'
    },
    make_acc_btn: {
        color: '#FFFFFF',
        fontWeight: '900'
    },
    make_face_acc: {
        backgroundColor: '#3C5A99',
        color: 'white'
    },
    divider_text: {
        fontSize: '12px',
        color: '#AFAFAF'
    },
    empty: {
        width: '100%',
        height: '7vh'
    }
});
class MobileRegisterPatient extends Component {
    render () {
        const { classes } = this.props;
        return (
            <div className={ classes.root }>
                <div className="row center-xs center-sm">
                    <div className="col-xs-11 col-sm-11">
                        <div className={ classes.span_container_1 }>
                            <span className={ classes.span1 }>Dołącz, jako </span><span className={ classes.span2 }>Pacjent</span>
                        </div>
                    </div>
                </div>
                <MMMobileInput
                    placeholder="Imię"
                    type="text"
                />
                <MMMobileInput
                    placeholder="Nazwisko"
                    type="text"
                />
                <MMMobileInput
                    type="text"
                    placeholder="Adres e-mail"
                />
                <MMMobileInput
                    placeholder="Hasło"
                    type="password"
                    
                />
                <MMMobileInput
                    placeholder="Powtórz hasło"
                    type="password"
                />
                <div className="row middle-xs middle-sm">
                    <Checkbox />
                    <div className={ `${classes.terms} col-xs-10 col-sm-11` }>

                        <span className={ classes.span6 }>
                                    Akceptuję&nbsp;</span>
                        <span className={ classes.span5 }>
                                    Regulamin serwisu&nbsp;</span>

                        <span className={ classes.span6 }>
                                    i&nbsp;</span>
                        <span className={ classes.span5 }>
                                    Politykę prywatności.</span>
                    </div>
                </div>
                <div className="row center-xs center-sm middle-xs middle-sm">
                    <div className="col-xs-12 col-sm-12">
                        <Link to="/register/patientinterview/1">
                            <MMMobileButton color="primary" label="ZAŁÓŻ KONTO"></MMMobileButton>
                        </Link>
                    </div>

                </div>
                <MMTextDivider text="lub" />
                <div className="row center-xs center-sm middle-xs middle-sm">
                    <div className="col-xs-12 col-sm-12">
                        <MMMobileButton className={ classes.make_face_acc } label="ZAŁÓŻ PRZEZ FACEBOOK"></MMMobileButton>
                    </div>

                </div>
                <a href="https://zdrowie.plus/auth/realms/patient/protocol/openid-connect/auth?client_id=account&redirect_uri=https%3A%2F%2Fzdrowie.plus%2Fapp&state=0%2F190b3dd5-4968-438e-907c-a73e21e8cbc6&response_type=code&scope=openid">
                    <Button className={ `${classes.have_acc} row center-xs center-sm` }>
                                mam już konto
                    </Button>
                </a>
            </div>
        );
    }
}

export default withStyles(styles)(MobileRegisterPatient);
