import React, { Component } from 'react';
import { withStyles } from '@material-ui/core/styles';
import MMMobileAvatarUploader from '../widgets/MMMobileAvatarUploader';
import wzrost from '../pages/images/wzrost.svg';
import MMMobileStepper from '../widgets/MMMobileStepper';
import MMMobileButton from '../widgets/MMMobileButton';
import { Link } from 'react-router-dom';
import MMMobileHeightInput from '../widgets/MMMobileHeightInput'

const styles = theme => ({
    root: theme.mixins.gutters({
        fontFamily: 'Poppins',
        minHeight: '80vh',
        overflowX: 'hidden'
    }),
    container: {
        marginTop: '3vh'
    },
    span1: {
        color: '#000000',
        textAlign: 'center',
        fontSize: '20px',
        fontWeight: '400'
    },
    span2: {
        color: '#AFAFAF',
        textAlign: 'center',
        fontSize: '14px',
        fontWeight: '500'
    },
    span_container_1: {
        marginTop: '2vh',
        marginBottom: '1vh'
    },
    span_container_2: {
        lineHeight: '95%',
        marginBottom: '1vh'
    },
    height_icon: {
        backgroundImage: `url(${wzrost})`,
        backgroundSize: 'contain',
        backgroundPosition: 'center',
        backgroundRepeat: 'no-repeat',
        width: '100%',
        height: '13vh',
        opacity: '0.2'
    },
    image_container: {
        marginTop: '3vh'
    },
    span3: {
        fontWeight: '500',
        color: '#AFAFAF',
        fontSize: '12px',
        marginBottom: '1vh'
    },
    begin_btn: {
        marginTop: '1vh'
    },
    later_btn: {
        marginTop: '1vh'
    },
    container_3: {
        marginTop: '3vh',
        marginBottom: '2vh'
    },
    empty_div: {
        width: '100%',
        height: '1vh'
    }
});

class MobilePatientInterviewHeight extends Component {
    render () {
        const { classes, onItemChange } = this.props;
        console.log(onItemChange)
        return (
            <div className={ classes.root }>
                <div className={ `${classes.container} row center-xs center-sm` }>
                    
                    <MMMobileAvatarUploader></MMMobileAvatarUploader>
              

                </div>
                <div className={ classes.span_container_1 }>
                    <span className={ classes.span1 }>Wzrost</span>
                </div>
                <div className={ `${classes.span_container_2} col-xs-10 col-xs-offset-1 col-sm-8 col-sm-offset-2` }>
                    <span className={ classes.span2 }>Ile masz wzrostu?</span>
                </div>
                <div className="row center-xs center-sm">
                    <MMMobileHeightInput onChange={(e) => onItemChange('height', e.target.value )}>
                   
                    </MMMobileHeightInput>
                </div>
                
                <div className={ `${classes.image_container} row center-xs center-sm middle-xs middle-sm` }>
                    <div className={ classes.height_icon }>
                    </div>
                </div>
                <div className={ classes.container_3 }>
                    <div className="row center-xs center-sm">
                        <div className="col-xs-12 col-sm-12">
                            <MMMobileStepper
                                activeStep={ 2 }
                            ></MMMobileStepper>
                        </div>
                    </div>
                    <div className="row center-xs center-sm">
                        <div className="row col-xs-11 col-sm-10">
                            <div className={ `${classes.span3} col-xs-6 col-sm-6 start-xs` }>Postęp ukończenia:</div>
                            <div className={ `${classes.span3} col-xs-6 col-sm-6 end-xs` }>krok 3 z 4</div>
                        </div>
                        <div className="col-xs-12 col-sm-12">
                            <Link to="/register/patientinterview/4">
                                <MMMobileButton
                                    color="primary" 
                                    label="DALEJ" 
                                    className={ `${classes.begin_btn} ` }
                                >
                                </MMMobileButton>
                            </Link>
                        </div>
                        <div className={ classes.empty_div }></div>
                        
                        <div className="col-xs-12 col-sm-12">
                            <Link to="/register/patientinterview/4">
                                <MMMobileButton
                                    color="secondary"
                                    label="UZUPEŁNIJ PÓŹNIEJ"
                                    className={ `${classes.later_btn} ` }
                                ></MMMobileButton>
                            </Link>
                        </div> 
                    </div>
                </div>

            </div>

        );
    }
}

export default withStyles(styles)(MobilePatientInterviewHeight);
