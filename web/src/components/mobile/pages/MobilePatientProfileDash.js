import React, { Component } from 'react';
import { withStyles } from '@material-ui/core/styles';
import MMMobileAvatarUploader from '../widgets/MMMobileAvatarUploader';
import cameraIcon from '../pages/images/cameraIcon.png';
import chatIcon from '../pages/images/chatIcon.png';

const styles = theme => ({
    root: theme.mixins.gutters({
        fontFamily: 'Poppins'
    }),
    white_container: {
        backgroundColor: '#FFFFFF',
        webkitBoxShadow: '0px 0px 12px -1px rgba(0,0,0,0.31)',
        mozBoxShadow: '0px 0px 12px -1px rgba(0,0,0,0.31)',
        boxShadow: '0px 0px 12px -1px rgba(0,0,0,0.31)',
        overflowY: 'scroll',
        height: '85vh',
        overflowX: 'hidden'
    },
    container_1: {
        marginTop: '3vh'
    },
    span1: {
        display: 'inline',
        fontWeight: '500',
        fontSize: '20px'
    },
    span2: {
        fontWeight: '500',
        color: '#AFAFAF',
        fontSize: '14px'
    },
    span_container_1: {
        marginTop: '4vh'
    },
    span_container_2: {
        marginBottom: '4vh'
    },
    span_container_3: {
        marginTop: '2vh',
        width: '100%',
        marginBottom: '2vh'
    },
    box_1: {
        backgroundColor: '#01B5B6',
        minHeight: '27vh',
        width: '100%',
        paddingTop: '3vh',
        paddingBottom: '3vh',
        paddingRight: '1vh',
        paddingLeft: '1vh'
    },
    box_2: {
        backgroundColor: '#2196F3',
        minHeight: '27vh',
        width: '100%',
        paddingTop: '3vh',
        paddingBottom: '3vh',
        paddingRight: '1vh',
        paddingLeft: '1vh'
    },
    box_1_icon: {
        backgroundImage: `url(${cameraIcon})`,
        backgroundSize: 'contain',
        backgroundPosition: 'center',
        backgroundRepeat: 'no-repeat',
        width: '6vh',
        height: '6vh'
    },
    box_2_icon: {
        backgroundImage: `url(${chatIcon})`,
        backgroundSize: 'contain',
        backgroundPosition: 'center',
        backgroundRepeat: 'no-repeat',
        width: '6vh',
        height: '6vh'
    },
    icon_text_1: {
        color: 'white',
        fontSize: '14px',
        fontWeight: '600',
        textAlign: 'center',
        marginTop: '2vh',
        lineHeight: '97%',
        marginBottom: '2vh'
    },
    icon_text_2: {
        color: 'white',
        fontSize: '11px',
        fontWeight: '500',
        textAlign: 'center',
        lineHeight: '97%'
    },
    icon_text_3: {
        color: 'white',
        fontSize: '12px',
        fontWeight: '600',
        textAlign: 'center'
    },
    container_2: {
        marginTop: '3vh'
    },
    last_active: {
        color: '#AFAFAF',
        fontWeight: '700',
        fontSize: '13px',
        textAlign: 'left'
    }
});

class MobilePatientProfileDash extends Component {
    render () {
        const { classes } = this.props;
        return (
            <div className={ `${ classes.root } row center-xs center-sm` }>
                <div className={ `${classes.white_container} col-xs-11 col-sm-11` }>
                    <div className={ `${classes.container_1} row center-xs center-sm` }>
                        <MMMobileAvatarUploader></MMMobileAvatarUploader>
                        <div className="row center-xs center-sm middle-xs middle-sm">
                            <div className={ `${classes.span_container_1} col-xs-12 col-sm-12` }>
                                <span className={ classes.span1 }>Witaj Jan Kowalski</span>
                            </div>
                            <div className={ `${classes.span_container_2} col-xs-12 col-sm-12` }>
                                <span className={ classes.span2 }>Jak się dzisiaj czujesz?</span>
                            </div>
                        </div>
                       
                    </div>
                    <div className="row center-xs center-sm middle-xs middle-sm">
                        <div className={ `${classes.container_3} row start-xs center-sm col-xs-6 col-sm-5 middle-xs middle-sm` }>
                            <div className={ `${classes.box_1} row center-xs center-sm middle-xs` }>
                                <div className={ `${classes.box_1_icon} col-xs-6 col-sm-6` }>
                                </div>
                        
                                <div className={ `${classes.icon_text_1} col-xs-12 col-sm-12` }>
                                Porozmawiaj z lekarzem
                                </div>
                                <div className={ `${classes.icon_text_2}` }>
                                Skonsultuj się online
                                </div>
                                <div className={ `${classes.icon_text_3} col-xs-12 col-sm-12` }>
                                VIDEO/CHAT/TEL
                                </div>
                            </div>
                           
                        </div>
                        <div className={ `${classes.container_3} row end-xs end-sm col-xs-6 col-sm-5 middle-xs middle-sm` }>
                            <div className={ `${classes.box_2} row center-xs center-sm middle-xs` }>
                                <div className={ `${classes.box_2_icon} col-xs-6 col-sm-6` }>
                                </div>

                                <div className={ `${classes.icon_text_1}` }>
                                Zadaj pytanie lekarzowi
                                </div>
                                <div className={ `${classes.icon_text_2} col-xs-12 col-sm-12` }>
                                Zadaj lekarzowi pytania i poczekaj na odpowiedź.
                                </div>
                        

                            </div>
                       

                        </div>
                        <div className={ `${classes.container_2} col-xs-12` }>
                            <div className={ `${classes.last_active} ` }>
                                OSTATNIA AKTYWNOŚĆ
                            </div>
                        </div>
                   
                    </div>
                   
                   
                </div>
                
            </div>
        );
    }
}

export default withStyles(styles)(MobilePatientProfileDash);
