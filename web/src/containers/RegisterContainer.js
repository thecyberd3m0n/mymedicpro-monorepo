import React, { Component } from "react";
import { Route, withRouter, Switch } from "react-router-dom";
import MobileRegisterDoctor from "../components/mobile/pages/MobileRegisterDoctor";
import { isMobile } from "react-device-detect";
import DesktopRegisterPatient from "../components/desktop/pages/DesktopRegisterPatient";
import DesktopRegisterDoctor from "../components/desktop/pages/DesktopRegisterDoctor";
import MedicalInterviewAge from "../components/desktop/pages/MedicalInterviewAge";
import MedicalInterviewSex from "../components/desktop/pages/MedicalInterviewSex";
import MedicalInterviewSize from "../components/desktop/pages/MedicalInterviewSize";
import MedicalInterviewWeight from "../components/desktop/pages/MedicalInterviewWeight";
import MedicalInterviewEnd from "../components/desktop/pages/MedicalInterviewEnd";
import MobileRegisterPatient from "../components/mobile/pages/MobileRegisterPatient";
import MMMobileBackgroundContainer from "../components/mobile/widgets/MMMobileBackgroundContainer";
import MobilePatientInterview from "../components/mobile/pages/MobilePatientInterview";
import MobilePatientInterviewGender from "../components/mobile/pages/MobilePatientInterviewGender";
import MobilePatientInterviewHeight from "../components/mobile/pages/MobilePatientInterviewHeight";
import MobilePatientInterviewWeight from "../components/mobile/pages/MobilePatientInterviewWeight";
import MobilePatientInterviewFinish from "../components/mobile/pages/MobilePatientInterviewFinish";
import MobilePatientChangePass from "../components/mobile/pages/MobilePatientChangePass";
import MobilePatientForgetPass from "../components/mobile/pages/MobilePatientForgetPass";

const ROUTES = {
  mobile: [
    {
      path: "/register/patient",
      component: MobileRegisterPatient,
      name: "Rejestracja"
    },
    {
      path: "/register/practitioner",
      component: MobileRegisterDoctor,
      name: "Rejestracja"
    },
    {
      path: "/register/changepasspatient",
      component: MobilePatientChangePass,
      name: "Rejestracja"
    },
    {
      path: "/register/changepasspractitioner",
      component: MobilePatientChangePass,
      name: "Zmień hasło"
    },
    {
      path: "/register/patientinterview/1",
      component: MobilePatientInterview,
      name: "Wywiad pacjenta"
    },
    {
      path: "/register/patientinterview/2",
      component: MobilePatientInterviewGender,
      name: "Wywiad pacjenta"
    },
    {
      path: "/register/patientinterview/3",
      component: MobilePatientInterviewHeight,
      name: "Wywiad pacjenta"
    },
    {
      path: "/register/patientinterview/4",
      component: MobilePatientInterviewWeight,
      name: "Wywiad pacjenta"
    },
    {
      path: "/register/patientinterview/5",
      component: MobilePatientInterviewFinish,
      name: "Wywiad pacjenta"
    },
    {
      path: "/register/remindpasswordpatient",
      component: MobilePatientForgetPass,
      name: "Przypominanie hasła"
    }
  ],
  desktop: [
    {
      path: "/register/patient",
      component: DesktopRegisterPatient,
      name: "Rejestracja"
    },
    {
      path: "/register/practitioner",
      component: DesktopRegisterDoctor,
      name: "Rejestracja"
    },
    {
      path: "/register/patientinterview/1",
      component: MedicalInterviewAge,
      name: "Wywiad pacjenta"
    },
    {
      path: "/register/patientinterview/2",
      component: MedicalInterviewSex,
      name: "Wywiad pacjenta"
    },
    {
      path: "/register/patientinterview/3",
      component: MedicalInterviewWeight,
      name: "Wywiad pacjenta"
    },
    {
      path: "/register/patientinterview/4",
      component: MedicalInterviewSize,
      name: "Wywiad pacjenta"
    },
    {
      path: "/register/patientinterview/5",
      component: MedicalInterviewSize,
      name: "Wywiad pacjenta"
    },
    {
      path: "/register/remindpasswordpatient",
      component: MedicalInterviewEnd,
      name: "Przypominanie hasła"
    }
  ]
};

class RegisterContainer extends Component {
  constructor(props) {
    super(props);
    this.state = {
      role: "patient",
      pathName: "",
      interview: {
        birthDate: null,
        sex: 'M',
        weight: 0,
        height: 0
      }
    };
    this.handlePatientInterviewFormChange = this.handlePatientInterviewFormChange.bind(this)
    this.handlePatientInterviewFormSubmit = this.handlePatientInterviewFormSubmit.bind(this)
  }

  componentDidMount() {
    if (this.props.location.pathname.indexOf("patient") > 0) {
      this.setState({ role: "patient" });
    } else {
      this.setState({ role: "pracitioner" });
    }
    this.setState({pathName: ROUTES.mobile.find(route => route.path === this.props.location.pathname).name})
  }

  handlePatientInterviewFormChange(name, value) {
    const interview = this.state.interview
    interview[name] = value
    this.setState({interview: interview})
  }

  handlePatientInterviewFormSubmit() {
  }

  render() {
    const { pathName } = this.state
    // const pathName = ROUTES.mobile.find((route) => route.path === path).name
    return (
      <div>
        {isMobile ? (
          <div>
            <MMMobileBackgroundContainer role={this.state.role} title={pathName}>
              <Switch>
                {ROUTES.mobile.map((route, i) => (
                  <Route
                    key={i}
                    path={route.path}
                    component={route.component}
                    onItemChange={this.handlePatientInterviewFormChange}
                  />
                ))}
              </Switch>
            </MMMobileBackgroundContainer>
          </div>
        ) : (
          <Switch>
            {ROUTES.desktop.map((route, i) => (
              <Route key={i} path={route.path} component={route.component} />
            ))}
          </Switch>
        )}
      </div>
    );
  }
}
export default withRouter(RegisterContainer);
