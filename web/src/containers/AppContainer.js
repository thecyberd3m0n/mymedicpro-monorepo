import React, { Component } from 'react';
import { Route, withRouter, Switch } from 'react-router-dom';
import MobilePatientProfileDash from '../components/mobile/pages/MobilePatientProfileDash';
import MobileMenu from '../components/mobile/pages/MobileMenu';
import MMMobileBackgroundProfileContainer from '../components/mobile/widgets/MMMobileBackgroundProfileContainer';
import { isMobile } from 'react-device-detect';
import DesktopProfilePatient from '../components/desktop/pages/DesktopProfilePatient';

class AppContainer extends Component {
    render() {
        return (
            <div>
                {isMobile ? (
                    <MMMobileBackgroundProfileContainer>
                        <Switch>
                            <Route path="/app/profile" component={ MobilePatientProfileDash } />
                            <Route path="/app/menu" component={ MobileMenu } />
                        </Switch>
                    </MMMobileBackgroundProfileContainer>
                ) : (
                    <Switch>
                        <Route path="/app/profile" component={ DesktopProfilePatient } />
                    </Switch>
                )}
                {/* <Redirect from="/app" to="/app/profile"/> */}
            </div>
        );

    }
}
export default withRouter(AppContainer);
