import React, { Component } from "react";
import { MuiThemeProvider } from "@material-ui/core/styles";
import theme from "./const/theme";
import { Route, withRouter, Switch, Redirect } from "react-router-dom";
import RegisterContainer from "./containers/RegisterContainer";
import AppContainer from "./containers/AppContainer";

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      logged: false,
      user: {}
    };
  }

  // componentDidMount() {
  //   //Check login status
  // }

  render() {
    const { logged, user } = this.state;
    return (
      <MuiThemeProvider theme={theme}>
        <Switch>
            <Route component={AppContainer} path="/app" />
            <Route component={RegisterContainer} path="/register" user={user}/>
          {logged ? (
            <>
            <Redirect from="/" to="/app/profile" />
            </>
          ) : (
            <>
            <Redirect from="/" to="/register/patient" />
            </>
          )}
        </Switch>
      </MuiThemeProvider>
    );
  }
}

export default withRouter(App);
