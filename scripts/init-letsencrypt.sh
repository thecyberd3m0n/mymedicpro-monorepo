#!/bin/bash
# Get
SOLUTION_DATA_PATH=$1
DOMAIN=$2
EMAIL=$3

domains=( "${DOMAIN}" )
rsa_key_size=4096
data_path=$SOLUTION_DATA_PATH/certbot
email="${EMAIL}" #Adding a valid address is strongly recommended 
staging=0 #Set to 1 if you're just testing your setup to avoid hitting request limits
echo "### DOMAIN is ${DOMAIN}"
echo "### Preparing directories in $data_path ..."
rm -Rf "$data_path"
mkdir -p "$data_path/www"
mkdir -p "$data_path/letsencrypt/live/$domains"
echo "### Created $data_path/letscenrypt/live/$domains"

# echo "### Starting ui ..."
# SOLUTION_DATA_PATH=$data_path docker-compose up -d ui

echo "### Downloading recommended HTTPS parameters ..."
# staging=1 #Set to 1 if you're just testing your setup to avoid hitting request limits
echo "### DOMAIN is ${DOMAIN}"
echo "### Preparing directories in $data_path ..."
rm -Rf "$data_path"
mkdir -p "$data_path/www"
mkdir -p "$data_path/letsencrypt/live/$domains"
echo "### Created $data_path/letscenrypt/live/$domains"

echo "### Creating dummy certificate in ${data_path}/letsencrypt/live/$domains..."
path="$data_path/letsencrypt/live/$domains"
mkdir -p "$path"
SOLUTION_DATA_PATH=$data_path docker-compose run --rm --entrypoint "\
    openssl req -x509 -nodes -newkey rsa:1024 -days 1\
      -keyout '/etc/letsencrypt/live/$domains/privkey.pem' \
      -out '/etc/letsencrypt/live/$domains/fullchain.pem' \
      -subj '/CN=localhost'" certbot

echo "### Downloading recommended HTTPS parameters ..."
curl https://raw.githubusercontent.com/certbot/certbot/master/certbot-nginx/certbot_nginx/options-ssl-nginx.conf --output "$1/letsencrypt/options-ssl-nginx.conf"
curl https://raw.githubusercontent.com/certbot/certbot/master/certbot/ssl-dhparams.pem --output "$1/letsencrypt/ssl-dhparams.pem"


SOLUTION_DATA_PATH=$data_path docker-compose up -d ui
echo "### Deleting dummy certificate ..."
sudo rm -Rf "$1/letsencrypt/live"

echo "### Downloading recommended TLS options ..."
curl -s https://raw.githubusercontent.com/certbot/certbot/master/certbot-nginx/certbot_nginx/options-ssl-nginx.conf > "$1/letsencrypt/options-ssl-nginx.conf"
curl -s https://raw.githubusercontent.com/certbot/certbot/master/certbot/ssl-dhparams.pem > "$1/letsencrypt/ssl-dhparams.pem"


echo "### Requesting initial certificate ..."

#Join $domains to -d args
domain_args=""
for domain in "${domains[@]}"; do
 domain_args="$domain_args -d $domain"
done

echo $domain_args

#Select appropriate email arg
case "$email" in
 "") email_arg="--register-unsafely-without-email" ;;
 *) email_arg="--email $email" ;;
esac

#Enable staging mode if needed
if [ $staging != "0" ]; then staging_arg="--staging"; fi

SOLUTION_DATA_PATH=$data_path docker-compose run --rm --entrypoint "\
 certbot certonly --webroot -w /var/www/certbot \
   $staging_arg \
   $email_arg \
   $domain_args \
   --rsa-key-size $rsa_key_size \
   --agree-tos \
   --force-renewal" certbot
