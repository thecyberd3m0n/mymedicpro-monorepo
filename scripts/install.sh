#!/bin/bash

# Preconditions
if [ "$EUID" -ne 0 ]
  then echo "Please run as root. Aborting"
  exit
fi

IS_HTTPS=0
IS_GITLAB=0
LETSENCRYPT=0
# Read variables
read -e -p "Where you want to create SOLUTION_DATA directory: " -i "/root/mymedic" SOLUTION_DATA_PATH
read -p "Protect system by htpasswd? [Y/n]" -n 1 -r
echo    # (optional) move to a new line
if [[ $REPLY =~ ^[Yy]$ ]]
then
    IS_HTTPS=1
    read -e -p "Please enter LOGIN:" HTPASSWD_LOGIN
    echo -n Password:
    read -s HTPASSWD_PASS
fi
echo
read -p "Generate Lets-encrypt certificates? [Y/n]" -n 1 -r
echo    # (optional) move to a new line
if [[ $REPLY =~ ^[Yy]$ ]]
then
    LETSENCRYPT=1
    read -e -p "Please enter DOMAIN: " DOMAIN
    read -e -p "Please enter EMAIL: " EMAIL
fi
read -p "Are you're using Gitlab-Runner? [Y/n]" -n 1 -r
if [[ $REPLY =~ ^[Yy]$ ]]
then
    IS_GITLAB=1
fi
echo
# Procedure

echo "Creating directory $SOLUTION_DATA_PATH"

mkdir -p $SOLUTION_DATA_PATH
mkdir -p $SOLUTION_DATA_PATH/nginx
if  [ 1==IS_HTTPS ]
then
    echo "Generating htpasswd in $SOLUTION_DATA_PATH/nginx"
    htpasswd -b -c $SOLUTION_DATA_PATH/nginx/.htpasswd HTPASSWD_LOGIN HTPASSWD_PASS
fi
touch $SOLUTION_DATA_PATH/nginx/mymedic.conf
if [ 1==LETSENCRYPT ]
then
    echo "Generating SSL nginx configuration to $SOLUTION_DATA_PATH/nginx/mymedic.conf"
    ./generate-nginx-letsencrypt.sh $DOMAIN > $SOLUTION_DATA_PATH/nginx/mymedic.conf
    ./init-letsencrypt.sh $SOLUTION_DATA_PATH $DOMAIN $EMAIL
else
    echo "Generating non-SSL nginx configuration to $SOLUTION_DATA_PATH/nginx/mymedic.conf"
    ./generate-nginx-conf.sh $DOMAIN > $SOLUTION_DATA_PATH/nginx/mymedic.conf
fi

if [ 1==IS_GITLAB ]
then
    echo "Changing ownership for $SOLUTION_DATA_PATH for gitlab-runner"
    chown -R gitlab-runner $SOLUTION_DATA_PATH
fi
echo "vars: \n"
echo "SOLUTION_DATA_PATH=$SOLUTION_DATA_PATH"
