#!/bin/bash
if [ -z "$HTPASSWD_LOGIN" ] && [ -z "$HTPASSWD_PASS" ]
then
    echo "Generating htpasswd in $SOLUTION_DATA/nginx"
    htpasswd -b -c $SOLUTION_DATA/nginx/ HTPASSWD_LOGIN HTPASSWD_PASS
fi
