#!/bin/bash

DOMAIN=$1

cat << EOF
server {
    listen 80;
    server_name $DOMAIN;
    location / {
        return 301 https://$DOMAIN$request_uri;
    }
    
}

server {
    listen 443 ssl;
    charset utf-8;
    server_name $DOMAIN;
    if (\$http_x_forwarded_proto = "http") {
        return 301 https://$DOMAIN$request_uri;
    }
    if (\$host != "$DOMAIN") {
        return 301 https://$DOMAIN$request_uri;
    }
    ssl_certificate /etc/letsencrypt/live/$DOMAIN/fullchain.pem;
    ssl_certificate_key /etc/letsencrypt/live/$DOMAIN/privkey.pem;
    ssl_dhparam /etc/letsencrypt/ssl-dhparams.pem;
    gzip on;
    gzip_http_version 1.1;
    gzip_disable "MSIE [1-6]\.";
    gzip_min_length 256;
    gzip_vary on;
    gzip_proxied expired no-cache no-store private auth;
    gzip_types text/plain text/css application/json application/javascript application/x-javascript text/xml application/xml application/xml+rss text/javascript;
    gzip_comp_level 9;
    access_log /var/log/nginx/access.log;
    error_log /var/log/nginx/error.log;
    root /usr/share/nginx/html;
    location / {
        try_files $uri /index.html =404;
        include /etc/nginx/mime.types;
        auth_basic "Restricted";
        auth_basic_user_file /etc/nginx/conf.d/.htpasswd;
    }
    location /.well-known/acme-challenge/ {
        root /var/www/certbot;
    }
}
EOF