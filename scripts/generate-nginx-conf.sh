#!/bin/bash

cat << EOF
server {
    listen 80;
    charset utf-8;
    gzip on;
    gzip_http_version 1.1;
    gzip_disable "MSIE [1-6]\.";
    gzip_min_length 256;
    gzip_vary on;
    gzip_proxied expired no-cache no-store private auth;
    gzip_types text/plain text/css application/json application/javascript application/x-javascript text/xml application/xml application/xml+rss text/javascript;
    gzip_comp_level 9;
    access_log /var/log/nginx/access.log;
    error_log /var/log/nginx/error.log;
    root /usr/share/nginx/html;
    location /.well-known/acme-challenge/ {
        root /var/www/certbot;
    }
}
EOF