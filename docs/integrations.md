########
KEYCLOAK

###############
THEMING CUSTOMIZATION

`from Java Freemarker Language to React App`

```integrations/keycloak/themes/wrapped-theme``` - default FTL theme, that adds everything from Keycloak BE to Javascript `window.keycloak` namespace. 
Use it to add values to React app 
```login``` - React app, created using create-react-app. Provides Material-UI. Uses `window.keycloak` namespace to grab values.
```Dockerfile``` - build React App, move built React with wrapped-theme to mymedicpro theme. Theme ```mymedicpro``` should be accessible out-of-box, from application start.

Pro tips:

- Run dockerized and clean Keycloak on localhost:8080/auth with theme folder exposed to temp/

It's good to have this script to write Keycloak themes, however mind that we're already using ftl-wrapper for html5 apps
```docker run --net="host" -v /home/tcd/dev/mymedicpro-monorepo/temp:/opt/jboss/keycloak/themes:rw jboss/keycloak```

do not forget to copy original themes
########