const axios = require("axios");
const express = require("express");
const bodyParser = require("body-parser");
const KcAdminClient = require("keycloak-admin").default;
const { KEYCLOAK_URL, KEYCLOAK_ADMIN_USERNAME, KEYCLOAK_ADMIN_PASSWORD, FHIR_URL } = process.env

const kcAdminClient = new KcAdminClient({
  baseUrl: KEYCLOAK_URL,
  realmName: "master"
});

var app = express();
// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: false }));

// parse application/json
app.use(bodyParser.json());

const l = console.log;

function parseKcAdminResponse(res) {
  const { status, statusText, config, headers } = res.response;
  return {
    error: {
      status,
      statusText,
      config: {
        headers: config.headers
      },
      headers
    }
  };
}

app
  .post("/registerpractitioner", async (req, res) => {
    const { firstName, lastName, pwz, email, password } = req.body;

    await kcAdminClient.auth({
      username: KEYCLOAK_ADMIN_USERNAME,
      password: KEYCLOAK_ADMIN_PASSWORD,
      grantType: "password",
      clientId: "admin-cli"
    });
    kcAdminClient.setConfig({
      realmName: "master"
    });

    l("body ", req.body);

    try {
      const request = {
        username: firstName,
        realm: "practitioner",
        email: email,
        enabled: true
      };
      l("await kcAdminClient.users.create( request ) ", request);
      await kcAdminClient.users.create(request);
    } catch (kcAdminResponse) {
      l("Error: await kcAdminClient.users.create( request ) ", kcAdminResponse);
      return res
        .status(kcAdminResponse.response.status)
        .json(parseKcAdminResponse(kcAdminResponse));
    }

    kcAdminClient.setConfig({
      realmName: "practitioner"
    });

    l("Set Config");

    let userArr;
    try {
      userArr = await kcAdminClient.users.find({ username: firstName });
    } catch (e) {
      console.error(e.response.body);
    }
    const user = userArr[0];
    axios.put(`${FHIR_URL}/Practitioner/${user.id}`, {
      resourceType: "Practitioner",
      id: user.id,
      identifier: [
        {
          use: "official",
          system: "urn:oid:2.16.840.1.113883.3.4424.1.6.2",
          value: pwz
        }
      ],
      active: true,
      name: [
        {
          text: `${firstName} ${lastName}`,
          family: firstName,
          given: [firstName],
          prefix: ["dr.n.med."]
        }
      ],
      telecom: [
        {
          system: "email",
          value: email,
          use: "work"
        }
      ]
    }).then((fhirUser) => {
        res.status(201).json( fhirUser.data );
    }).catch(e => {
        res.status(500)
        console.error(e.response)
    })
    ;
  })

app.post("/registerpatient", async (req, res) => {
  const { firstName, lastName, pwz, email, password } = req.body;
  console.log("creating user", firstName, email);

  await kcAdminClient.auth({
    username: "admin",
    password: "admin",
    grantType: "password",
    clientId: "admin-cli"
  });
  kcAdminClient.setConfig({
    realmName: "master"
  });
  try {
    await kcAdminClient.users.create({
      username: firstName,
      realm: "patient",
      email: email,
      enabled: true
    });
  } catch (e) {
    const { status, statusText, config, headers } = e.response;
    res.status(e.status).json({
      error: {
        status,
        statusText,
        config: {
          headers: config.headers
        },
        headers
      }
    });
  }
  kcAdminClient.setConfig({
    realmName: "patient"
  });
  const userArr = await kcAdminClient.users.find({ username: firstName });
  const user = userArr[0];

  try {
    const fhirUser = await axios.put(
      `http://192.168.1.144/fhir/Patient/${user.id}`,
      {
        resourceType: "Patient",
        id: user.id,
        active: true,
        name: [
          {
            text: `${firstName} ${lastName}`,
            family: firstName,
            given: [firstName],
            prefix: ["dr.n.med."]
          }
        ],
        telecom: [
          {
            system: "email",
            value: email,
            use: "work"
          }
        ]
      }
    );
    res.json(fhirUser);
  } catch (e) {
    console.error(JSON.stringify(e));
    res.status(401).json("error", { error: e });
  }
});
app.listen(4000);
console.log("Server listening on port 4000");

// init();

// async function registerUser(config) {
//   const { username, email, lastName } = config;

// //   await kcAdminClient.auth({
// //     username: "admin",
// //     password: "admin",
// //     grantType: "password",
// //     clientId: "admin-cli"
// //   });

//   await kcAdminClient.users.create({
//     username,
//     realm: "patient",
//     email: email,
//     enabled: true
//   });
//   kcAdminClient.setConfig({
//     realmName: "patient"
//   });
//   const userArr = await kcAdminClient.users.find({ username: username });
//   const user = userArr[0]
//   try {
//     const fhirUser = await axios.put(`http://localhost/fhir/Patient/${user.id}`, {
//       resourceType: "Patient",
//       id: user.id,
//       identifier: [
//         {
//           use: "official",
//           system: "urn:oid:2.16.840.1.113883.3.4424.1.6.2",
//           value: "1778033"
//         }
//       ],
//       active: true,
//       name: [
//         {
//           text: `${username} ${lastName}`,
//           family: username,
//           given: [username],
//           prefix: ["dr.n.med."]
//         }
//       ],
//       telecom: [
//         {
//           system: "email",
//           value: email,
//           use: "work"
//         }
//       ]
//     });
//     console.log(fhirUser)
//   } catch (e) {
//     console.error(e)
//   }
// }
