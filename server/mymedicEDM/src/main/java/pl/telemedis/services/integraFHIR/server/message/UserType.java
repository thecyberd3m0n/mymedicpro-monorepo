package pl.telemedis.services.integraFHIR.server.message;

public enum UserType {

	PATIENT, PRACTITIONER;
	
}
