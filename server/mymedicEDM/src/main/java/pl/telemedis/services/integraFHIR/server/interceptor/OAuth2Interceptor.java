package pl.telemedis.services.integraFHIR.server.interceptor;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import ca.uhn.fhir.rest.server.interceptor.InterceptorAdapter;
import pl.telemedis.services.integraFHIR.server.message.TokenValidationStatus;

@Component
public class OAuth2Interceptor extends InterceptorAdapter {

	private static final Logger LOGGER = LoggerFactory.getLogger(OAuth2Interceptor.class);

	@Autowired
	RestTemplate template;
	@Value("${auth.url}")
	private String validateUrl;

	@Override
	public boolean incomingRequestPreProcessed(HttpServletRequest req, HttpServletResponse res) {
		String authHeader = req.getHeader("Authorization");
		if (authHeader == null || !authHeader.startsWith("Bearer ")) {
			// sendResponse(res);
			// return false;
		} else {
			authHeader = authHeader.replaceAll("Bearer ", "");
			final TokenValidationStatus authStatus = template.getForObject(validateUrl, TokenValidationStatus.class,
					authHeader);
			LOGGER.info("token validation: status={}", authStatus);
			MDC.put("userId", String.valueOf(authStatus.getUserId()));
		}
		// if (!authStatus.isValid()) {
		// sendResponse(res);
		// }
		// return authStatus.isValid();
		return true;
	}

	private void sendResponse(HttpServletResponse res) {
		res.setStatus(401);
		res.setContentType("application/json");
		try {
			res.getWriter().append("{\"message\":\"Token verification failed.\"}");
			res.getWriter().close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
