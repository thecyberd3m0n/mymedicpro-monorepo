package pl.telemedis.services.integraFHIR.server.config;

import java.util.Properties;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;

import org.apache.commons.dbcp2.BasicDataSource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowire;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.annotation.PropertySources;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.web.client.RestTemplate;

import ca.uhn.fhir.jpa.config.BaseJavaConfigDstu3;
import ca.uhn.fhir.jpa.dao.DaoConfig;

import ca.uhn.fhir.jpa.search.LuceneSearchMappingFactory;
import ca.uhn.fhir.jpa.util.SubscriptionsRequireManualActivationInterceptorDstu3;
import ca.uhn.fhir.rest.server.interceptor.IServerInterceptor;
import ca.uhn.fhir.rest.server.interceptor.LoggingInterceptor;
import ca.uhn.fhir.rest.server.interceptor.ResponseHighlighterInterceptor;
import pl.telemedis.services.integraFHIR.server.interceptor.OAuth2Interceptor;

@Configuration
@PropertySources({ @PropertySource("classpath:fhir.properties"),
		@PropertySource(value = "file:conf/fhir.properties", ignoreResourceNotFound = true) })
@EnableTransactionManagement()
@EnableAsync
public class FhirConfiguration extends BaseJavaConfigDstu3 {
	private static final Logger LOGGER = LoggerFactory.getLogger(OAuth2Interceptor.class);

	@Value("${db.url}")
	String dbUrl;
	@Value("${db.username}")
	String dbUsername;
	@Value("${db.password}")
	String dbPassword;

	// @Bean()
	// public DaoConfig daoConfig() {
	// DaoConfig retVal = new DaoConfig();
	// retVal.setAllowMultipleDelete(true);
	// retVal.setAllowInlineMatchUrlReferences(true);
	// return retVal;
	// }
	
	
	/**
	 * Configure FHIR properties around the the JPA server via this bean
	 */
	@Bean()
	public DaoConfig daoConfig() {
		DaoConfig retVal = new DaoConfig();
		retVal.setAllowMultipleDelete(true);
		retVal.setAllowInlineMatchUrlReferences(true);
		return retVal;
	}

	/**
	 * The following bean configures the database connection. The 'url' property
	 * value of "jdbc:derby:directory:jpaserver_derby_files;create=true"
	 * indicates that the server should save resources in a directory called
	 * "jpaserver_derby_files".
	 * 
	 * A URL to a remote database could also be placed here, along with login
	 * credentials and other properties supported by BasicDataSource.
	 */

	@Bean(destroyMethod = "close")
	public DataSource dataSource() {

		BasicDataSource retVal = new BasicDataSource();

		retVal.setDriverClassName("org.postgresql.Driver");
		retVal.setUrl(dbUrl);
		LOGGER.info("dbUrl={}", dbUrl);
		retVal.setUsername(dbUsername);
		LOGGER.info("dbUsername={}", dbUsername);
		retVal.setPassword(dbPassword);
		LOGGER.info("dbPassword={}", dbPassword);
		return retVal;
	}

	@Override
	@Bean()
	public LocalContainerEntityManagerFactoryBean entityManagerFactory() {
		String test = null;
		LOGGER.info("entityManagerFactory: test={}", test);
		LocalContainerEntityManagerFactoryBean retVal = super.entityManagerFactory();
		retVal.setPersistenceUnitName("HAPI_PU");
		retVal.setDataSource(dataSource());
		retVal.setJpaProperties(jpaProperties());
		LOGGER.info("entityManagerFactory: retVal={}", retVal);
		return retVal;
	}

	private Properties jpaProperties() {
		String test = null;
		LOGGER.info("jpaProperties: test={}", test);
		Properties extraProperties = new Properties();
		LOGGER.info("jpaProperties: extraProperties={}", extraProperties);
		extraProperties.put("hibernate.dialect", org.hibernate.dialect.PostgreSQL9Dialect.class.getName());
		extraProperties.put("hibernate.format_sql", "true");
		extraProperties.put("hibernate.show_sql", "false");
		extraProperties.put("hibernate.hbm2ddl.auto", "update");
		extraProperties.put("hibernate.jdbc.batch_size", "20");
		extraProperties.put("hibernate.cache.use_query_cache", "false");
		extraProperties.put("hibernate.cache.use_second_level_cache", "false");
		extraProperties.put("hibernate.cache.use_structured_entries", "false");
		extraProperties.put("hibernate.cache.use_minimal_puts", "false");
		extraProperties.put("hibernate.search.model_mapping", LuceneSearchMappingFactory.class.getName());
		extraProperties.put("hibernate.search.default.directory_provider", "filesystem");
		extraProperties.put("hibernate.search.default.indexBase", "target/lucenefiles");
		extraProperties.put("hibernate.search.lucene_version", "LUCENE_CURRENT");
		LOGGER.info("jpaProperties: extraProperties={}", extraProperties);
		return extraProperties;
	}

	/**
	 * Do some fancy logging to create a nice access log that has details about
	 * each incoming request.
	 */

	public IServerInterceptor loggingInterceptor() {
		String test = null;
		LOGGER.info("loggingInterceptor: test={}", test);
		LoggingInterceptor retVal = new LoggingInterceptor();
		LOGGER.info("loggingInterceptor: retVal={}", retVal);
		retVal.setLoggerName("fhirtest.access");
		retVal.setMessageFormat(
				"Path[${servletPath}] Source[${requestHeader.x-forwarded-for}] Operation[${operationType} ${operationName} ${idOrResourceName}] UA[${requestHeader.user-agent}] Params[${requestParameters}] ResponseEncoding[${responseEncodingNoDefault}]");
		retVal.setLogExceptions(true);
		retVal.setErrorMessageFormat("ERROR - ${requestVerb} ${requestUrl}");
		LOGGER.info("loggingInterceptor: retVal={}", retVal);
		return retVal;
	}

	/**
	 * This interceptor adds some pretty syntax highlighting in responses when a
	 * browser is detected
	 */

	@Bean(autowire = Autowire.BY_TYPE)
	public IServerInterceptor responseHighlighterInterceptor() {
		ResponseHighlighterInterceptor retVal = new ResponseHighlighterInterceptor();
		return retVal;
	}

	@Bean(autowire = Autowire.BY_TYPE)
	public IServerInterceptor subscriptionSecurityInterceptor() {
		SubscriptionsRequireManualActivationInterceptorDstu3 retVal = new SubscriptionsRequireManualActivationInterceptorDstu3();
		return retVal;
	}

	@Bean()
	public JpaTransactionManager transactionManager(EntityManagerFactory entityManagerFactory) {
		JpaTransactionManager retVal = new JpaTransactionManager();
		retVal.setEntityManagerFactory(entityManagerFactory);
		return retVal;
	}

	@Bean(autowire = Autowire.BY_TYPE)
	public IServerInterceptor oauth2Interceptor() {
		OAuth2Interceptor retVal = new OAuth2Interceptor();
		return retVal;
	}

	@Bean
	public RestTemplate restTemplate() {
		return new RestTemplate();
	}

}
