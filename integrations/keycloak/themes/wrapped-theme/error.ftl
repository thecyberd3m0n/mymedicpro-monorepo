<#import "template.ftl" as layout>
<@layout.registrationLayout displayMessage=false; section>
    <#if section = "head">
        <script>
        window.keycloak = {
            error: "${message.summary}"
        }
        console.error('KEYCLOAK ERROR: ', "${message.summary}")
    </script>
    </#if>
</@layout.registrationLayout>