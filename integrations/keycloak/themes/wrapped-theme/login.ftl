<#import "template.ftl" as layout>
<@layout.registrationLayout displayInfo=social.displayInfo; section>
<#if section = "head">
    <script>
        window.keycloak = {
            realm: {
                displayName: "${realm.displayName}",
                rememberMe: "${realm.rememberMe?c}",
                loginWithEmailAllowed: "${realm.loginWithEmailAllowed?c}",
                resetPasswordAllowed: "${realm.resetPasswordAllowed?c}",
                registrationEmailAsUsername: "${realm.registrationEmailAsUsername?c}"
            },
            url: {
                resourcesPath: "${url.resourcesPath}",
                loginAction: "${url.loginAction}".replace(/&amp;/g, '&'),
            },
            msg: {
                loginTitle: "${msg("loginTitle",(realm.displayName!''))}" || "master",
                doLoginIn: "${msg("doLogIn")}",
                username: "${msg("username")}",
                password: "${msg("password")}",
                loginUrl: "${msg("loginUrl")}"
            }
        }
    </script>
</#if>
</@layout.registrationLayout>
