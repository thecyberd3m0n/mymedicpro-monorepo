
const concat = require('concat');
const paths = require('../config/paths');
const path = require('path');   
concat([path.resolve(`${paths.appBuild}/static/css/2.chunk.css`),
    path.resolve(`${paths.appBuild}/static/css/main.chunk.css`)], path.resolve(`${paths.appBuild}/static/css/app.bundle.css`));

concat([path.resolve(`${paths.appBuild}/static/js/2.chunk.js`),
    path.resolve(`${paths.appBuild}/static/js/main.chunk.js`)], path.resolve(`${paths.appBuild}/static/js/app.bundle.js`));
