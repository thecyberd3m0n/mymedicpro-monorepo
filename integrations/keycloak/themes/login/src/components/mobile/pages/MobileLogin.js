import React, { Component } from 'react';
import { withStyles } from '@material-ui/core/styles';
import MMMobileInput from '../widgets/MMMobileInput';
import Switch from '@material-ui/core/Switch';
import MMMobileButton from '../widgets/MMMobileButton';
import { Button } from '@material-ui/core';

const styles = theme => ({
    root: theme.mixins.gutters({
        fontFamily: 'Poppins',
        minHeight: '80vh',
        overflowX: 'hidden'
    }),
    span1: {
        display: 'inline',
        fontWeight: '500',
        fontSize: '20px'
    },
    span_container_1: {
        marginTop: '4vh'
    },
    span2: {
        fontWeight: '500',
        color: '#2B99F3',
        fontSize: '14px'
    },
    span_container_2: {
        marginTop: '2vh',
        marginBottom: '2vh'
    },
    span3: {
        fontWeight: '500',
        color: '#AFAFAF',
        fontSize: '14px'
    },
    span_container_3: {
        marginTop: '2vh'
    },
    forget_pass: {
        fontWeight: '500',
        color: '#2B99F3',
        fontSize: '14px',
        textTransform: 'lowercase'
    },
    error: {
        color: 'red'
    }
});
class MobileLogin extends Component {
    constructor(props) {
        super(props);
        this.handleChange = this.handleChange.bind(this);
        this.state = {
            action: window.keycloak.url.loginAction,
            error: window.keycloak.error
        };
    }

    handleChange (e) {
        const { name, value } = e.target;
        this.setState({ [name]: value });
    }

    render () {
        const { classes, realm } = this.props;
        const { action, error } = this.state;
        
        return (
            <form className={ classes.root } noValidate action={ action } method="post">
                <div className={ `${classes.span_container_1} row center-xs center-sm middle-xs middle-sm` }>
                    <span className={ classes.span1 }>Zaloguj się, jako { (realm.displayName || 'patient').toLowerCase() === 'patient' ? 'Pacjent' : 'Lekarz'}</span>
                </div>
                
                <div className={ `${classes.span_container_2} row center-xs center-sm middle-xs middle-sm` }>
                    {
                        (realm.displayName || 'patient').toLowerCase() === 'patient' ? (
                        
                            <a href="https://zdrowie.plus/auth/realms/practitioner/protocol/openid-connect/auth?client_id=account&redirect_uri=%2Fapp&state=0%2F190b3dd5-4968-438e-907c-a73e21e8cbc6&response_type=code&scope=openid" 
                                className={ classes.span2 }
                            >Jesteś lekarzem?</a>
                        ) : (
                            <a href="https://zdrowie.plus/auth/realms/patient/protocol/openid-connect/auth?client_id=account&redirect_uri=%2Fapp&state=0%2F190b3dd5-4968-438e-907c-a73e21e8cbc6&response_type=code&scope=openid" 
                                className={ classes.span2 }
                            >Jesteś pacjentem?</a>
                        )
                    }
                </div>
                <MMMobileInput
                    type="email"
                    placeholder="Adres e-mail *"
                    name="username"
                ></MMMobileInput>
                <MMMobileInput
                    placeholder="Hasło *"
                    type="password"
                    name="password"
                ></MMMobileInput>
                <div className="row end-xs end-sm middle-xs middle-sm">
                    <span className={ classes.span3 }>Zapamiętaj mnie</span>
                    <Switch
                        color="primary"
                        name="rememberMe"
                    />
                </div>
                {
                    (error && error.length > 0) &&
                    <p className={ classes.error }>{ error }</p>
                }
                <div className="row center-xs center-sm middle-xs middle-sm">
                    <div className="col-xs-12 col-sm-12">
                        <MMMobileButton type="submit" color="primary" label="ZALOGUJ SIĘ"></MMMobileButton>
                    </div>
                </div>
                <div className={ classes.span_container_3 }>
                    <a href="/register/remindpasswordpatient">
                        <Button className={ classes.forget_pass }>Zapomniałeś hasła?</Button>
                    </a>
                </div>
            </form>
        );
    }
}

export default withStyles(styles)(MobileLogin);
