import React, { Component } from 'react';
import Fab from '@material-ui/core/Fab';
import { withStyles } from '@material-ui/core/styles';

const styles = () => ({
    root: {
        width: '100%',
        marginLeft: 'auto',
        marginRight: 'auto',
        fontWeight: '700',
        fontFamily: 'Poppins',
        fontSize: '0.7em'
    }
});
class MMMobileButton extends Component {
    render () {
        const { classes, label } = this.props;
        return (
            <Fab
                variant="extended"
                size="large"
                className={ classes.root }
                { ...this.props }
            >{label}</Fab>
        );
    }
}

export default withStyles(styles)(MMMobileButton);
