import React, { Component } from 'react';
import { withStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import MMMobileBottomBar from './MMMobileBottomBar';


const styles = () => ({
    root: {
        textAlign: 'center',
        fontFamily: 'Poppins'
    },
    white_container: {
        backgroundColor: '#FFFFFF',
        height: '90vh',
        width: '100%',
        overflowX: 'hidden'
    }
});

class MMMobileBackgroundProfileContainer extends Component {
    render () {
        const { classes, children } = this.props;
        return (
            <div className={ classes.root }>
                <AppBar position="static">
                    <Toolbar>
                        <IconButton color="inherit">
                            <MenuIcon />
                        </IconButton>
                        <Typography variant="h6" color="inherit">
                            Tablica
                        </Typography>
                    </Toolbar>
                </AppBar>
                <div className={ classes.white_container }>
                    {children}
                </div>
                <MMMobileBottomBar></MMMobileBottomBar>
            </div>
        );
    }
}
export default withStyles(styles)(MMMobileBackgroundProfileContainer);
