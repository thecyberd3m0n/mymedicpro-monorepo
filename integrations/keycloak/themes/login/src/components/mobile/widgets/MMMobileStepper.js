import React, { Component } from 'react';
import { withStyles } from '@material-ui/core/styles';
import MobileStepper from '@material-ui/core/MobileStepper';

const styles = () => ({
    root: {
        width: '100%',
        flexGrow: '1',
        fontFamily: 'Poppins sans-serif'
    }
});
class MMMobileStepper extends Component {
    constructor (props) {
        super(props);
        this.state = {
            activeStep: 0
        };
    }
    handleNext () {
        this.setState(state => ({
            activeStep: state.activeStep + 1
        }));
    }
    handleBack () {
        this.setState(state => ({
            activeStep: state.activeStep - 1
        }));
    }

    render () {
        const { steps } = this.props;
        return (
            <div className="row center-xs center-sm">
                <MobileStepper
                    steps={ steps }
                    variant="progress"
                    position="static"
                    activeStep={ this.state.activeStep }
                    { ...this.props }
                >
                </MobileStepper>
            </div>
        );
    }
}

export default withStyles(styles)(MMMobileStepper);
