import React, { Component } from 'react';
import TextField from '@material-ui/core/TextField';
import { withStyles } from '@material-ui/core/styles';

const styles = () => ({
    root: {
        fontFamily: 'Poppins'
    },
    input: {
        fontFamily: 'Poppins'
    }
});

class MMMobileInput extends Component {
    render () {
        const { classes } = this.props;
        return (
            // <div className={ `${classes.root} col-xs-12 col-sm-12` }>
            <TextField
                margin="dense"
                variant="outlined"
                className={ classes.root }
                fullWidth
                { ...this.props }
                InputProps={ { className: classes.input } }
            />
            // </div>
        );
    }
}

export default withStyles(styles)(MMMobileInput);
