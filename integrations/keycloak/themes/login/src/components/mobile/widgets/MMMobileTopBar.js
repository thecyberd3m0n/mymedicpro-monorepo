import React, { Component } from 'react';
import { withStyles } from '@material-ui/core/styles';
import IconButton from '@material-ui/core/IconButton';

const styles = theme => ({
    root: theme.mixins.gutters({
        backgroundColor: theme.palette.primary.light,
        textAlign: 'center',
        fontFamily: 'Poppins sans-serif'
    }),
    arrow: {
        color: '#FFFFFF',
        padding: '4px'
    },
    register: {
        fontWeight: '600',
        textTransform: 'uppercase',
        color: '#FFFFFF',
        fontSize: '15px',
        width: '100%'
    },
    container_1: {
        height: '7vh'
    }
});
class MMMobileTopBar extends Component {
    render () {
        const { classes, title } = this.props;
        return (
            <div className={ `${classes.container_1} row center-xs middle-xs` }>
                <div className="col-xs-4 center-xs">
                    <IconButton className={ classes.arrow }>
                        <i className="material-icons">
                            keyboard_arrow_left</i>
                    </IconButton>
                    {/* <IconButton className={classes.arrow}>
            <i className="material-icons">menu</i>
          </IconButton>
          <IconButton className={classes.arrow}>
            <i className="material-icons">dashboard</i>
          </IconButton> */}
                </div>
                <div className={ `${classes.register} col-xs-4 center-xs` }>
                    {title}
                </div>
                <div className="col-xs-4"></div>
            </div>
        );
    }
}

export default withStyles(styles, { withTheme: true })(MMMobileTopBar);
