import React, { Component } from 'react';
import { withStyles } from '@material-ui/core/styles';
import wywiadAvatar from '../pages/images/wywiad_avatar.png';
import interviewCamera from '../pages/images/interviewCamera.png';
const styles = theme => ({
    root: {
        width: '100%',
        backgroundColor: theme.palette.primary.light
    },
    avatar: {
        backgroundImage: `url(${wywiadAvatar})`,
        height: '15vh',
        width: '15vh',
        backgroundSize: 'contain',
        backgroundRepeat: 'no-repeat',
        backgroundPosition: 'center',
        position: 'relative'
    },
    camera_background: {
        backgroundImage: `url(${interviewCamera})`,
        backgroundSize: 'contain',
        position: 'absolute',
        backgroundPosition: 'center',
        backgroundRepeat: 'no-repeat',
        bottom: '0px',
        right: '0px',
        height: '4vh',
        width: '4vh'
    }
});

class MMMobileAvatarUploader extends Component {
    render () {
        const { classes } = this.props;
        return (
            <div className="row center-xs center-sm">
                <div className={ classes.avatar }>
                    <div className={ classes.camera_background }>
                    </div>
                </div>
            </div>
        );
    }
}

export default withStyles(styles)(MMMobileAvatarUploader);
