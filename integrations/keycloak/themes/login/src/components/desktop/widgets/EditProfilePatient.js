import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import foto from '../pages/images/foto.png';

const styles = () => ({
    root: {
        textAlign: 'left',
        fontFamily: 'Poppins sans-serif',
        fontSize: '14px'
    },
    avatar: {
        width: '154px',
        height: '154px',
        backgroundSize: 'cover',
        backgroundPosition: 'center center',
        backgroundRepeat: 'none',
        borderRadius: '50%'
    },
    avatarBox: {
        padding: '20px'
    },
    foto: {
        backgroundImage: `url(${foto})`,
        height: '42px',
        width: '42px',
        backgroundSize: 'contain',
        backgroundRepeat: 'no-repeat',
        backgroundPosition: 'center',
        position: 'relative',
        top: '155px',
        left: '118px'
    },
    namePatient: {
        fontSize: '20px',
        color: 'rgba(32, 32, 32, 255)'
    },
    headerProfile: {
        textAlign: 'right',
        width: '100%',
        borderBottom: '1px solid #dadada',
        margin: '0'
    },
    contentBox: {
        border: '1px solid #dadada',
        margin: '20px'
    },
    parametrs: {
        display: 'inline-block'
    },
    btnBlue: {
        border: 'none',
        borderRadius: '3px',
        paddingTop: '9px',
        paddingBottom: '9px',
        paddingLeft: '16px',
        paddingRight: '16px',
        fontSize: '14px',
        fontWeight: '600',
        backgroundColor: '#2196f3',
        color: '#fff',
        marginLeft: '5px',
        marginRight: '5px'
    },
    mr: {
        marginLeft: '0',
        marginRight: '0'
    }
});

class EditProfilePatient extends React.Component {
    render () {
        const { classes } = this.props;

        // const urlAvatar = this.props.patientStore.getAvatar("http://via.placeholder.com/35x35");
        const urlAvatar = 'http://mymedic.com.pl/wp-content/uploads/2019/03/avatar.png';
        const namePatient = 'Anna Kowalska';
        const howYears = '21 lat';

        return (
        <>
            <div className={ classes.headerProfile }>
                <a className={ classes.linkBlue } href="/">Poleć aplikację </a>
                <button className={ classes.btnBlue } href="/">Umów e-Wizytę</button>
                <span className={ classes.textProfile }>Poleć aplikację </span>
            </div>

          <div className={ classes.contentBox }>
              <div className={ `${classes.mr} row` }>
                  <div className="col-xs-12 col-sm-12 col-md-3">
                      <div className={ classes.avatarBox }>
                          <div className={ classes.foto } />
                          <div className={ classes.avatar } style={ {
                              backgroundImage: 'url(' + urlAvatar + ')' } }
                          />
                      </div>
                  </div>
                  <div className="col-xs-12 col-sm-12 col-md-9">
                      <div className={ classes.personalEdit }>
                          <div className={ classes.name }>{namePatient}</div>
                          <div className={ classes.years }>{howYears}</div>
                          <div className={ classes.parametrs }>
                              <p>WAGA</p>
                              <p>58</p>
                          </div>
                          <div className={ classes.parametrs }>
                              <p>WZROST</p>
                              <p>170</p>
                          </div>
                          <div className={ classes.parametrs }>
                              <p>BM</p>
                              <p>30</p>
                          </div>
                          <div className={ classes.parametrs }>
                              <p>KREW</p>
                              <p>0 Rh +</p>
                          </div>
                      </div>
                  </div>
              </div>
              <div className={ `${classes.mr} row` }>
                  <div className="col-xs-12 col-sm-12 col-md-12">
              Dane osobowe <button className={ classes.btnBlue } href="/">Edytuj</button>
                  </div>
              </div>
              <div className={ `${classes.mr} row` }>
                  <div className="col-xs-12 col-sm-12 col-md-4">
                      <p>Imię *</p>
                      <p>Anna</p>
                  </div>
                  <div className="col-xs-12 col-sm-12 col-md-4">
                      <p>Nazwisko *</p>
                      <p>Kowalska</p>
                  </div>
                  <div className="col-xs-12 col-sm-12 col-md-4">
                      <p>Płeć *</p>
                      <p>Kobieta</p>
                  </div>
              </div>
              <div className={ `${classes.mr} row` }>
                  <div className="col-xs-12 col-sm-12 col-md-4">
                      <p>Data urodzenia * </p>
                      <p>12-07-1994</p>
                  </div>
                  <div className="col-xs-12 col-sm-12 col-md-8">
                      <p>PESEL *</p>
                      <p>94071200343</p>
                  </div>
              </div>
              <div className={ `${classes.mr} row` }>
                  <div className="col-xs-12 col-sm-12 col-md-12">
              Dane adresowe <button className={ classes.btnBlue } href="/">Edytuj</button>
                  </div>
              </div>
              <div className={ `${classes.mr} row` }>
                  <div className="col-xs-12 col-sm-12 col-md-4">
                      <p>Ulica</p>
                      <p>Jaśminowa</p>
                  </div>
                  <div className="col-xs-12 col-sm-12 col-md-4">
                      <p>Numer domu</p>
                      <p>18</p>
                  </div>
                  <div className="col-xs-12 col-sm-12 col-md-4">
                      <p>Numer lokalu</p>
                      <p>2</p>
                  </div>
              </div>
              <div className={ `${classes.mr} row` }>
                  <div className="col-xs-12 col-sm-12 col-md-4">
                      <p>Miasto</p>
                      <p>Warszawa</p>
                  </div>
                  <div className="col-xs-12 col-sm-12 col-md-8">
                      <p>Kod pocztowy</p>
                      <p>00-950</p>
                  </div>
              </div>
              <div className={ `${classes.mr} row` }>
                  <div className="col-xs-12 col-sm-12 col-md-12">
                Dane kontaktowe <button className={ classes.btnBlue } href="/">Edytuj</button>
                  </div>
              </div>
              <div className={ `${classes.mr} row` }>
                  <div className="col-xs-12 col-sm-12 col-md-4">
                      <p>Numer telefonu *</p>
                      <p>0 500 500 500</p>
                  </div>
                  <div className="col-xs-12 col-sm-12 col-md-8">
                      <p>adres e-mail *</p>
                      <p>kontakt@wp.pl</p>
                  </div>
              </div>
          </div>
        </>
        );
    }
}

export default withStyles(styles)(EditProfilePatient);
