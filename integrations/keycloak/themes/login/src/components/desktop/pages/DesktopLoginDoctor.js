import React, { Component } from 'react';
import { withStyles } from '@material-ui/core/styles';
import logo from './images/logo-profile.png';
import login from './images/img-login.png';
import graf from './images/graf.png';
import geo from './images/geo.png';
import nextArrow from './images/next.png';
import Checkbox from '@material-ui/core/Checkbox';

const styles = () => ({
    root: {
        textAlign: 'left',
        fontFamily: 'Poppins sans-serif',
        fontSize: '16px'
    },
    bdg: {
        paddingBottom: '85px',
        width: '100%'
    },
    bgt: {
        backgroundColor: '#f1f1f1'
    },
    sc: {
        backgroundColor: '#fff',
        width: '100%',
        height: '100%',
        margin: '0 auto',
        marginBottom: '5px'
    },
    inputs: {
        border: '1px solid',
        borderColor: '#cccccc',
        color: '#cccccc',
        borderRadius: '3px',
        marginTop: '6px',
        marginBottom: '6px',
        padding: '12px',
        fontSize: '15px',
        width: '92%'
    },
    logo: {
        backgroundImage: `url(${logo})`,
        height: '60px',
        width: '200px',
        backgroundSize: 'contain',
        backgroundRepeat: 'no-repeat',
        backgroundPosition: 'left',
        marginTop: '20px',
        marginLeft: '150px',
        marginBottom: '0'
    },
    elementMenu: {
        display: 'inline-block',
        height: '6vh',
        marginTop: '5vh',
        marginBottom: '3vh',
        marginRight: '15px',
        paddingTop: '15px',
        paddingBottom: '15px',
        color: '#8b909b',
        fontWeight: '600',
        textDecoration: 'none'
    },
    login: {
        backgroundImage: `url(${login})`,
        height: '72px',
        width: '230px',
        backgroundSize: 'contain',
        backgroundRepeat: 'no-repeat',
        backgroundPosition: 'center'
    },
    graf: {
        backgroundImage: `url(${graf})`,
        height: '340px',
        width: '180px',
        backgroundSize: 'contain',
        backgroundRepeat: 'no-repeat',
        backgroundPosition: 'center',
        position: 'absolute',
        top: '45%',
        right: '2%'
    },
    geo: {
        display: 'inline-block',
        backgroundImage: `url(${geo})`,
        height: '15px',
        width: '13px',
        backgroundSize: 'contain',
        backgroundRepeat: 'no-repeat',
        backgroundPosition: 'center',
        marginRight: '4px',
        verticalAlign: 'middle'
    },
    nextArrow: {
        display: 'inline-block',
        backgroundImage: `url(${nextArrow})`,
        height: '18px',
        width: '18px',
        backgroundSize: 'contain',
        backgroundRepeat: 'no-repeat',
        backgroundPosition: 'center',
        marginRight: '4px',
        verticalAlign: 'middle'
    },
    bg: {
        width: '100%',
        backgroundColor: '#f1f1f1'
    },
    borderBox: {
        maxWidth: '952px',
        border: '1px solid #dadada',
        color: '#dadada'
    },
    formBox: {
        textAlign: 'center',
        paddingTop: '20px',
        paddingLeft: '85px',
        paddingRight: '85px',
        paddingBottom: '20px'
    },
    formBox1: {
        textAlign: 'left',
        paddingLeft: '85px',
        paddingRight: '85px',
        paddingBottom: '20px'
    },
    formBox2: {
        textAlign: 'center',
        paddingTop: '0',
        paddingLeft: '85px',
        paddingRight: '85px',
        paddingBottom: '20px'
    },
    centerBox: {
        width: '230px',
        height: '230px',
        position: 'relative',
        left: '50%',
        top: '50%',
        marginLeft: '-115px',
        marginTop: '-115px'
    },
    span1: {
        color: '#383838',
        display: 'inline',
        fontWeight: '700',
        fontSize: '20px'
    },
    span2: {
        color: '#43ceba',
        fontWeight: '700',
        fontSize: '20px'
    },
    span3: {
        color: '#383838'
    },
    p1: {
        color: '#383838',
        fontWeight: '700',
        fontSize: '20px'
    },
    btnForm: {
        border: 'none',
        borderRadius: '3px',
        width: '100%',
        paddingTop: '12px',
        paddingBottom: '12px',
        fontSize: '15px',
        fontWeight: '600',
        backgroundColor: '#0eb9ba',
        color: '#fff',
        marginBottom: '15px'
    },
    btnLogin: {
        border: 'none',
        borderRadius: '3px',
        paddingTop: '12px',
        paddingBottom: '12px',
        paddingLeft: '33px',
        paddingRight: '33px',
        fontSize: '15px',
        fontWeight: '600',
        backgroundColor: '#2196f3',
        color: '#fff',
        textDecoration: 'none'
    },
    linkText: {
        lineHeight: '0.8',
        color: '#8b909b',
        fontWeight: '600',
        textDecoration: 'none'
    },
    linkText1: {
        lineHeight: '0.8',
        color: '#000',
        fontWeight: '600',
        textDecoration: 'none'
    },
    linkText2: {
        lineHeight: '0.8',
        color: '#259ee3',
        fontWeight: '600',
        textDecoration: 'none'
    },
    linkText3: {
        fontSize: '14px',
        lineHeight: '0.8',
        color: '#8b909b',
        fontWeight: '600',
        textDecoration: 'none'
    },
    btnTextMenu: {
        color: '#fff',
        display: 'inline-block',
        fontSize: '16px',
        lineHeight: '15px',
        padding: '12px 15px',
        fontWeight: '400',
        textDecoration: 'none'
    },
    btnTextMenu1: {
        backgroundColor: '#0eb9ba',
        color: '#fff',
        display: 'inline-block',
        fontSize: '16px',
        lineHeight: '15px',
        padding: '12px 15px',
        fontWeight: '400',
        textDecoration: 'none'
    },
    btnTextMenu2: {
        color: '#000',
        display: 'inline-block',
        fontSize: '16px',
        lineHeight: '15px',
        padding: '12px 15px',
        fontWeight: '400',
        textDecoration: 'none'
    },
    btnRegisterMenu: {
        color: '#fff',
        display: 'inline-block',
        border: '2px solid',
        borderColor: '#fff',
        borderRadius: '3px',
        fontSize: '16px',
        lineHeight: '15px',
        padding: '10px 15px',
        fontWeight: '500',
        marginLeft: '15px',
        marginTop: '20px',
        textDecoration: 'none'
    },
    btnLoginMenu: {
        color: '#fff',
        display: 'inline-block',
        border: '2px solid',
        borderColor: '#fff',
        borderRadius: '3px',
        fontSize: '16px',
        lineHeight: '15px',
        padding: '10px 15px',
        fontWeight: '500',
        marginLeft: '15px',
        marginTop: '20px',
        textDecoration: 'none'
    },
    boxRight: {
        marginTop: '10px',
        marginBottom: '32px',
        marginRight: '150px',
        textAlign: 'right'
    },
    boxTop: {
        marginTop: '10px',
        marginRight: '150px',
        marginBottom: '0',
        textAlign: 'right'
    },
    crumbsBox: {
        marginBottom: '20px'
    },
    linkBox: {
        display: 'inline-block',
        verticalAlign: 'top',
        marginTop: '-12px'
    },
    linkBox1: {
        display: 'inline-block',
        width: '240px'
    },
    rowLine: {
        backgroundColor: '#0eb9ba',
        borderBottom: '1px solid',
        color: '#dadada',
        marginBottom: '25px'
    },
    cf: {
        paddingBottom: '45px'
    },
    error: {
        color: 'red'
    }
});

class DesktopLoginDoctor extends Component {
    constructor(props) {
        super(props);
        this.handleChange = this.handleChange.bind(this);
        this.state = {
            action: window.keycloak.url.loginAction,
            error: window.keycloak.error
        };
    }

    handleChange (e) {
        const { name, value } = e.target;
        this.setState({ [name]: value });
    }

    render () {
        const { classes } = this.props;
        const { action, error } = this.state;

        return (
            <section className={ classes.bdg }>
                <div className={ classes.sc }>
                    <div className={ classes.graf } />
                    <div className={ `${classes.bgt} container-fluid` }>
                        <div className="row">
                            <div className="col-xs-12 col-sm-12 col-md-12">
                                <div className={ classes.boxTop }>
                                    <a className={ classes.btnTextMenu2 } href="https://zdrowie.plus/auth/realms/patient/protocol/openid-connect/auth?client_id=account&redirect_uri=https%3A%2F%2Fzdrowie.plus%2Fapp&state=0%2F190b3dd5-4968-438e-907c-a73e21e8cbc6&response_type=code&scope=openid">
                    Strefa Pacjenta
                                    </a>
                                    <a className={ classes.btnTextMenu1 } href="https://zdrowie.plus/auth/realms/practitioner/protocol/openid-connect/auth?client_id=account&redirect_uri=https%3A%2F%2Fzdrowie.plus%2Fapp&state=0%2F190b3dd5-4968-438e-907c-a73e21e8cbc6&response_type=code&scope=openid">
                    Strefa Lekarza
                                    </a>
                                    <a className={ classes.btnTextMenu2 } href="/">
                    Strefa Biznesu
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className={ `${classes.rowLine} container-fluid ` }>
                        <div className="row">
                            <div className="col-xs-12 col-sm-12 col-md-2">
                                <div className={ classes.logo } />
                            </div>
                            <div className="col-xs-12 col-sm-12 col-md-10">
                                <div className={ classes.boxRight }>
                                    <a className={ classes.btnTextMenu } href="/">
                    Aktualności
                                    </a>
                                    <a className={ classes.btnTextMenu } href="/">
                    Jak to działa
                                    </a>
                                    <a className={ classes.btnTextMenu } href="/">
                    Aplikacje
                                    </a>
                                    <a className={ classes.btnRegisterMenu } href="/register/practitioner">
                    Załóż konto
                                    </a>
                                    {/* TODO: link is static, changing domain would disable app */}
                                    <a className={ classes.btnLoginMenu } href="https://zdrowie.plus/auth/realms/practitioner/protocol/openid-connect/auth?client_id=account&redirect_uri=https%3A%2F%2Fzdrowie.plus%2Fapp&state=0%2F190b3dd5-4968-438e-907c-a73e21e8cbc6&response_type=code&scope=openid">
                    Zaloguj się
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className={ `${classes.cf} container` }>
                        <div className={ classes.crumbsBox }>
                            <div className={ classes.geo } /> <a className={ classes.linkText } href="/">Strona główna</a><div className={ classes.nextArrow } /><span className={ classes.linkText1 }>Logowanie</span>
                        </div>
                        <div className={ `${classes.borderBox} row` }>
                            <div className="col-xs-12 col-sm-12 col-md-6">
                                <form className={ classes.root } noValidate action={ action } method="post">
                                    <div className={ classes.formBox }>
                                        <h1>
                                            <span className={ classes.span1 }>Zaloguj się, jako </span>
                                            {/* TODO: link is static, changing domain would disable app */}
                                            <a className={ classes.span2 } href="https://zdrowie.plus/auth/realms/practitioner/protocol/openid-connect/auth?client_id=account&redirect_uri=https%3A%2F%2Fzdrowie.plus%2Fapp&state=0%2F190b3dd5-4968-438e-907c-a73e21e8cbc6&response_type=code&scope=openid">Lekarz</a>
                                        </h1>
                                        <label>
                                            <input type="e-mail" className={ classes.inputs } placeholder="Adres e-mail *" name="username" />
                                        </label>
                                        <label>
                                            <input type="password" className={ classes.inputs } placeholder="Hasło * " name="password" />
                                        </label>
                                    </div>
                                    <div className={ classes.formBox1 }>
                                        <div className={ classes.linkBox }><Checkbox color="primary" name="rememberMe" /></div>
                                        <div className={ classes.linkBox1 }>
                                            <span className={ classes.linkText3 }>Zapamiętaj mnie</span>{' '}
                                            <br />
                                        </div>
                                        {
                                            (error.length > 0) &&
                                            <p className={ classes.error }>{ error }</p>
                                        }
                                    </div>
                                    <div className={ classes.formBox2 }>
                                        <button type="submit" className={ classes.btnForm }>
                        Zaloguj się
                                        </button>
                                        <a className={ classes.linkText } href="/register/remindpasswordpatient">
                        Zapomniałeś hasła?
                                        </a>
                                    </div>
                                </form>
                            </div>
                            <div
                                className={ `${
                                    classes.bg
                                } col-xs-12 col-sm-12 col-md-6 center-xs center-sm` }
                            >
                                <div className={ classes.centerBox }>
                                    <div className={ classes.login } />
                                    <p className={ classes.p1 }>Nie masz jeszcze konta?</p>
                                    <a href="/register/practitioner" className={ classes.btnLogin }>
                    Zarejestruj się
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        );
    }
}

export default withStyles(styles)(DesktopLoginDoctor);
