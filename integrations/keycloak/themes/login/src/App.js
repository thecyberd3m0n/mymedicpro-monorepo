import React, { Component } from 'react';
import './App.css';
import { MuiThemeProvider } from '@material-ui/core/styles';
import theme from './const/theme';
import { isMobile } from 'react-device-detect';
import MMMobileBackgroundContainer from './components/mobile/widgets/MMMobileBackgroundContainer';
import DesktopLoginDoctor from './components/desktop/pages/DesktopLoginDoctor';
import MobileLogin from './components/mobile/pages/MobileLogin';
class App extends Component {
    constructor(props) {
        super(props);
        this.state = {
            realm: window.keycloak.realm
        };
    }
    render() {
        const { realm } = this.state;
        return (
            <MuiThemeProvider theme={ theme }>
                {isMobile ? (
                    <div>
                        <MMMobileBackgroundContainer title="Zaloguj się" role={ realm }>
                            <MobileLogin realm={ realm } />
                        </MMMobileBackgroundContainer>
                    </div>
                ) : (
                    <div>
                        <DesktopLoginDoctor />
                    </div>
                )}
            </MuiThemeProvider>
        );
    }
}

export default App;
