import { createMuiTheme } from '@material-ui/core/styles';

const theme = createMuiTheme({
    typography: {
        useNextVariants: true
    },
    palette: {
        type: 'light',
        primary: {
            light: '#01B5B6', // turcus
            main: '#2196F3', // light blue
            dark: '#3C5A99', // dark blue
            contrastText: '#fff' // white
        },
        secondary: {
            light: '#f44336', // orange-red
            main: '#01B5B6', // dark blue
            dark: '#1d1d1d', // black
            contrastText: '#fff' // white
        },
        inherit: {
            main: '#3C5A99'
        }
    }
});
export default theme;
